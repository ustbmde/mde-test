SYNTAXDEF testmodel
FOR <http://www.ustb.edu.cn/sei/mde/testing/testdefinition> <testing.genmodel>
START TestModel,testcase.TestCaseModel

IMPORTS {
	ecore : <http://www.eclipse.org/emf/2002/Ecore>
	testing : <http://www.ustb.edu.cn/sei/mde/testing> <testing.genmodel>
	testcase : <http://www.ustb.edu.cn/sei/mde/testing/testcase> <testing.genmodel>
}


OPTIONS {
	reloadGeneratorModel = "true";
	overrideBuilder = "false"; 
}

TOKENS {

//  DEFINE URI $'<'('0'..'9'|'a'..'z'|'A'..'Z'|'_'|'-'|'/'|'\\'|'\.'|':')+'>'$;
//  DEFINE BOOLEAN $'true'|'false'$;
//  DEFINE NAME $(('a'..'z'|'A'..'Z'|'0'..'9'|'_')+'!')?('a'..'z'|'A'..'Z'|'_')('0'..'9'|'a'..'z'|'A'..'Z'|'_')*('@post')?$;
//  DEFINE NUMBER $('0'..'9')+$;
//  DEFINE OBJ_URI $'@'('a'..'z'|'A'..'Z'|'0'..'9'|'_'|'-'|'/'|'\\'|'\.'|':'|'#')*$;
//  DEFINE LITERAL $('a'..'z'|'A'..'Z'|'_')('0'..'9'|'a'..'z'|'A'..'Z'|'_')*'::'('a'..'z'|'A'..'Z'|'_')('0'..'9'|'a'..'z'|'A'..'Z'|'_')*$;

  DEFINE IDENTIFIER $('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'0'..'9'|'_')*$;
  DEFINE NUMBER $'-'?('0'..'9')+$;
  DEFINE SL_COMMENT $ '//'(~('\n'|'\r'|'\uffff'))* $;
  DEFINE ML_COMMENT $ '/*'.*'*/'$;
  DEFINE ANNOTATION $'@'('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'0'..'9'|'_')*$;
}


RULES {
	// syntax definition for class 'StartMetaClass'
	//testing.StringToStringEntry ::= key[IDENTIFIER] "=" value['\'','\'','\\'] ;
	//Type ::= "type" name[IDENTIFIER] "{" (properties ";")* "}";
	//Property ::= type[IDENTIFIER]  many["[]":""]  name[IDENTIFIER];
	
	ecore.EClass ::= eAnnotations* interface["interface":"type"] name[IDENTIFIER] ("extends" eSuperTypes[IDENTIFIER] ("," eSuperTypes[IDENTIFIER])*)? "{" eStructuralFeatures* eOperations*"}";
	ecore.EAttribute ::= eAnnotations*  eType[IDENTIFIER] ("[" lowerBound[NUMBER] ".." upperBound[NUMBER] "]")? name[IDENTIFIER] ";";
	ecore.EReference ::= eAnnotations*  eType[IDENTIFIER] "*" ("[" lowerBound[NUMBER] ".." upperBound[NUMBER] "]")? name[IDENTIFIER] ";";
	
	ecore.EOperation ::= eAnnotations*  eType[IDENTIFIER] ("[" lowerBound[NUMBER] ".." upperBound[NUMBER] "]")? name[IDENTIFIER] "(" (eParameters ("," eParameters)*)* ")" ";" ;
	
	ecore.EParameter ::= eAnnotations* eType[IDENTIFIER] ("[" lowerBound[NUMBER] ".." upperBound[NUMBER] "]")? name[IDENTIFIER] ;
	
	ecore.EAnnotation ::= source[ANNOTATION] ("(" details ("," details)* ")")?;
	
	ecore.EStringToStringMapEntry ::= key['"','"','\\'] "=" value['"','"','\\'] ;
	
	
	TestModel ::=  "model" name[IDENTIFIER] classifiers* scenarios*;
	
	TestScenario ::= "test" name[IDENTIFIER] "{" !0  (variables ";" !0)* ("do" do:Script !0 )? assertion !0 "}" !0;
	
	Script ::= (language[IDENTIFIER] ":" )? script['<%', '%>','\\'] ;
	
	Variable ::= "var" type[IDENTIFIER] name[IDENTIFIER] value ;
	ValueRangeSpecification ::= "in" "[" minLiteral['"','"','\\'] ".." maxLiteral['"','"','\\'] "]" ;
	ValueSetSpecification ::= "in" "{" literals['"','"','\\'] ("," literals['"','"','\\'])* "}" ;
	ValueScriptSpecification ::= "=" (language[IDENTIFIER] ":" )? script['<%', '%>','\\'] ;
	ReturnValueSpecification ::= "=" interface[IDENTIFIER] "." operation[IDENTIFIER] "(" (actualParameters[IDENTIFIER] ("," actualParameters[IDENTIFIER])*)? ")" ;
	Oracle ::= "assert" (language[IDENTIFIER] ":" )? script['<%', '%>','\\'] ;
	
	testcase.TestCaseModel ::= "module" name[IDENTIFIER] ("based on" #1 testmodel['"','"','\\'])? ("import" #1 ePackage['"','"','\\'])? !0 testcases* !0;
	
	testcase.TestCase ::= "test" #1 name[IDENTIFIER] ("based on" #1 scenario[IDENTIFIER])? "{"!0 (variables ";" !0)* ("do" do:Script !0)? assertion !0 "}"!0;
	testcase.ValueLiteralSpecification ::= "=" literal['"','"','\\'] ;
}