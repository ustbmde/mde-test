/**
 */
package edu.ustb.sei.mde.testing.testdefinition;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Range Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMinLiteral <em>Min Literal</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMaxLiteral <em>Max Literal</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getValueRangeSpecification()
 * @model
 * @generated
 */
public interface ValueRangeSpecification extends ValueSpecification {
	/**
	 * Returns the value of the '<em><b>Min Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Literal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Literal</em>' attribute.
	 * @see #setMinLiteral(String)
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getValueRangeSpecification_MinLiteral()
	 * @model required="true"
	 * @generated
	 */
	String getMinLiteral();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMinLiteral <em>Min Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Literal</em>' attribute.
	 * @see #getMinLiteral()
	 * @generated
	 */
	void setMinLiteral(String value);

	/**
	 * Returns the value of the '<em><b>Max Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Literal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Literal</em>' attribute.
	 * @see #setMaxLiteral(String)
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getValueRangeSpecification_MaxLiteral()
	 * @model required="true"
	 * @generated
	 */
	String getMaxLiteral();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMaxLiteral <em>Max Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Literal</em>' attribute.
	 * @see #getMaxLiteral()
	 * @generated
	 */
	void setMaxLiteral(String value);

} // ValueRangeSpecification
