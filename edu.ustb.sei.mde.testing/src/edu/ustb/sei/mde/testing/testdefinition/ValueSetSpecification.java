/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Set Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification#getLiterals <em>Literals</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getValueSetSpecification()
 * @model
 * @generated
 */
public interface ValueSetSpecification extends ValueSpecification {
	/**
	 * Returns the value of the '<em><b>Literals</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literals</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literals</em>' attribute list.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getValueSetSpecification_Literals()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getLiterals();

} // ValueSetSpecification
