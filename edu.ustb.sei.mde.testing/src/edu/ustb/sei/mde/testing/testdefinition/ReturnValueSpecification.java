/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Return Value Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getInterface <em>Interface</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getOperation <em>Operation</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getActualParameters <em>Actual Parameters</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getReturnValueSpecification()
 * @model
 * @generated
 */
public interface ReturnValueSpecification extends ValueSpecification {
	/**
	 * Returns the value of the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' reference.
	 * @see #setInterface(EClass)
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getReturnValueSpecification_Interface()
	 * @model required="true"
	 * @generated
	 */
	EClass getInterface();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getInterface <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(EClass value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see #setOperation(EOperation)
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getReturnValueSpecification_Operation()
	 * @model required="true"
	 * @generated
	 */
	EOperation getOperation();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(EOperation value);

	/**
	 * Returns the value of the '<em><b>Actual Parameters</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.testing.testdefinition.Variable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Parameters</em>' reference list.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getReturnValueSpecification_ActualParameters()
	 * @model
	 * @generated
	 */
	EList<Variable> getActualParameters();

} // ReturnValueSpecification
