/**
 */
package edu.ustb.sei.mde.testing.testdefinition.impl;

import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;
import edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value Range Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueRangeSpecificationImpl#getMinLiteral <em>Min Literal</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueRangeSpecificationImpl#getMaxLiteral <em>Max Literal</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ValueRangeSpecificationImpl extends ValueSpecificationImpl implements ValueRangeSpecification {
	/**
	 * The default value of the '{@link #getMinLiteral() <em>Min Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLiteral()
	 * @generated
	 * @ordered
	 */
	protected static final String MIN_LITERAL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinLiteral() <em>Min Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinLiteral()
	 * @generated
	 * @ordered
	 */
	protected String minLiteral = MIN_LITERAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxLiteral() <em>Max Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxLiteral()
	 * @generated
	 * @ordered
	 */
	protected static final String MAX_LITERAL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaxLiteral() <em>Max Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxLiteral()
	 * @generated
	 * @ordered
	 */
	protected String maxLiteral = MAX_LITERAL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueRangeSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestdefinitionPackage.Literals.VALUE_RANGE_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMinLiteral() {
		return minLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinLiteral(String newMinLiteral) {
		String oldMinLiteral = minLiteral;
		minLiteral = newMinLiteral;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL, oldMinLiteral, minLiteral));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMaxLiteral() {
		return maxLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxLiteral(String newMaxLiteral) {
		String oldMaxLiteral = maxLiteral;
		maxLiteral = newMaxLiteral;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL, oldMaxLiteral, maxLiteral));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL:
				return getMinLiteral();
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL:
				return getMaxLiteral();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL:
				setMinLiteral((String)newValue);
				return;
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL:
				setMaxLiteral((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL:
				setMinLiteral(MIN_LITERAL_EDEFAULT);
				return;
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL:
				setMaxLiteral(MAX_LITERAL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL:
				return MIN_LITERAL_EDEFAULT == null ? minLiteral != null : !MIN_LITERAL_EDEFAULT.equals(minLiteral);
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL:
				return MAX_LITERAL_EDEFAULT == null ? maxLiteral != null : !MAX_LITERAL_EDEFAULT.equals(maxLiteral);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minLiteral: ");
		result.append(minLiteral);
		result.append(", maxLiteral: ");
		result.append(maxLiteral);
		result.append(')');
		return result.toString();
	}

} //ValueRangeSpecificationImpl
