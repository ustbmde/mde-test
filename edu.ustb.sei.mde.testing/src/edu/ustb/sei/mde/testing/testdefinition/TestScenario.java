/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getTestScenario()
 * @model
 * @generated
 */
public interface TestScenario extends AbstractTestDefinition {

} // TestScenario
