/**
 */
package edu.ustb.sei.mde.testing.testdefinition.impl;

import edu.ustb.sei.mde.testing.impl.NamedElementImpl;

import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.TestScenario;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.impl.TestModelImpl#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.impl.TestModelImpl#getScenarios <em>Scenarios</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestModelImpl extends NamedElementImpl implements TestModel {
	/**
	 * The cached value of the '{@link #getClassifiers() <em>Classifiers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<EClass> classifiers;

	/**
	 * The cached value of the '{@link #getScenarios() <em>Scenarios</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<TestScenario> scenarios;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestdefinitionPackage.Literals.TEST_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EClass> getClassifiers() {
		if (classifiers == null) {
			classifiers = new EObjectContainmentEList<EClass>(EClass.class, this, TestdefinitionPackage.TEST_MODEL__CLASSIFIERS);
		}
		return classifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestScenario> getScenarios() {
		if (scenarios == null) {
			scenarios = new EObjectContainmentEList<TestScenario>(TestScenario.class, this, TestdefinitionPackage.TEST_MODEL__SCENARIOS);
		}
		return scenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestdefinitionPackage.TEST_MODEL__CLASSIFIERS:
				return ((InternalEList<?>)getClassifiers()).basicRemove(otherEnd, msgs);
			case TestdefinitionPackage.TEST_MODEL__SCENARIOS:
				return ((InternalEList<?>)getScenarios()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestdefinitionPackage.TEST_MODEL__CLASSIFIERS:
				return getClassifiers();
			case TestdefinitionPackage.TEST_MODEL__SCENARIOS:
				return getScenarios();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestdefinitionPackage.TEST_MODEL__CLASSIFIERS:
				getClassifiers().clear();
				getClassifiers().addAll((Collection<? extends EClass>)newValue);
				return;
			case TestdefinitionPackage.TEST_MODEL__SCENARIOS:
				getScenarios().clear();
				getScenarios().addAll((Collection<? extends TestScenario>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestdefinitionPackage.TEST_MODEL__CLASSIFIERS:
				getClassifiers().clear();
				return;
			case TestdefinitionPackage.TEST_MODEL__SCENARIOS:
				getScenarios().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestdefinitionPackage.TEST_MODEL__CLASSIFIERS:
				return classifiers != null && !classifiers.isEmpty();
			case TestdefinitionPackage.TEST_MODEL__SCENARIOS:
				return scenarios != null && !scenarios.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TestModelImpl
