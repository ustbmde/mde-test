/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

import edu.ustb.sei.mde.testing.NamedElement;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.TestModel#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.TestModel#getScenarios <em>Scenarios</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getTestModel()
 * @model
 * @generated
 */
public interface TestModel extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Classifiers</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifiers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifiers</em>' containment reference list.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getTestModel_Classifiers()
	 * @model containment="true"
	 * @generated
	 */
	EList<EClass> getClassifiers();

	/**
	 * Returns the value of the '<em><b>Scenarios</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.testing.testdefinition.TestScenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenarios</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenarios</em>' containment reference list.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getTestModel_Scenarios()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestScenario> getScenarios();

} // TestModel
