/**
 */
package edu.ustb.sei.mde.testing.testdefinition.impl;

import edu.ustb.sei.mde.testing.impl.NamedElementImpl;

import edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition;
import edu.ustb.sei.mde.testing.testdefinition.Oracle;
import edu.ustb.sei.mde.testing.testdefinition.Script;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;
import edu.ustb.sei.mde.testing.testdefinition.Variable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Test Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.impl.AbstractTestDefinitionImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.impl.AbstractTestDefinitionImpl#getAssertion <em>Assertion</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.impl.AbstractTestDefinitionImpl#getDo <em>Do</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractTestDefinitionImpl extends NamedElementImpl implements AbstractTestDefinition {
	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<Variable> variables;

	/**
	 * The cached value of the '{@link #getAssertion() <em>Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertion()
	 * @generated
	 * @ordered
	 */
	protected Oracle assertion;

	/**
	 * The cached value of the '{@link #getDo() <em>Do</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDo()
	 * @generated
	 * @ordered
	 */
	protected Script do_;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractTestDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestdefinitionPackage.Literals.ABSTRACT_TEST_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variable> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList<Variable>(Variable.class, this, TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Oracle getAssertion() {
		return assertion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertion(Oracle newAssertion, NotificationChain msgs) {
		Oracle oldAssertion = assertion;
		assertion = newAssertion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION, oldAssertion, newAssertion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertion(Oracle newAssertion) {
		if (newAssertion != assertion) {
			NotificationChain msgs = null;
			if (assertion != null)
				msgs = ((InternalEObject)assertion).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION, null, msgs);
			if (newAssertion != null)
				msgs = ((InternalEObject)newAssertion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION, null, msgs);
			msgs = basicSetAssertion(newAssertion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION, newAssertion, newAssertion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Script getDo() {
		return do_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDo(Script newDo, NotificationChain msgs) {
		Script oldDo = do_;
		do_ = newDo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO, oldDo, newDo);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDo(Script newDo) {
		if (newDo != do_) {
			NotificationChain msgs = null;
			if (do_ != null)
				msgs = ((InternalEObject)do_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO, null, msgs);
			if (newDo != null)
				msgs = ((InternalEObject)newDo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO, null, msgs);
			msgs = basicSetDo(newDo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO, newDo, newDo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION:
				return basicSetAssertion(null, msgs);
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO:
				return basicSetDo(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__VARIABLES:
				return getVariables();
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION:
				return getAssertion();
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO:
				return getDo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends Variable>)newValue);
				return;
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION:
				setAssertion((Oracle)newValue);
				return;
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO:
				setDo((Script)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__VARIABLES:
				getVariables().clear();
				return;
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION:
				setAssertion((Oracle)null);
				return;
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO:
				setDo((Script)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__VARIABLES:
				return variables != null && !variables.isEmpty();
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION:
				return assertion != null;
			case TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO:
				return do_ != null;
		}
		return super.eIsSet(featureID);
	}

} //AbstractTestDefinitionImpl
