/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage
 * @generated
 */
public interface TestdefinitionFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestdefinitionFactory eINSTANCE = edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Test Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Scenario</em>'.
	 * @generated
	 */
	TestScenario createTestScenario();

	/**
	 * Returns a new object of class '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable</em>'.
	 * @generated
	 */
	Variable createVariable();

	/**
	 * Returns a new object of class '<em>Oracle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Oracle</em>'.
	 * @generated
	 */
	Oracle createOracle();

	/**
	 * Returns a new object of class '<em>Value Range Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Range Specification</em>'.
	 * @generated
	 */
	ValueRangeSpecification createValueRangeSpecification();

	/**
	 * Returns a new object of class '<em>Value Set Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Set Specification</em>'.
	 * @generated
	 */
	ValueSetSpecification createValueSetSpecification();

	/**
	 * Returns a new object of class '<em>Value Script Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Script Specification</em>'.
	 * @generated
	 */
	ValueScriptSpecification createValueScriptSpecification();

	/**
	 * Returns a new object of class '<em>Return Value Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Return Value Specification</em>'.
	 * @generated
	 */
	ReturnValueSpecification createReturnValueSpecification();

	/**
	 * Returns a new object of class '<em>Test Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Model</em>'.
	 * @generated
	 */
	TestModel createTestModel();

	/**
	 * Returns a new object of class '<em>Script</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Script</em>'.
	 * @generated
	 */
	Script createScript();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TestdefinitionPackage getTestdefinitionPackage();

} //TestdefinitionFactory
