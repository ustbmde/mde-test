/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Oracle</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getOracle()
 * @model
 * @generated
 */
public interface Oracle extends Script {

} // Oracle
