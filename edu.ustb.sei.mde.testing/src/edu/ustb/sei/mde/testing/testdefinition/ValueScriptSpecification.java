/**
 */
package edu.ustb.sei.mde.testing.testdefinition;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Script Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getValueScriptSpecification()
 * @model
 * @generated
 */
public interface ValueScriptSpecification extends ValueSpecification, Script {

} // ValueScriptSpecification
