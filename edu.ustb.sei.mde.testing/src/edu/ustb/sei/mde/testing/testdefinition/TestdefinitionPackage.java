/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

import edu.ustb.sei.mde.testing.TestingPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory
 * @model kind="package"
 * @generated
 */
public interface TestdefinitionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "testdefinition";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/testing/testdefinition";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "testdefinition";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestdefinitionPackage eINSTANCE = edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.AbstractTestDefinitionImpl <em>Abstract Test Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.AbstractTestDefinitionImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getAbstractTestDefinition()
	 * @generated
	 */
	int ABSTRACT_TEST_DEFINITION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TEST_DEFINITION__NAME = TestingPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TEST_DEFINITION__VARIABLES = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TEST_DEFINITION__ASSERTION = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Do</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TEST_DEFINITION__DO = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Abstract Test Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TEST_DEFINITION_FEATURE_COUNT = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Abstract Test Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TEST_DEFINITION_OPERATION_COUNT = TestingPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.TestScenarioImpl <em>Test Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestScenarioImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getTestScenario()
	 * @generated
	 */
	int TEST_SCENARIO = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SCENARIO__NAME = ABSTRACT_TEST_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SCENARIO__VARIABLES = ABSTRACT_TEST_DEFINITION__VARIABLES;

	/**
	 * The feature id for the '<em><b>Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SCENARIO__ASSERTION = ABSTRACT_TEST_DEFINITION__ASSERTION;

	/**
	 * The feature id for the '<em><b>Do</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SCENARIO__DO = ABSTRACT_TEST_DEFINITION__DO;

	/**
	 * The number of structural features of the '<em>Test Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SCENARIO_FEATURE_COUNT = ABSTRACT_TEST_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Test Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_SCENARIO_OPERATION_COUNT = ABSTRACT_TEST_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.VariableImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = TestingPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__VALUE = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__TYPE = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_OPERATION_COUNT = TestingPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ScriptImpl <em>Script</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ScriptImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getScript()
	 * @generated
	 */
	int SCRIPT = 9;

	/**
	 * The feature id for the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT__SCRIPT = 0;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT__LANGUAGE = 1;

	/**
	 * The number of structural features of the '<em>Script</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Script</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.OracleImpl <em>Oracle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.OracleImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getOracle()
	 * @generated
	 */
	int ORACLE = 2;

	/**
	 * The feature id for the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORACLE__SCRIPT = SCRIPT__SCRIPT;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORACLE__LANGUAGE = SCRIPT__LANGUAGE;

	/**
	 * The number of structural features of the '<em>Oracle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORACLE_FEATURE_COUNT = SCRIPT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Oracle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORACLE_OPERATION_COUNT = SCRIPT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueSpecificationImpl <em>Value Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueSpecificationImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueSpecification()
	 * @generated
	 */
	int VALUE_SPECIFICATION = 3;

	/**
	 * The number of structural features of the '<em>Value Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SPECIFICATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Value Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueRangeSpecificationImpl <em>Value Range Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueRangeSpecificationImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueRangeSpecification()
	 * @generated
	 */
	int VALUE_RANGE_SPECIFICATION = 4;

	/**
	 * The feature id for the '<em><b>Min Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_RANGE_SPECIFICATION__MIN_LITERAL = VALUE_SPECIFICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_RANGE_SPECIFICATION__MAX_LITERAL = VALUE_SPECIFICATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Value Range Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_RANGE_SPECIFICATION_FEATURE_COUNT = VALUE_SPECIFICATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Value Range Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_RANGE_SPECIFICATION_OPERATION_COUNT = VALUE_SPECIFICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueSetSpecificationImpl <em>Value Set Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueSetSpecificationImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueSetSpecification()
	 * @generated
	 */
	int VALUE_SET_SPECIFICATION = 5;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SET_SPECIFICATION__LITERALS = VALUE_SPECIFICATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Value Set Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SET_SPECIFICATION_FEATURE_COUNT = VALUE_SPECIFICATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Value Set Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SET_SPECIFICATION_OPERATION_COUNT = VALUE_SPECIFICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueScriptSpecificationImpl <em>Value Script Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueScriptSpecificationImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueScriptSpecification()
	 * @generated
	 */
	int VALUE_SCRIPT_SPECIFICATION = 6;

	/**
	 * The feature id for the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SCRIPT_SPECIFICATION__SCRIPT = VALUE_SPECIFICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SCRIPT_SPECIFICATION__LANGUAGE = VALUE_SPECIFICATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Value Script Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SCRIPT_SPECIFICATION_FEATURE_COUNT = VALUE_SPECIFICATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Value Script Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_SCRIPT_SPECIFICATION_OPERATION_COUNT = VALUE_SPECIFICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ReturnValueSpecificationImpl <em>Return Value Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ReturnValueSpecificationImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getReturnValueSpecification()
	 * @generated
	 */
	int RETURN_VALUE_SPECIFICATION = 7;

	/**
	 * The feature id for the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_VALUE_SPECIFICATION__INTERFACE = VALUE_SPECIFICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_VALUE_SPECIFICATION__OPERATION = VALUE_SPECIFICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Actual Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS = VALUE_SPECIFICATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Return Value Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_VALUE_SPECIFICATION_FEATURE_COUNT = VALUE_SPECIFICATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Return Value Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_VALUE_SPECIFICATION_OPERATION_COUNT = VALUE_SPECIFICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.TestModelImpl <em>Test Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestModelImpl
	 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getTestModel()
	 * @generated
	 */
	int TEST_MODEL = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL__NAME = TestingPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Classifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL__CLASSIFIERS = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Scenarios</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL__SCENARIOS = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Test Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL_FEATURE_COUNT = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Test Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL_OPERATION_COUNT = TestingPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.TestScenario <em>Test Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Scenario</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestScenario
	 * @generated
	 */
	EClass getTestScenario();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.testing.testdefinition.Variable#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.Variable#getValue()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_Value();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.testing.testdefinition.Variable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.Variable#getType()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_Type();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.Oracle <em>Oracle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Oracle</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.Oracle
	 * @generated
	 */
	EClass getOracle();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.ValueSpecification <em>Value Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Specification</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ValueSpecification
	 * @generated
	 */
	EClass getValueSpecification();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification <em>Value Range Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Range Specification</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification
	 * @generated
	 */
	EClass getValueRangeSpecification();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMinLiteral <em>Min Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Literal</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMinLiteral()
	 * @see #getValueRangeSpecification()
	 * @generated
	 */
	EAttribute getValueRangeSpecification_MinLiteral();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMaxLiteral <em>Max Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Literal</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification#getMaxLiteral()
	 * @see #getValueRangeSpecification()
	 * @generated
	 */
	EAttribute getValueRangeSpecification_MaxLiteral();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification <em>Value Set Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Set Specification</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification
	 * @generated
	 */
	EClass getValueSetSpecification();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Literals</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification#getLiterals()
	 * @see #getValueSetSpecification()
	 * @generated
	 */
	EAttribute getValueSetSpecification_Literals();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification <em>Value Script Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Script Specification</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification
	 * @generated
	 */
	EClass getValueScriptSpecification();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification <em>Return Value Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Value Specification</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification
	 * @generated
	 */
	EClass getReturnValueSpecification();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interface</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getInterface()
	 * @see #getReturnValueSpecification()
	 * @generated
	 */
	EReference getReturnValueSpecification_Interface();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getOperation()
	 * @see #getReturnValueSpecification()
	 * @generated
	 */
	EReference getReturnValueSpecification_Operation();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getActualParameters <em>Actual Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Actual Parameters</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification#getActualParameters()
	 * @see #getReturnValueSpecification()
	 * @generated
	 */
	EReference getReturnValueSpecification_ActualParameters();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.TestModel <em>Test Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Model</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestModel
	 * @generated
	 */
	EClass getTestModel();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.testing.testdefinition.TestModel#getClassifiers <em>Classifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classifiers</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestModel#getClassifiers()
	 * @see #getTestModel()
	 * @generated
	 */
	EReference getTestModel_Classifiers();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.testing.testdefinition.TestModel#getScenarios <em>Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scenarios</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestModel#getScenarios()
	 * @see #getTestModel()
	 * @generated
	 */
	EReference getTestModel_Scenarios();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.Script <em>Script</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Script</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.Script
	 * @generated
	 */
	EClass getScript();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.testing.testdefinition.Script#getScript <em>Script</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Script</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.Script#getScript()
	 * @see #getScript()
	 * @generated
	 */
	EAttribute getScript_Script();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.testing.testdefinition.Script#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.Script#getLanguage()
	 * @see #getScript()
	 * @generated
	 */
	EAttribute getScript_Language();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition <em>Abstract Test Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Test Definition</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition
	 * @generated
	 */
	EClass getAbstractTestDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getVariables()
	 * @see #getAbstractTestDefinition()
	 * @generated
	 */
	EReference getAbstractTestDefinition_Variables();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getAssertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assertion</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getAssertion()
	 * @see #getAbstractTestDefinition()
	 * @generated
	 */
	EReference getAbstractTestDefinition_Assertion();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getDo <em>Do</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Do</em>'.
	 * @see edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getDo()
	 * @see #getAbstractTestDefinition()
	 * @generated
	 */
	EReference getAbstractTestDefinition_Do();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TestdefinitionFactory getTestdefinitionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.TestScenarioImpl <em>Test Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestScenarioImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getTestScenario()
		 * @generated
		 */
		EClass TEST_SCENARIO = eINSTANCE.getTestScenario();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.VariableImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE__VALUE = eINSTANCE.getVariable_Value();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE__TYPE = eINSTANCE.getVariable_Type();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.OracleImpl <em>Oracle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.OracleImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getOracle()
		 * @generated
		 */
		EClass ORACLE = eINSTANCE.getOracle();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueSpecificationImpl <em>Value Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueSpecificationImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueSpecification()
		 * @generated
		 */
		EClass VALUE_SPECIFICATION = eINSTANCE.getValueSpecification();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueRangeSpecificationImpl <em>Value Range Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueRangeSpecificationImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueRangeSpecification()
		 * @generated
		 */
		EClass VALUE_RANGE_SPECIFICATION = eINSTANCE.getValueRangeSpecification();

		/**
		 * The meta object literal for the '<em><b>Min Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_RANGE_SPECIFICATION__MIN_LITERAL = eINSTANCE.getValueRangeSpecification_MinLiteral();

		/**
		 * The meta object literal for the '<em><b>Max Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_RANGE_SPECIFICATION__MAX_LITERAL = eINSTANCE.getValueRangeSpecification_MaxLiteral();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueSetSpecificationImpl <em>Value Set Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueSetSpecificationImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueSetSpecification()
		 * @generated
		 */
		EClass VALUE_SET_SPECIFICATION = eINSTANCE.getValueSetSpecification();

		/**
		 * The meta object literal for the '<em><b>Literals</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_SET_SPECIFICATION__LITERALS = eINSTANCE.getValueSetSpecification_Literals();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ValueScriptSpecificationImpl <em>Value Script Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ValueScriptSpecificationImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getValueScriptSpecification()
		 * @generated
		 */
		EClass VALUE_SCRIPT_SPECIFICATION = eINSTANCE.getValueScriptSpecification();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ReturnValueSpecificationImpl <em>Return Value Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ReturnValueSpecificationImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getReturnValueSpecification()
		 * @generated
		 */
		EClass RETURN_VALUE_SPECIFICATION = eINSTANCE.getReturnValueSpecification();

		/**
		 * The meta object literal for the '<em><b>Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_VALUE_SPECIFICATION__INTERFACE = eINSTANCE.getReturnValueSpecification_Interface();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_VALUE_SPECIFICATION__OPERATION = eINSTANCE.getReturnValueSpecification_Operation();

		/**
		 * The meta object literal for the '<em><b>Actual Parameters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS = eINSTANCE.getReturnValueSpecification_ActualParameters();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.TestModelImpl <em>Test Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestModelImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getTestModel()
		 * @generated
		 */
		EClass TEST_MODEL = eINSTANCE.getTestModel();

		/**
		 * The meta object literal for the '<em><b>Classifiers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_MODEL__CLASSIFIERS = eINSTANCE.getTestModel_Classifiers();

		/**
		 * The meta object literal for the '<em><b>Scenarios</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_MODEL__SCENARIOS = eINSTANCE.getTestModel_Scenarios();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.ScriptImpl <em>Script</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.ScriptImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getScript()
		 * @generated
		 */
		EClass SCRIPT = eINSTANCE.getScript();

		/**
		 * The meta object literal for the '<em><b>Script</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT__SCRIPT = eINSTANCE.getScript_Script();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT__LANGUAGE = eINSTANCE.getScript_Language();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testdefinition.impl.AbstractTestDefinitionImpl <em>Abstract Test Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.AbstractTestDefinitionImpl
		 * @see edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl#getAbstractTestDefinition()
		 * @generated
		 */
		EClass ABSTRACT_TEST_DEFINITION = eINSTANCE.getAbstractTestDefinition();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_TEST_DEFINITION__VARIABLES = eINSTANCE.getAbstractTestDefinition_Variables();

		/**
		 * The meta object literal for the '<em><b>Assertion</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_TEST_DEFINITION__ASSERTION = eINSTANCE.getAbstractTestDefinition_Assertion();

		/**
		 * The meta object literal for the '<em><b>Do</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_TEST_DEFINITION__DO = eINSTANCE.getAbstractTestDefinition_Do();

	}

} //TestdefinitionPackage
