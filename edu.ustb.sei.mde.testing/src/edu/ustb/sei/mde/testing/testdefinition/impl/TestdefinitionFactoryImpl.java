/**
 */
package edu.ustb.sei.mde.testing.testdefinition.impl;

import edu.ustb.sei.mde.testing.testdefinition.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestdefinitionFactoryImpl extends EFactoryImpl implements TestdefinitionFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TestdefinitionFactory init() {
		try {
			TestdefinitionFactory theTestdefinitionFactory = (TestdefinitionFactory)EPackage.Registry.INSTANCE.getEFactory(TestdefinitionPackage.eNS_URI);
			if (theTestdefinitionFactory != null) {
				return theTestdefinitionFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TestdefinitionFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestdefinitionFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TestdefinitionPackage.TEST_SCENARIO: return createTestScenario();
			case TestdefinitionPackage.VARIABLE: return createVariable();
			case TestdefinitionPackage.ORACLE: return createOracle();
			case TestdefinitionPackage.VALUE_RANGE_SPECIFICATION: return createValueRangeSpecification();
			case TestdefinitionPackage.VALUE_SET_SPECIFICATION: return createValueSetSpecification();
			case TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION: return createValueScriptSpecification();
			case TestdefinitionPackage.RETURN_VALUE_SPECIFICATION: return createReturnValueSpecification();
			case TestdefinitionPackage.TEST_MODEL: return createTestModel();
			case TestdefinitionPackage.SCRIPT: return createScript();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestScenario createTestScenario() {
		TestScenarioImpl testScenario = new TestScenarioImpl();
		return testScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable createVariable() {
		VariableImpl variable = new VariableImpl();
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Oracle createOracle() {
		OracleImpl oracle = new OracleImpl();
		return oracle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueRangeSpecification createValueRangeSpecification() {
		ValueRangeSpecificationImpl valueRangeSpecification = new ValueRangeSpecificationImpl();
		return valueRangeSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueSetSpecification createValueSetSpecification() {
		ValueSetSpecificationImpl valueSetSpecification = new ValueSetSpecificationImpl();
		return valueSetSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueScriptSpecification createValueScriptSpecification() {
		ValueScriptSpecificationImpl valueScriptSpecification = new ValueScriptSpecificationImpl();
		return valueScriptSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnValueSpecification createReturnValueSpecification() {
		ReturnValueSpecificationImpl returnValueSpecification = new ReturnValueSpecificationImpl();
		return returnValueSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestModel createTestModel() {
		TestModelImpl testModel = new TestModelImpl();
		return testModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Script createScript() {
		ScriptImpl script = new ScriptImpl();
		return script;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestdefinitionPackage getTestdefinitionPackage() {
		return (TestdefinitionPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TestdefinitionPackage getPackage() {
		return TestdefinitionPackage.eINSTANCE;
	}

} //TestdefinitionFactoryImpl
