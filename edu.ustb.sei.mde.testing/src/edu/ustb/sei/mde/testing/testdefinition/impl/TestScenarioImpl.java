/**
 */
package edu.ustb.sei.mde.testing.testdefinition.impl;

import edu.ustb.sei.mde.testing.testdefinition.TestScenario;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TestScenarioImpl extends AbstractTestDefinitionImpl implements TestScenario {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestdefinitionPackage.Literals.TEST_SCENARIO;
	}

} //TestScenarioImpl
