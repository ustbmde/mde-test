/**
 */
package edu.ustb.sei.mde.testing.testdefinition;

import edu.ustb.sei.mde.testing.NamedElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Test Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getVariables <em>Variables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getAssertion <em>Assertion</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getDo <em>Do</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getAbstractTestDefinition()
 * @model abstract="true"
 * @generated
 */
public interface AbstractTestDefinition extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.testing.testdefinition.Variable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getAbstractTestDefinition_Variables()
	 * @model containment="true"
	 * @generated
	 */
	EList<Variable> getVariables();

	/**
	 * Returns the value of the '<em><b>Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion</em>' containment reference.
	 * @see #setAssertion(Oracle)
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getAbstractTestDefinition_Assertion()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Oracle getAssertion();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getAssertion <em>Assertion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion</em>' containment reference.
	 * @see #getAssertion()
	 * @generated
	 */
	void setAssertion(Oracle value);

	/**
	 * Returns the value of the '<em><b>Do</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do</em>' containment reference.
	 * @see #setDo(Script)
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#getAbstractTestDefinition_Do()
	 * @model containment="true"
	 * @generated
	 */
	Script getDo();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition#getDo <em>Do</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do</em>' containment reference.
	 * @see #getDo()
	 * @generated
	 */
	void setDo(Script value);

} // AbstractTestDefinition
