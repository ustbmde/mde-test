/**
 */
package edu.ustb.sei.mde.testing.testdefinition.impl;

import edu.ustb.sei.mde.testing.TestingPackage;

import edu.ustb.sei.mde.testing.impl.TestingPackageImpl;

import edu.ustb.sei.mde.testing.testcase.TestcasePackage;

import edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl;

import edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition;
import edu.ustb.sei.mde.testing.testdefinition.Oracle;
import edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification;
import edu.ustb.sei.mde.testing.testdefinition.Script;
import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.TestScenario;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;
import edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification;
import edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification;
import edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification;
import edu.ustb.sei.mde.testing.testdefinition.ValueSpecification;
import edu.ustb.sei.mde.testing.testdefinition.Variable;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestdefinitionPackageImpl extends EPackageImpl implements TestdefinitionPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testScenarioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass oracleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueRangeSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueSetSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueScriptSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass returnValueSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scriptEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractTestDefinitionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TestdefinitionPackageImpl() {
		super(eNS_URI, TestdefinitionFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TestdefinitionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TestdefinitionPackage init() {
		if (isInited) return (TestdefinitionPackage)EPackage.Registry.INSTANCE.getEPackage(TestdefinitionPackage.eNS_URI);

		// Obtain or create and register package
		TestdefinitionPackageImpl theTestdefinitionPackage = (TestdefinitionPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TestdefinitionPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TestdefinitionPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		TestingPackageImpl theTestingPackage = (TestingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TestingPackage.eNS_URI) instanceof TestingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TestingPackage.eNS_URI) : TestingPackage.eINSTANCE);
		TestcasePackageImpl theTestcasePackage = (TestcasePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TestcasePackage.eNS_URI) instanceof TestcasePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TestcasePackage.eNS_URI) : TestcasePackage.eINSTANCE);

		// Create package meta-data objects
		theTestdefinitionPackage.createPackageContents();
		theTestingPackage.createPackageContents();
		theTestcasePackage.createPackageContents();

		// Initialize created meta-data
		theTestdefinitionPackage.initializePackageContents();
		theTestingPackage.initializePackageContents();
		theTestcasePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTestdefinitionPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TestdefinitionPackage.eNS_URI, theTestdefinitionPackage);
		return theTestdefinitionPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestScenario() {
		return testScenarioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariable() {
		return variableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariable_Value() {
		return (EReference)variableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariable_Type() {
		return (EReference)variableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOracle() {
		return oracleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueSpecification() {
		return valueSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueRangeSpecification() {
		return valueRangeSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueRangeSpecification_MinLiteral() {
		return (EAttribute)valueRangeSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueRangeSpecification_MaxLiteral() {
		return (EAttribute)valueRangeSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueSetSpecification() {
		return valueSetSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueSetSpecification_Literals() {
		return (EAttribute)valueSetSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueScriptSpecification() {
		return valueScriptSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReturnValueSpecification() {
		return returnValueSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReturnValueSpecification_Interface() {
		return (EReference)returnValueSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReturnValueSpecification_Operation() {
		return (EReference)returnValueSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReturnValueSpecification_ActualParameters() {
		return (EReference)returnValueSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestModel() {
		return testModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestModel_Classifiers() {
		return (EReference)testModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestModel_Scenarios() {
		return (EReference)testModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScript() {
		return scriptEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScript_Script() {
		return (EAttribute)scriptEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScript_Language() {
		return (EAttribute)scriptEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractTestDefinition() {
		return abstractTestDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractTestDefinition_Variables() {
		return (EReference)abstractTestDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractTestDefinition_Assertion() {
		return (EReference)abstractTestDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractTestDefinition_Do() {
		return (EReference)abstractTestDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestdefinitionFactory getTestdefinitionFactory() {
		return (TestdefinitionFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		testScenarioEClass = createEClass(TEST_SCENARIO);

		variableEClass = createEClass(VARIABLE);
		createEReference(variableEClass, VARIABLE__VALUE);
		createEReference(variableEClass, VARIABLE__TYPE);

		oracleEClass = createEClass(ORACLE);

		valueSpecificationEClass = createEClass(VALUE_SPECIFICATION);

		valueRangeSpecificationEClass = createEClass(VALUE_RANGE_SPECIFICATION);
		createEAttribute(valueRangeSpecificationEClass, VALUE_RANGE_SPECIFICATION__MIN_LITERAL);
		createEAttribute(valueRangeSpecificationEClass, VALUE_RANGE_SPECIFICATION__MAX_LITERAL);

		valueSetSpecificationEClass = createEClass(VALUE_SET_SPECIFICATION);
		createEAttribute(valueSetSpecificationEClass, VALUE_SET_SPECIFICATION__LITERALS);

		valueScriptSpecificationEClass = createEClass(VALUE_SCRIPT_SPECIFICATION);

		returnValueSpecificationEClass = createEClass(RETURN_VALUE_SPECIFICATION);
		createEReference(returnValueSpecificationEClass, RETURN_VALUE_SPECIFICATION__INTERFACE);
		createEReference(returnValueSpecificationEClass, RETURN_VALUE_SPECIFICATION__OPERATION);
		createEReference(returnValueSpecificationEClass, RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS);

		testModelEClass = createEClass(TEST_MODEL);
		createEReference(testModelEClass, TEST_MODEL__CLASSIFIERS);
		createEReference(testModelEClass, TEST_MODEL__SCENARIOS);

		scriptEClass = createEClass(SCRIPT);
		createEAttribute(scriptEClass, SCRIPT__SCRIPT);
		createEAttribute(scriptEClass, SCRIPT__LANGUAGE);

		abstractTestDefinitionEClass = createEClass(ABSTRACT_TEST_DEFINITION);
		createEReference(abstractTestDefinitionEClass, ABSTRACT_TEST_DEFINITION__VARIABLES);
		createEReference(abstractTestDefinitionEClass, ABSTRACT_TEST_DEFINITION__ASSERTION);
		createEReference(abstractTestDefinitionEClass, ABSTRACT_TEST_DEFINITION__DO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TestingPackage theTestingPackage = (TestingPackage)EPackage.Registry.INSTANCE.getEPackage(TestingPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		testScenarioEClass.getESuperTypes().add(this.getAbstractTestDefinition());
		variableEClass.getESuperTypes().add(theTestingPackage.getNamedElement());
		oracleEClass.getESuperTypes().add(this.getScript());
		valueRangeSpecificationEClass.getESuperTypes().add(this.getValueSpecification());
		valueSetSpecificationEClass.getESuperTypes().add(this.getValueSpecification());
		valueScriptSpecificationEClass.getESuperTypes().add(this.getValueSpecification());
		valueScriptSpecificationEClass.getESuperTypes().add(this.getScript());
		returnValueSpecificationEClass.getESuperTypes().add(this.getValueSpecification());
		testModelEClass.getESuperTypes().add(theTestingPackage.getNamedElement());
		abstractTestDefinitionEClass.getESuperTypes().add(theTestingPackage.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(testScenarioEClass, TestScenario.class, "TestScenario", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(variableEClass, Variable.class, "Variable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariable_Value(), this.getValueSpecification(), null, "value", null, 1, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariable_Type(), ecorePackage.getEClassifier(), null, "type", null, 1, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(oracleEClass, Oracle.class, "Oracle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(valueSpecificationEClass, ValueSpecification.class, "ValueSpecification", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(valueRangeSpecificationEClass, ValueRangeSpecification.class, "ValueRangeSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueRangeSpecification_MinLiteral(), ecorePackage.getEString(), "minLiteral", null, 1, 1, ValueRangeSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getValueRangeSpecification_MaxLiteral(), ecorePackage.getEString(), "maxLiteral", null, 1, 1, ValueRangeSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueSetSpecificationEClass, ValueSetSpecification.class, "ValueSetSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueSetSpecification_Literals(), ecorePackage.getEString(), "literals", null, 1, -1, ValueSetSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueScriptSpecificationEClass, ValueScriptSpecification.class, "ValueScriptSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(returnValueSpecificationEClass, ReturnValueSpecification.class, "ReturnValueSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReturnValueSpecification_Interface(), ecorePackage.getEClass(), null, "interface", null, 1, 1, ReturnValueSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReturnValueSpecification_Operation(), ecorePackage.getEOperation(), null, "operation", null, 1, 1, ReturnValueSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReturnValueSpecification_ActualParameters(), this.getVariable(), null, "actualParameters", null, 0, -1, ReturnValueSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testModelEClass, TestModel.class, "TestModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestModel_Classifiers(), ecorePackage.getEClass(), null, "classifiers", null, 0, -1, TestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestModel_Scenarios(), this.getTestScenario(), null, "scenarios", null, 0, -1, TestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scriptEClass, Script.class, "Script", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScript_Script(), ecorePackage.getEString(), "script", null, 1, 1, Script.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScript_Language(), ecorePackage.getEString(), "language", null, 0, 1, Script.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractTestDefinitionEClass, AbstractTestDefinition.class, "AbstractTestDefinition", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractTestDefinition_Variables(), this.getVariable(), null, "variables", null, 0, -1, AbstractTestDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractTestDefinition_Assertion(), this.getOracle(), null, "assertion", null, 1, 1, AbstractTestDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractTestDefinition_Do(), this.getScript(), null, "do", null, 0, 1, AbstractTestDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //TestdefinitionPackageImpl
