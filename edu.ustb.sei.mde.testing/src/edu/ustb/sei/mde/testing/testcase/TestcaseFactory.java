/**
 */
package edu.ustb.sei.mde.testing.testcase;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage
 * @generated
 */
public interface TestcaseFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestcaseFactory eINSTANCE = edu.ustb.sei.mde.testing.testcase.impl.TestcaseFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Test Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case</em>'.
	 * @generated
	 */
	TestCase createTestCase();

	/**
	 * Returns a new object of class '<em>Test Case Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Test Case Model</em>'.
	 * @generated
	 */
	TestCaseModel createTestCaseModel();

	/**
	 * Returns a new object of class '<em>Value Literal Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Literal Specification</em>'.
	 * @generated
	 */
	ValueLiteralSpecification createValueLiteralSpecification();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TestcasePackage getTestcasePackage();

} //TestcaseFactory
