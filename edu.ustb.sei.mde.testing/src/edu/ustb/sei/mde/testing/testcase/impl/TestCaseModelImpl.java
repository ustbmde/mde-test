/**
 */
package edu.ustb.sei.mde.testing.testcase.impl;

import edu.ustb.sei.mde.testing.impl.NamedElementImpl;

import edu.ustb.sei.mde.testing.testcase.TestCase;
import edu.ustb.sei.mde.testing.testcase.TestCaseModel;
import edu.ustb.sei.mde.testing.testcase.TestcasePackage;

import edu.ustb.sei.mde.testing.testdefinition.TestModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Case Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.impl.TestCaseModelImpl#getTestcases <em>Testcases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.impl.TestCaseModelImpl#getTestmodel <em>Testmodel</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.impl.TestCaseModelImpl#getEPackage <em>EPackage</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestCaseModelImpl extends NamedElementImpl implements TestCaseModel {
	/**
	 * The cached value of the '{@link #getTestcases() <em>Testcases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestcases()
	 * @generated
	 * @ordered
	 */
	protected EList<TestCase> testcases;

	/**
	 * The cached value of the '{@link #getTestmodel() <em>Testmodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestmodel()
	 * @generated
	 * @ordered
	 */
	protected TestModel testmodel;

	/**
	 * The cached value of the '{@link #getEPackage() <em>EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEPackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage ePackage;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestCaseModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestcasePackage.Literals.TEST_CASE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TestCase> getTestcases() {
		if (testcases == null) {
			testcases = new EObjectContainmentEList<TestCase>(TestCase.class, this, TestcasePackage.TEST_CASE_MODEL__TESTCASES);
		}
		return testcases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestModel getTestmodel() {
		if (testmodel != null && testmodel.eIsProxy()) {
			InternalEObject oldTestmodel = (InternalEObject)testmodel;
			testmodel = (TestModel)eResolveProxy(oldTestmodel);
			if (testmodel != oldTestmodel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TestcasePackage.TEST_CASE_MODEL__TESTMODEL, oldTestmodel, testmodel));
			}
		}
		return testmodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestModel basicGetTestmodel() {
		return testmodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestmodel(TestModel newTestmodel) {
		TestModel oldTestmodel = testmodel;
		testmodel = newTestmodel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.TEST_CASE_MODEL__TESTMODEL, oldTestmodel, testmodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getEPackage() {
		if (ePackage != null && ePackage.eIsProxy()) {
			InternalEObject oldEPackage = (InternalEObject)ePackage;
			ePackage = (EPackage)eResolveProxy(oldEPackage);
			if (ePackage != oldEPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TestcasePackage.TEST_CASE_MODEL__EPACKAGE, oldEPackage, ePackage));
			}
		}
		return ePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetEPackage() {
		return ePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEPackage(EPackage newEPackage) {
		EPackage oldEPackage = ePackage;
		ePackage = newEPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestcasePackage.TEST_CASE_MODEL__EPACKAGE, oldEPackage, ePackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE_MODEL__TESTCASES:
				return ((InternalEList<?>)getTestcases()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE_MODEL__TESTCASES:
				return getTestcases();
			case TestcasePackage.TEST_CASE_MODEL__TESTMODEL:
				if (resolve) return getTestmodel();
				return basicGetTestmodel();
			case TestcasePackage.TEST_CASE_MODEL__EPACKAGE:
				if (resolve) return getEPackage();
				return basicGetEPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE_MODEL__TESTCASES:
				getTestcases().clear();
				getTestcases().addAll((Collection<? extends TestCase>)newValue);
				return;
			case TestcasePackage.TEST_CASE_MODEL__TESTMODEL:
				setTestmodel((TestModel)newValue);
				return;
			case TestcasePackage.TEST_CASE_MODEL__EPACKAGE:
				setEPackage((EPackage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE_MODEL__TESTCASES:
				getTestcases().clear();
				return;
			case TestcasePackage.TEST_CASE_MODEL__TESTMODEL:
				setTestmodel((TestModel)null);
				return;
			case TestcasePackage.TEST_CASE_MODEL__EPACKAGE:
				setEPackage((EPackage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestcasePackage.TEST_CASE_MODEL__TESTCASES:
				return testcases != null && !testcases.isEmpty();
			case TestcasePackage.TEST_CASE_MODEL__TESTMODEL:
				return testmodel != null;
			case TestcasePackage.TEST_CASE_MODEL__EPACKAGE:
				return ePackage != null;
		}
		return super.eIsSet(featureID);
	}

} //TestCaseModelImpl
