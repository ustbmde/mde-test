/**
 */
package edu.ustb.sei.mde.testing.testcase;

import edu.ustb.sei.mde.testing.TestingPackage;

import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.testing.testcase.TestcaseFactory
 * @model kind="package"
 * @generated
 */
public interface TestcasePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "testcase";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/testing/testcase";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "testcase";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestcasePackage eINSTANCE = edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testcase.impl.TestCaseImpl <em>Test Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testcase.impl.TestCaseImpl
	 * @see edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl#getTestCase()
	 * @generated
	 */
	int TEST_CASE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__NAME = TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__VARIABLES = TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__VARIABLES;

	/**
	 * The feature id for the '<em><b>Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__ASSERTION = TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__ASSERTION;

	/**
	 * The feature id for the '<em><b>Do</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__DO = TestdefinitionPackage.ABSTRACT_TEST_DEFINITION__DO;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE__SCENARIO = TestdefinitionPackage.ABSTRACT_TEST_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_FEATURE_COUNT = TestdefinitionPackage.ABSTRACT_TEST_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Test Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_OPERATION_COUNT = TestdefinitionPackage.ABSTRACT_TEST_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testcase.impl.TestCaseModelImpl <em>Test Case Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testcase.impl.TestCaseModelImpl
	 * @see edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl#getTestCaseModel()
	 * @generated
	 */
	int TEST_CASE_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_MODEL__NAME = TestingPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Testcases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_MODEL__TESTCASES = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Testmodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_MODEL__TESTMODEL = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_MODEL__EPACKAGE = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Test Case Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_MODEL_FEATURE_COUNT = TestingPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Test Case Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASE_MODEL_OPERATION_COUNT = TestingPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.testing.testcase.impl.ValueLiteralSpecificationImpl <em>Value Literal Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.testing.testcase.impl.ValueLiteralSpecificationImpl
	 * @see edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl#getValueLiteralSpecification()
	 * @generated
	 */
	int VALUE_LITERAL_SPECIFICATION = 2;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LITERAL_SPECIFICATION__LITERAL = TestdefinitionPackage.VALUE_SPECIFICATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Value Literal Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LITERAL_SPECIFICATION_FEATURE_COUNT = TestdefinitionPackage.VALUE_SPECIFICATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Value Literal Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_LITERAL_SPECIFICATION_OPERATION_COUNT = TestdefinitionPackage.VALUE_SPECIFICATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testcase.TestCase <em>Test Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.TestCase
	 * @generated
	 */
	EClass getTestCase();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.testing.testcase.TestCase#getScenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scenario</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.TestCase#getScenario()
	 * @see #getTestCase()
	 * @generated
	 */
	EReference getTestCase_Scenario();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel <em>Test Case Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Case Model</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.TestCaseModel
	 * @generated
	 */
	EClass getTestCaseModel();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getTestcases <em>Testcases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Testcases</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.TestCaseModel#getTestcases()
	 * @see #getTestCaseModel()
	 * @generated
	 */
	EReference getTestCaseModel_Testcases();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getTestmodel <em>Testmodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Testmodel</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.TestCaseModel#getTestmodel()
	 * @see #getTestCaseModel()
	 * @generated
	 */
	EReference getTestCaseModel_Testmodel();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getEPackage <em>EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>EPackage</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.TestCaseModel#getEPackage()
	 * @see #getTestCaseModel()
	 * @generated
	 */
	EReference getTestCaseModel_EPackage();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification <em>Value Literal Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Literal Specification</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification
	 * @generated
	 */
	EClass getValueLiteralSpecification();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Literal</em>'.
	 * @see edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification#getLiteral()
	 * @see #getValueLiteralSpecification()
	 * @generated
	 */
	EAttribute getValueLiteralSpecification_Literal();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TestcaseFactory getTestcaseFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testcase.impl.TestCaseImpl <em>Test Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testcase.impl.TestCaseImpl
		 * @see edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl#getTestCase()
		 * @generated
		 */
		EClass TEST_CASE = eINSTANCE.getTestCase();

		/**
		 * The meta object literal for the '<em><b>Scenario</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE__SCENARIO = eINSTANCE.getTestCase_Scenario();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testcase.impl.TestCaseModelImpl <em>Test Case Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testcase.impl.TestCaseModelImpl
		 * @see edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl#getTestCaseModel()
		 * @generated
		 */
		EClass TEST_CASE_MODEL = eINSTANCE.getTestCaseModel();

		/**
		 * The meta object literal for the '<em><b>Testcases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE_MODEL__TESTCASES = eINSTANCE.getTestCaseModel_Testcases();

		/**
		 * The meta object literal for the '<em><b>Testmodel</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE_MODEL__TESTMODEL = eINSTANCE.getTestCaseModel_Testmodel();

		/**
		 * The meta object literal for the '<em><b>EPackage</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASE_MODEL__EPACKAGE = eINSTANCE.getTestCaseModel_EPackage();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.testing.testcase.impl.ValueLiteralSpecificationImpl <em>Value Literal Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.testing.testcase.impl.ValueLiteralSpecificationImpl
		 * @see edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl#getValueLiteralSpecification()
		 * @generated
		 */
		EClass VALUE_LITERAL_SPECIFICATION = eINSTANCE.getValueLiteralSpecification();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VALUE_LITERAL_SPECIFICATION__LITERAL = eINSTANCE.getValueLiteralSpecification_Literal();

	}

} //TestcasePackage
