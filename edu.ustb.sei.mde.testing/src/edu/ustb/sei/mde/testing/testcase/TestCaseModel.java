/**
 */
package edu.ustb.sei.mde.testing.testcase;

import edu.ustb.sei.mde.testing.NamedElement;

import edu.ustb.sei.mde.testing.testdefinition.TestModel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getTestcases <em>Testcases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getTestmodel <em>Testmodel</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getEPackage <em>EPackage</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getTestCaseModel()
 * @model
 * @generated
 */
public interface TestCaseModel extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Testcases</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.testing.testcase.TestCase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Testcases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Testcases</em>' containment reference list.
	 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getTestCaseModel_Testcases()
	 * @model containment="true"
	 * @generated
	 */
	EList<TestCase> getTestcases();

	/**
	 * Returns the value of the '<em><b>Testmodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Testmodel</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Testmodel</em>' reference.
	 * @see #setTestmodel(TestModel)
	 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getTestCaseModel_Testmodel()
	 * @model
	 * @generated
	 */
	TestModel getTestmodel();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getTestmodel <em>Testmodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Testmodel</em>' reference.
	 * @see #getTestmodel()
	 * @generated
	 */
	void setTestmodel(TestModel value);

	/**
	 * Returns the value of the '<em><b>EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EPackage</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EPackage</em>' reference.
	 * @see #setEPackage(EPackage)
	 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getTestCaseModel_EPackage()
	 * @model
	 * @generated
	 */
	EPackage getEPackage();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testcase.TestCaseModel#getEPackage <em>EPackage</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EPackage</em>' reference.
	 * @see #getEPackage()
	 * @generated
	 */
	void setEPackage(EPackage value);

} // TestCaseModel
