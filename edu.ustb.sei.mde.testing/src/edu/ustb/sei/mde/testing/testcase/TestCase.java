/**
 */
package edu.ustb.sei.mde.testing.testcase;

import edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition;
import edu.ustb.sei.mde.testing.testdefinition.TestScenario;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.TestCase#getScenario <em>Scenario</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getTestCase()
 * @model
 * @generated
 */
public interface TestCase extends AbstractTestDefinition {
	/**
	 * Returns the value of the '<em><b>Scenario</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario</em>' reference.
	 * @see #setScenario(TestScenario)
	 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getTestCase_Scenario()
	 * @model
	 * @generated
	 */
	TestScenario getScenario();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testcase.TestCase#getScenario <em>Scenario</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario</em>' reference.
	 * @see #getScenario()
	 * @generated
	 */
	void setScenario(TestScenario value);

} // TestCase
