/**
 */
package edu.ustb.sei.mde.testing.testcase.impl;

import edu.ustb.sei.mde.testing.testcase.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestcaseFactoryImpl extends EFactoryImpl implements TestcaseFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TestcaseFactory init() {
		try {
			TestcaseFactory theTestcaseFactory = (TestcaseFactory)EPackage.Registry.INSTANCE.getEFactory(TestcasePackage.eNS_URI);
			if (theTestcaseFactory != null) {
				return theTestcaseFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TestcaseFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestcaseFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TestcasePackage.TEST_CASE: return createTestCase();
			case TestcasePackage.TEST_CASE_MODEL: return createTestCaseModel();
			case TestcasePackage.VALUE_LITERAL_SPECIFICATION: return createValueLiteralSpecification();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCase createTestCase() {
		TestCaseImpl testCase = new TestCaseImpl();
		return testCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestCaseModel createTestCaseModel() {
		TestCaseModelImpl testCaseModel = new TestCaseModelImpl();
		return testCaseModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueLiteralSpecification createValueLiteralSpecification() {
		ValueLiteralSpecificationImpl valueLiteralSpecification = new ValueLiteralSpecificationImpl();
		return valueLiteralSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestcasePackage getTestcasePackage() {
		return (TestcasePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TestcasePackage getPackage() {
		return TestcasePackage.eINSTANCE;
	}

} //TestcaseFactoryImpl
