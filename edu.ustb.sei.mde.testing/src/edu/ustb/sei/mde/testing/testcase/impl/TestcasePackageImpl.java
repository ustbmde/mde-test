/**
 */
package edu.ustb.sei.mde.testing.testcase.impl;

import edu.ustb.sei.mde.testing.TestingPackage;

import edu.ustb.sei.mde.testing.impl.TestingPackageImpl;
import edu.ustb.sei.mde.testing.testcase.TestCase;
import edu.ustb.sei.mde.testing.testcase.TestCaseModel;
import edu.ustb.sei.mde.testing.testcase.TestcaseFactory;
import edu.ustb.sei.mde.testing.testcase.TestcasePackage;

import edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;

import edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestcasePackageImpl extends EPackageImpl implements TestcasePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testCaseModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueLiteralSpecificationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TestcasePackageImpl() {
		super(eNS_URI, TestcaseFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TestcasePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TestcasePackage init() {
		if (isInited) return (TestcasePackage)EPackage.Registry.INSTANCE.getEPackage(TestcasePackage.eNS_URI);

		// Obtain or create and register package
		TestcasePackageImpl theTestcasePackage = (TestcasePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TestcasePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TestcasePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		TestingPackageImpl theTestingPackage = (TestingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TestingPackage.eNS_URI) instanceof TestingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TestingPackage.eNS_URI) : TestingPackage.eINSTANCE);
		TestdefinitionPackageImpl theTestdefinitionPackage = (TestdefinitionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TestdefinitionPackage.eNS_URI) instanceof TestdefinitionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TestdefinitionPackage.eNS_URI) : TestdefinitionPackage.eINSTANCE);

		// Create package meta-data objects
		theTestcasePackage.createPackageContents();
		theTestingPackage.createPackageContents();
		theTestdefinitionPackage.createPackageContents();

		// Initialize created meta-data
		theTestcasePackage.initializePackageContents();
		theTestingPackage.initializePackageContents();
		theTestdefinitionPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTestcasePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TestcasePackage.eNS_URI, theTestcasePackage);
		return theTestcasePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCase() {
		return testCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCase_Scenario() {
		return (EReference)testCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestCaseModel() {
		return testCaseModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCaseModel_Testcases() {
		return (EReference)testCaseModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCaseModel_Testmodel() {
		return (EReference)testCaseModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestCaseModel_EPackage() {
		return (EReference)testCaseModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueLiteralSpecification() {
		return valueLiteralSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueLiteralSpecification_Literal() {
		return (EAttribute)valueLiteralSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestcaseFactory getTestcaseFactory() {
		return (TestcaseFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		testCaseEClass = createEClass(TEST_CASE);
		createEReference(testCaseEClass, TEST_CASE__SCENARIO);

		testCaseModelEClass = createEClass(TEST_CASE_MODEL);
		createEReference(testCaseModelEClass, TEST_CASE_MODEL__TESTCASES);
		createEReference(testCaseModelEClass, TEST_CASE_MODEL__TESTMODEL);
		createEReference(testCaseModelEClass, TEST_CASE_MODEL__EPACKAGE);

		valueLiteralSpecificationEClass = createEClass(VALUE_LITERAL_SPECIFICATION);
		createEAttribute(valueLiteralSpecificationEClass, VALUE_LITERAL_SPECIFICATION__LITERAL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TestdefinitionPackage theTestdefinitionPackage = (TestdefinitionPackage)EPackage.Registry.INSTANCE.getEPackage(TestdefinitionPackage.eNS_URI);
		TestingPackage theTestingPackage = (TestingPackage)EPackage.Registry.INSTANCE.getEPackage(TestingPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		testCaseEClass.getESuperTypes().add(theTestdefinitionPackage.getAbstractTestDefinition());
		testCaseModelEClass.getESuperTypes().add(theTestingPackage.getNamedElement());
		valueLiteralSpecificationEClass.getESuperTypes().add(theTestdefinitionPackage.getValueSpecification());

		// Initialize classes, features, and operations; add parameters
		initEClass(testCaseEClass, TestCase.class, "TestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestCase_Scenario(), theTestdefinitionPackage.getTestScenario(), null, "scenario", null, 0, 1, TestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testCaseModelEClass, TestCaseModel.class, "TestCaseModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestCaseModel_Testcases(), this.getTestCase(), null, "testcases", null, 0, -1, TestCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestCaseModel_Testmodel(), theTestdefinitionPackage.getTestModel(), null, "testmodel", null, 0, 1, TestCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestCaseModel_EPackage(), ecorePackage.getEPackage(), null, "ePackage", null, 0, 1, TestCaseModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueLiteralSpecificationEClass, ValueLiteralSpecification.class, "ValueLiteralSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueLiteralSpecification_Literal(), ecorePackage.getEString(), "literal", null, 1, 1, ValueLiteralSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //TestcasePackageImpl
