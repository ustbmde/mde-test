/**
 */
package edu.ustb.sei.mde.testing.testcase;

import edu.ustb.sei.mde.testing.testdefinition.ValueSpecification;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Literal Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification#getLiteral <em>Literal</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getValueLiteralSpecification()
 * @model
 * @generated
 */
public interface ValueLiteralSpecification extends ValueSpecification {
	/**
	 * Returns the value of the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' attribute.
	 * @see #setLiteral(String)
	 * @see edu.ustb.sei.mde.testing.testcase.TestcasePackage#getValueLiteralSpecification_Literal()
	 * @model required="true"
	 * @generated
	 */
	String getLiteral();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification#getLiteral <em>Literal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' attribute.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(String value);

} // ValueLiteralSpecification
