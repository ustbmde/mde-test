/**
 */
package edu.ustb.sei.mde.testing.impl;

import edu.ustb.sei.mde.testing.NamedElement;
import edu.ustb.sei.mde.testing.TestingFactory;
import edu.ustb.sei.mde.testing.TestingPackage;

import edu.ustb.sei.mde.testing.testcase.TestcasePackage;

import edu.ustb.sei.mde.testing.testcase.impl.TestcasePackageImpl;

import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;

import edu.ustb.sei.mde.testing.testdefinition.impl.TestdefinitionPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestingPackageImpl extends EPackageImpl implements TestingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.testing.TestingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TestingPackageImpl() {
		super(eNS_URI, TestingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TestingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TestingPackage init() {
		if (isInited) return (TestingPackage)EPackage.Registry.INSTANCE.getEPackage(TestingPackage.eNS_URI);

		// Obtain or create and register package
		TestingPackageImpl theTestingPackage = (TestingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TestingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TestingPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		TestdefinitionPackageImpl theTestdefinitionPackage = (TestdefinitionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TestdefinitionPackage.eNS_URI) instanceof TestdefinitionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TestdefinitionPackage.eNS_URI) : TestdefinitionPackage.eINSTANCE);
		TestcasePackageImpl theTestcasePackage = (TestcasePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TestcasePackage.eNS_URI) instanceof TestcasePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TestcasePackage.eNS_URI) : TestcasePackage.eINSTANCE);

		// Create package meta-data objects
		theTestingPackage.createPackageContents();
		theTestdefinitionPackage.createPackageContents();
		theTestcasePackage.createPackageContents();

		// Initialize created meta-data
		theTestingPackage.initializePackageContents();
		theTestdefinitionPackage.initializePackageContents();
		theTestcasePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTestingPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TestingPackage.eNS_URI, theTestingPackage);
		return theTestingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestingFactory getTestingFactory() {
		return (TestingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TestdefinitionPackage theTestdefinitionPackage = (TestdefinitionPackage)EPackage.Registry.INSTANCE.getEPackage(TestdefinitionPackage.eNS_URI);
		TestcasePackage theTestcasePackage = (TestcasePackage)EPackage.Registry.INSTANCE.getEPackage(TestcasePackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theTestdefinitionPackage);
		getESubpackages().add(theTestcasePackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //TestingPackageImpl
