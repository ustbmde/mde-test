/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

public class TestmodelOutlinePageExpandAllAction extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.AbstractTestmodelOutlinePageAction {
	
	public TestmodelOutlinePageExpandAllAction(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Expand all", org.eclipse.jface.action.IAction.AS_PUSH_BUTTON);
		initialize("icons/expand_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().expandAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
