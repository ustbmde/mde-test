/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelAntlrTo
 * kenHelper.
 */
public class TestmodelAntlrTokenHelper {
	// This class is intentionally left empty.
}
