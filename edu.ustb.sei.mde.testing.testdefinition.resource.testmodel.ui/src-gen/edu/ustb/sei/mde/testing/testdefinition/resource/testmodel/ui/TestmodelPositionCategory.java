/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

/**
 * An enumeration of all position categories.
 */
public enum TestmodelPositionCategory {
	BRACKET, DEFINTION, PROXY;
}
