/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

public class TestmodelOutlinePageLinkWithEditorAction extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.AbstractTestmodelOutlinePageAction {
	
	public TestmodelOutlinePageLinkWithEditorAction(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Link with Editor", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/link_with_editor_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setLinkWithEditor(on);
	}
	
}
