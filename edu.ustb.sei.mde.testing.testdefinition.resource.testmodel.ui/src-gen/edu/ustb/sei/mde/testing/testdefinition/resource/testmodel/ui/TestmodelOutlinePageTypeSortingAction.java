/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

public class TestmodelOutlinePageTypeSortingAction extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.AbstractTestmodelOutlinePageAction {
	
	public TestmodelOutlinePageTypeSortingAction(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Group types", org.eclipse.jface.action.IAction.AS_CHECK_BOX);
		initialize("icons/group_types_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setGroupTypes(on);
		getTreeViewer().refresh();
	}
	
}
