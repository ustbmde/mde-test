/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

public class TestmodelOutlinePageCollapseAllAction extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.AbstractTestmodelOutlinePageAction {
	
	public TestmodelOutlinePageCollapseAllAction(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Collapse all", org.eclipse.jface.action.IAction.AS_PUSH_BUTTON);
		initialize("icons/collapse_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().collapseAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
