/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

public class TestmodelOutlinePageActionProvider {
	
	public java.util.List<org.eclipse.jface.action.IAction> getActions(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		java.util.List<org.eclipse.jface.action.IAction> defaultActions = new java.util.ArrayList<org.eclipse.jface.action.IAction>();
		defaultActions.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
