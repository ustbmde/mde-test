/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

public class TestmodelHoverTextProvider implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelHoverTextProvider {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelDefaultHoverTextProvider defaultProvider = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelDefaultHoverTextProvider();
	
	public String getHoverText(org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EObject referencedObject) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(referencedObject);
	}
	
	public String getHoverText(org.eclipse.emf.ecore.EObject object) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(object);
	}
	
}
