/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

public class TestmodelUIMetaInformation extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelMetaInformation {
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelHoverTextProvider getHoverTextProvider() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelHoverTextProvider();
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelImageProvider getImageProvider() {
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelImageProvider.INSTANCE;
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelColorManager createColorManager() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.IT
	 * estmodelTextResource,
	 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelColorMana
	 * ger) instead.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelTokenScanner createTokenScanner(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelColorManager colorManager) {
		return createTokenScanner(null, colorManager);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelTokenScanner createTokenScanner(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelColorManager colorManager) {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelTokenScanner(resource, colorManager);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelCodeCompletionHelper createCodeCompletionHelper() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelCodeCompletionHelper();
	}
	
}
