/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

/**
 * A provider for BracketHandler objects.
 */
public interface ITestmodelBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.ITestmodelBracketHandler getBracketHandler();
	
}
