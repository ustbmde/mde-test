/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class TestmodelProposalPostProcessor {
	
	public java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelCompletionProposal> process(java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.TestmodelCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
