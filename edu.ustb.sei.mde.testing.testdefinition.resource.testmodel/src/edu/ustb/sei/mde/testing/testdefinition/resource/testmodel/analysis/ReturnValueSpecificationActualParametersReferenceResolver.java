/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis;

import org.eclipse.emf.ecore.EClass;

import edu.ustb.sei.mde.testing.testcase.TestCase;
import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.TestScenario;
import edu.ustb.sei.mde.testing.testdefinition.Variable;
import edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.ResolverUtil;

public class ReturnValueSpecificationActualParametersReferenceResolver implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable> {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable> delegate = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable>();
	
	public void resolve(String identifier, edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolveResult<edu.ustb.sei.mde.testing.testdefinition.Variable> result) {
		TestScenario ts = ResolverUtil.getTestScenario(container);
		if(ts!=null) {
			for(Variable v : ts.getVariables()) {
				if(v.getName().equals(identifier)){
					result.addMapping(identifier, v);
				}
			}
		} else {
			TestCase tc = ResolverUtil.getTestCase(container);
			if(tc!=null) {
				for(Variable v : tc.getVariables()) {
					if(v.getName().equals(identifier)){
						result.addMapping(identifier, v);
					}
				}
			}
		}
	}
	
	public String deResolve(edu.ustb.sei.mde.testing.testdefinition.Variable element, edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container, org.eclipse.emf.ecore.EReference reference) {
		return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
