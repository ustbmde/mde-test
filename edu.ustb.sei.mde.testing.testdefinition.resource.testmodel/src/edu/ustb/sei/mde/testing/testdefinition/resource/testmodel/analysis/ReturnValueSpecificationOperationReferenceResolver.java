/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis;

import org.eclipse.emf.ecore.EOperation;

public class ReturnValueSpecificationOperationReferenceResolver implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EOperation> {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EOperation> delegate = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EOperation>();
	
	public void resolve(String identifier, edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolveResult<org.eclipse.emf.ecore.EOperation> result) {
		if(container==null) return;
		if(container.getInterface()==null) return;
		for(EOperation op : container.getInterface().getEAllOperations()) {
			if(op.getName().equals(identifier)) {
				result.addMapping(identifier, op);
			}
		}
	}
	
	public String deResolve(org.eclipse.emf.ecore.EOperation element, edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container, org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
