/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis;

import edu.ustb.sei.mde.testing.testcase.TestCaseModel;
import edu.ustb.sei.mde.testing.testdefinition.TestScenario;
import edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.ResolverUtil;

public class TestCaseScenarioReferenceResolver implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testcase.TestCase, edu.ustb.sei.mde.testing.testdefinition.TestScenario> {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testcase.TestCase, edu.ustb.sei.mde.testing.testdefinition.TestScenario> delegate = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testcase.TestCase, edu.ustb.sei.mde.testing.testdefinition.TestScenario>();
	
	public void resolve(String identifier, edu.ustb.sei.mde.testing.testcase.TestCase container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolveResult<edu.ustb.sei.mde.testing.testdefinition.TestScenario> result) {
		TestCaseModel model = ResolverUtil.getTestCaseModel(container);
		if(model==null || model.getTestmodel()==null) return;
		
		for(TestScenario s : model.getTestmodel().getScenarios()) {
			if(s.getName().equals(identifier)) {
				result.addMapping(identifier, s);
				return;
			}
		}
		//delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(edu.ustb.sei.mde.testing.testdefinition.TestScenario element, edu.ustb.sei.mde.testing.testcase.TestCase container, org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
		//return delegate.deResolve(element, container, reference);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
