/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;

import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.ResolverUtil;

public class VariableTypeReferenceResolver implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.Variable, org.eclipse.emf.ecore.EClassifier> {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.Variable, org.eclipse.emf.ecore.EClassifier> delegate = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.Variable, org.eclipse.emf.ecore.EClassifier>();
	
	public void resolve(String identifier, edu.ustb.sei.mde.testing.testdefinition.Variable container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolveResult<org.eclipse.emf.ecore.EClassifier> result) {
		EClassifier t = ResolverUtil.getPrimitiveType(identifier);
		if(t!=null) {
			result.addMapping(identifier, t);
			return;
		}
		TestModel model = ResolverUtil.getTestModel(container);
		t = ResolverUtil.getMessageType(identifier, model);
		if(t!=null) {
			result.addMapping(identifier, t);
			return;
		}
		
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(org.eclipse.emf.ecore.EClassifier element, edu.ustb.sei.mde.testing.testdefinition.Variable container, org.eclipse.emf.ecore.EReference reference) {
		if(element instanceof EDataType) {
			return ResolverUtil.getPrimitiveTypeName((EDataType)element);
		} else 
			return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
