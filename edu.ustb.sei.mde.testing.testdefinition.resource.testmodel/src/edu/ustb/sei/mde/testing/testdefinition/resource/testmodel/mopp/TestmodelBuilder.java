/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLEngineResult.Status;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xml.type.impl.XMLTypeFactoryImpl;

import edu.ustb.sei.mde.testing.testcase.TestCase;
import edu.ustb.sei.mde.testing.testcase.TestCaseModel;
import edu.ustb.sei.mde.testing.testcase.TestcaseFactory;
import edu.ustb.sei.mde.testing.testcase.TestcasePackage;
import edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification;
import edu.ustb.sei.mde.testing.testdefinition.Oracle;
import edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification;
import edu.ustb.sei.mde.testing.testdefinition.Script;
import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.TestScenario;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory;
import edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage;
import edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification;
import edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification;
import edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification;
import edu.ustb.sei.mde.testing.testdefinition.ValueSpecification;
import edu.ustb.sei.mde.testing.testdefinition.Variable;

public class TestmodelBuilder implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelBuilder {
	
	static private ResourceSet resourceSet = new ResourceSetImpl();
	static {
	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
		    Resource.Factory.Registry.DEFAULT_EXTENSION, new XMLTypeFactoryImpl());
	
	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("testmodel",
			new TestmodelResourceFactory());
	
	resourceSet.getPackageRegistry().put(TestdefinitionPackage.eINSTANCE.getNsURI(), TestdefinitionPackage.eINSTANCE);
	resourceSet.getPackageRegistry().put(TestcasePackage.eINSTANCE.getNsURI(), TestcasePackage.eINSTANCE);
	
	}
	
	public boolean isBuildingNeeded(org.eclipse.emf.common.util.URI uri) {
		// change this to return true to enable building of all resources
		return true;
	}
	
	public org.eclipse.core.runtime.IStatus build(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResource resource, org.eclipse.core.runtime.IProgressMonitor monitor) {
		// set option overrideBuilder to 'false' and then perform build here
		
		if(resource.getContents().size()==0 || resource.getContents().get(0).eClass()!=TestdefinitionPackage.eINSTANCE.getTestModel()) {
			TestCaseModel testCaseModel = (TestCaseModel)resource.getContents().get(0);
			if(new TestCaseBuilder(testCaseModel.getTestmodel(), testCaseModel).build())
				return org.eclipse.core.runtime.Status.OK_STATUS;
			else return org.eclipse.core.runtime.Status.CANCEL_STATUS;
		}
		
		URI uri = resource.getURI();
		URI targetURI = uri.trimFileExtension().appendFileExtension("testcase.testmodel");
		
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResource target = null;
		
		target = (TestmodelResource) resourceSet.getResource(targetURI, true);
		if(target==null) {
			target = (TestmodelResource) resourceSet.createResource(targetURI);
		}
		
		
		TestModel model = (TestModel) resource.getContents().get(0);
		
		convertModel(model,target);
		
		
		
		try {
			target.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		resourceSet.getResources().remove(target);
		
		return org.eclipse.core.runtime.Status.OK_STATUS;
	}
	
	private void convertModel(TestModel model, TestmodelResource target) {
		TestCaseModel testCaseModel = null;
		if(target.getContents().size()==0) {
			testCaseModel = TestcaseFactory.eINSTANCE.createTestCaseModel();
			target.getContents().add(testCaseModel);
		} else {
			testCaseModel = (TestCaseModel)target.getContents().get(0);
		}
		
		testCaseModel.setTestmodel(model);
		
		if(testCaseModel.getName()==null) 
			testCaseModel.setName(model.getName()+"TestCase");
		
		
		for(TestScenario ts : model.getScenarios()) {
			convertTestScenario(ts,testCaseModel);
		}
	}
	
	private void convertTestScenario(TestScenario ts, TestCaseModel target) {
		List<TestCase> testCases = this.generateTestCases(ts);
		//TODO remove duplicate
		
		target.getTestcases().clear();
		target.getTestcases().addAll(testCases);
		
	}
	
	
	
	
	final static private Variable[] emptyVarArray = new Variable[0];
	private List<TestCase> generateTestCases(TestScenario ts) {
		List<Variable[]> allCombination = null;
		for(Variable v : ts.getVariables()) {
			if(allCombination==null) {
				List<Variable> vars = collectLiterals(v,emptyVarArray);
				allCombination = new ArrayList<Variable[]>();
				for(Variable vv : vars) {
					allCombination.add(new Variable[]{vv});
				}
			} else {
				List<Variable[]> next = new ArrayList<Variable[]>();
				for(Variable[] cv : allCombination) {
					List<Variable> vars = collectLiterals(v, cv);
					for(Variable x : vars) {
						Variable[] nv = copyAndCombine(cv,x);
						next.add(nv);
					}
				}
				allCombination = next;
			}
		}
		
		if(allCombination==null) return null;
		
		List<TestCase> testCases = new ArrayList<TestCase>();
		int i = 1;
		for(Variable[] v : allCombination) {
			TestCase tc = TestcaseFactory.eINSTANCE.createTestCase();
			tc.setScenario(ts);
			String str = ts.getName()+(i++);
			tc.setName(str);
			tc.getVariables().addAll(Arrays.asList(v));
			
			if(ts.getDo()!=null) {
				Script doS = TestdefinitionFactory.eINSTANCE.createScript();
				doS.setLanguage(ts.getDo().getLanguage());
				doS.setScript(ts.getDo().getScript());
				tc.setDo(doS);
			}
			
			if(ts.getAssertion()!=null) {
				Oracle ora = TestdefinitionFactory.eINSTANCE.createOracle();
				ora.setLanguage(ts.getAssertion().getLanguage());
				ora.setScript(ts.getAssertion().getScript());
				tc.setAssertion(ora);
				testCases.add(tc);
			}
		}
		return testCases;
		
	}
	
	private Variable[] copyAndCombine(Variable[] cv, Variable x) {
		Variable[] newVars = new Variable[cv.length+1];
		
		for(int i = 0; i<cv.length ; i++) {
			newVars[i] = EcoreUtil.copy(cv[i]);
		}
		newVars[cv.length] = EcoreUtil.copy(x);
		return newVars;
	}

	private List<Variable> collectLiterals(Variable v, Variable[] existingVars) {
		EClassifier type = v.getType();
		ValueSpecification value = v.getValue();
		List<Variable> vars = new ArrayList<Variable>();
		
		if(value instanceof ValueRangeSpecification) {
			Variable va,vb,vc;
			
			va = createVariable(v);
			ValueLiteralSpecification literala = TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
			va.setValue(literala);
			
			vc = createVariable(v);
			ValueLiteralSpecification literalc = TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
			vc.setValue(literalc);
			
			if(type==EcorePackage.eINSTANCE.getEString()) {
				literala.setLiteral(((ValueRangeSpecification) value).getMinLiteral());
				literalc.setLiteral(((ValueRangeSpecification) value).getMaxLiteral());
				vars.add(va);
//				vars.add(vb);
				vars.add(vc);
			} else if(type==EcorePackage.eINSTANCE.getEBoolean()) {
				literala.setLiteral(((ValueRangeSpecification) value).getMinLiteral());
				literalc.setLiteral(((ValueRangeSpecification) value).getMaxLiteral());
				vars.add(va);
				vars.add(vc);
			} else if(type==EcorePackage.eINSTANCE.getEInt()) {
				literala.setLiteral(((ValueRangeSpecification) value).getMinLiteral());
				literalc.setLiteral(((ValueRangeSpecification) value).getMaxLiteral());
				
				vars.add(va);
				vars.add(vc);

				try {
					String mid = middleInt(((ValueRangeSpecification) value).getMinLiteral(), ((ValueRangeSpecification) value).getMaxLiteral());
					if(mid!=null) {
						vb = createVariable(v);
						ValueLiteralSpecification literalb = TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
						vb.setValue(literalb);
						literalb.setLiteral(mid);
						vars.add(vb);
					}
				} catch(Exception e) {
				}
				
			} else if(type==EcorePackage.eINSTANCE.getEDouble()) {
				literala.setLiteral(((ValueRangeSpecification) value).getMinLiteral());
				literalc.setLiteral(((ValueRangeSpecification) value).getMaxLiteral());
				
				vars.add(va);
				vars.add(vc);

				try {
					String mid = middleDouble(((ValueRangeSpecification) value).getMinLiteral(), ((ValueRangeSpecification) value).getMaxLiteral());
					if(mid!=null) {
						vb = createVariable(v);
						ValueLiteralSpecification literalb = TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
						vb.setValue(literalb);
						literalb.setLiteral(mid);
						vars.add(vb);
					}
				} catch(Exception e) {
				}
			} else if(type==EcorePackage.eINSTANCE.getEDate()) {
				literala.setLiteral(((ValueRangeSpecification) value).getMinLiteral());
				literalc.setLiteral(((ValueRangeSpecification) value).getMaxLiteral());
				
				vars.add(va);
				vars.add(vc);

				try {
					String mid = middleDate(((ValueRangeSpecification) value).getMinLiteral(), ((ValueRangeSpecification) value).getMaxLiteral());
					if(mid!=null) {
						vb = createVariable(v);
						ValueLiteralSpecification literalb = TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
						vb.setValue(literalb);
						literalb.setLiteral(mid);
						vars.add(vb);
					}
				} catch(Exception e) {
				}
			} else {
				literala.setLiteral(((ValueRangeSpecification) value).getMinLiteral());
				literalc.setLiteral(((ValueRangeSpecification) value).getMaxLiteral());
				
				vars.add(va);
				vars.add(vc);
			}
		} else if(value instanceof ValueSetSpecification) {
			for(String literal : ((ValueSetSpecification) value).getLiterals()) {
				Variable va = createVariable(v);
				ValueLiteralSpecification literala = TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
				va.setValue(literala);
				literala.setLiteral(literal);
				vars.add(va);
			}
		} else if(value instanceof ValueScriptSpecification){
			Variable va = createVariable(v);
			ValueScriptSpecification literala = TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
			va.setValue(literala);
			literala.setScript(((ValueScriptSpecification) value).getScript());
			vars.add(va);
		} else if(value instanceof ReturnValueSpecification){
			Variable va = createVariable(v);
			ReturnValueSpecification literala = TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
			va.setValue(literala);
			literala.setInterface(((ReturnValueSpecification) value).getInterface());
			literala.setOperation(((ReturnValueSpecification) value).getOperation());
			for(Variable a : ((ReturnValueSpecification) value).getActualParameters()) {
				for(Variable ea : existingVars) {
					if(a.getName().equals(ea.getName())) {
						literala.getActualParameters().add(ea);
						break;
					}
				}
			}
			vars.add(va);
		} else if(value instanceof ValueLiteralSpecification){
			Variable va = createVariable(v);
			ValueLiteralSpecification literala = TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
			va.setValue(literala);
			literala.setLiteral(((ValueLiteralSpecification) value).getLiteral());
			vars.add(va);
		}
		
		return vars;
	}
	
	private Variable createVariable(Variable v) {
		Variable va = TestdefinitionFactory.eINSTANCE.createVariable();
		va.setName(v.getName());
		va.setType(v.getType());
		return va;
	}

	private String middleDate(String minLiteral, String maxLiteral) {
		try {
			Date min = DateFormat.getInstance().parse(minLiteral);
			Date max = DateFormat.getInstance().parse(maxLiteral);
			long ml = min.getTime();
			long mb = max.getTime();
			long mm = (ml + mb)/2L;
			if(mm==ml || mm==mb) return null;
			Date mid = new Date(mm);
			return mid.toString();
		} catch(Exception e) {
		}
		return null;
	}

	private String middleDouble(String minLiteral, String maxLiteral) {
		Double min = Double.parseDouble(minLiteral);
		Double max = Double.parseDouble(maxLiteral);
		Double mid = ((min+max)/2.0);
		if(min==mid || mid==max) return null;
		else return Double.toString(mid);
	}

	private String middleInt(String minLiteral, String maxLiteral) {
		Integer min = Integer.parseInt(minLiteral);
		Integer max = Integer.parseInt(maxLiteral);
		Integer mid = ((min+max)/2);
		if(min==mid || mid==max) return null;
		else return Integer.toString(mid);
	}

	/**
	 * Handles the deletion of the given resource.
	 */
	public org.eclipse.core.runtime.IStatus handleDeletion(org.eclipse.emf.common.util.URI uri, org.eclipse.core.runtime.IProgressMonitor monitor) {
		// by default nothing is done when a resource is deleted
		return org.eclipse.core.runtime.Status.OK_STATUS;
	}
	
}
