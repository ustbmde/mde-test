/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EcorePackage;

import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.ResolverUtil;

public class ETypedElementETypeReferenceResolver implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier> {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier> delegate = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>();
	
	public void resolve(String identifier, org.eclipse.emf.ecore.ETypedElement container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolveResult<org.eclipse.emf.ecore.EClassifier> result) {
		if(container instanceof EAttribute) {
			EDataType t = ResolverUtil.getPrimitiveType(identifier);
			if(t!=null) {
				result.addMapping(identifier, t);
				return;
			}
		} else {
			EClassifier t = ResolverUtil.getPrimitiveType(identifier);
			if(t!=null) {
				result.addMapping(identifier, t);
				return;
			}
			TestModel model = ResolverUtil.getTestModel(container);
			t = ResolverUtil.getMessageType(identifier, model);
			if(t!=null) {
				result.addMapping(identifier, t);
				return;
			}
		}
		
		delegate.resolve(identifier, container, reference, position, resolveFuzzy, result);
	}
	
	public String deResolve(org.eclipse.emf.ecore.EClassifier element, org.eclipse.emf.ecore.ETypedElement container, org.eclipse.emf.ecore.EReference reference) {
		if(element instanceof EDataType) {
			return ResolverUtil.getPrimitiveTypeName((EDataType)element);
		} else 
			return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
