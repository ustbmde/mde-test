/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis;

import org.eclipse.emf.ecore.EClass;

import edu.ustb.sei.mde.testing.testcase.TestCaseModel;
import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.ResolverUtil;

public class ReturnValueSpecificationInterfaceReferenceResolver implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EClass> {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EClass> delegate = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultResolverDelegate<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EClass>();
	
	public void resolve(String identifier, edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container, org.eclipse.emf.ecore.EReference reference, int position, boolean resolveFuzzy, final edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolveResult<org.eclipse.emf.ecore.EClass> result) {
		EClass cls = null;
		
		cls = ResolverUtil.getInterfaceInTestModel(identifier, container);
		if(cls!=null) {
			result.addMapping(identifier, cls);
		} else {
			cls = ResolverUtil.getInterfaceInTestCaseModel(identifier, container);
			if(cls!=null)
				result.addMapping(identifier, cls);
		}
	}

	
	
	public String deResolve(org.eclipse.emf.ecore.EClass element, edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container, org.eclipse.emf.ecore.EReference reference) {
		return element.getName();
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		// save options in a field or leave method empty if this resolver does not depend
		// on any option
	}
	
}
