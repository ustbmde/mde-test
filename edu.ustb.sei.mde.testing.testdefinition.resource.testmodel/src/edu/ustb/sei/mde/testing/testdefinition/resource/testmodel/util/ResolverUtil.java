package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

import edu.ustb.sei.mde.testing.testcase.TestCase;
import edu.ustb.sei.mde.testing.testcase.TestCaseModel;
import edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification;
import edu.ustb.sei.mde.testing.testdefinition.TestModel;
import edu.ustb.sei.mde.testing.testdefinition.TestScenario;

public class ResolverUtil {
//	static private HashMap<String,DataType> dataTypes = new HashMap<String,DataType>();
	
	static public EDataType getPrimitiveType(String name) {
		switch(name) {
		case "String": return EcorePackage.eINSTANCE.getEString();
		case "boolean": return EcorePackage.eINSTANCE.getEBoolean();
		case "int": return EcorePackage.eINSTANCE.getEInt();
		case "double": return EcorePackage.eINSTANCE.getEDouble();
		case "Date": return EcorePackage.eINSTANCE.getEDate();
		case "Object": return EcorePackage.eINSTANCE.getEJavaObject();
		default:
		}
		return null;
	}
	
	static public String getPrimitiveTypeName(EDataType dt) {
		if(dt==EcorePackage.eINSTANCE.getEString()) return "String";
		if(dt==EcorePackage.eINSTANCE.getEBoolean()) return "boolean";
		if(dt==EcorePackage.eINSTANCE.getEInt()) return "int";
		if(dt==EcorePackage.eINSTANCE.getEDouble()) return "double";
		if(dt==EcorePackage.eINSTANCE.getEDate()) return "Date";
		if(dt==EcorePackage.eINSTANCE.getEJavaObject()) return "Object";
		return "UNKNOWN:"+dt.getInstanceClassName();
	}
	
	static public EClass getMessageType(String identifier, TestModel model) {
		if(model==null) return null;
		
		for(EClass cls : model.getClassifiers()) {
			if(cls.isInterface()==false && cls.getName().equals(identifier)) {
				return cls;
			}
		}
		return null;
	}
	
	static public EClass getInterface(String identifier, TestModel model) {
		if(model==null) return null;
		
		for(EClass cls : model.getClassifiers()) {
			if(cls.isInterface()==true && cls.getName().equals(identifier)) {
				return cls;
			}
		}
		return null;
	}
	
	static public TestScenario getTestScenario(EObject container) {
		while(container!=null && !(container instanceof TestScenario)) {
			container = container.eContainer();
		}
		return (TestScenario)container;
	}
	
	static public TestModel getTestModel(EObject container) {
		while(container!=null && !(container instanceof TestModel)) {
			container = container.eContainer();
		}
		return (TestModel)container;
	}

	public static TestCaseModel getTestCaseModel(EObject container) {
		while(container!=null && !(container instanceof TestCaseModel)) {
			container = container.eContainer();
		}
		return (TestCaseModel)container;
	}

	public static TestCase getTestCase(EObject container) {
		while(container!=null && !(container instanceof TestCase)) {
			container = container.eContainer();
		}
		return (TestCase)container;
	}

	public static EClass getInterface(String identifier, EPackage ePackage) {
		if(ePackage==null) return null;
		TreeIterator<EObject> it = ePackage.eAllContents();
		while(it.hasNext()) {
			EObject obj = it.next();
			if(obj instanceof EClass) {
				EClass cls = (EClass)obj;
				if(cls.isInterface()==true && cls.getName().equals(identifier)) {
					return cls;
				}
			}
		}
		return null;
	}
	
	public static EClass getInterfaceInTestCaseModel(String identifier,
			edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container) {
		TestCaseModel model = ResolverUtil.getTestCaseModel(container);
		EClass cls = ResolverUtil.getInterface(identifier, model.getTestmodel());
		if(cls!=null)
			return cls;
		else {
			cls = ResolverUtil.getInterface(identifier, model.getEPackage());
			return cls;
		}
	}

	public static EClass getInterfaceInTestModel(String identifier,
			edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification container) {
		TestModel model = ResolverUtil.getTestModel(container);
		EClass cls = ResolverUtil.getInterface(identifier, model);
		return cls;
	}

}
