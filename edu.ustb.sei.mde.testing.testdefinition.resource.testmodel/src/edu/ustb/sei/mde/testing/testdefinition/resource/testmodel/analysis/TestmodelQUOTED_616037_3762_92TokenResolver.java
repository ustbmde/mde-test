/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis;

public class TestmodelQUOTED_616037_3762_92TokenResolver implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultTokenResolver defaultTokenResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultTokenResolver(true);
	
	public String deResolve(Object value, org.eclipse.emf.ecore.EStructuralFeature feature, org.eclipse.emf.ecore.EObject container) {
		// By default token de-resolving is delegated to the DefaultTokenResolver.
		String result = defaultTokenResolver.deResolve(value, feature, container, "=<%", "%>", "\\");
		return result;
	}
	
	public void resolve(String lexem, org.eclipse.emf.ecore.EStructuralFeature feature, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result) {
		// By default token resolving is delegated to the DefaultTokenResolver.
		defaultTokenResolver.resolve(lexem, feature, result, "=<%", "%>", "\\");
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		defaultTokenResolver.setOptions(options);
	}
	
}
