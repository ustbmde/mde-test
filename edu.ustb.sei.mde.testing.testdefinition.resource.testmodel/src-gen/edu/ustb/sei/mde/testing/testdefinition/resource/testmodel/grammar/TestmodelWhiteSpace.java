/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

public class TestmodelWhiteSpace extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelFormattingElement {
	
	private final int amount;
	
	public TestmodelWhiteSpace(int amount, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
