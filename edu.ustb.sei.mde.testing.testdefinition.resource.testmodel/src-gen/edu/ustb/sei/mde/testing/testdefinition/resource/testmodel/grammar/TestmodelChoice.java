/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

public class TestmodelChoice extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement {
	
	public TestmodelChoice(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelStringUtil.explode(getChildren(), "|");
	}
	
}
