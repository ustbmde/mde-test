/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelPrinter implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextPrinter {
	
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolverFactory tokenResolverFactory = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenResolverFactory();
	
	protected java.io.OutputStream outputStream;
	
	/**
	 * Holds the resource that is associated with this printer. This may be null if
	 * the printer is used stand alone.
	 */
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource;
	
	private java.util.Map<?, ?> options;
	private String encoding = System.getProperty("file.encoding");
	
	public TestmodelPrinter(java.io.OutputStream outputStream, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
		super();
		this.outputStream = outputStream;
		this.resource = resource;
	}
	
	protected int matchCount(java.util.Map<String, Integer> featureCounter, java.util.Collection<String> needed) {
		int pos = 0;
		int neg = 0;
		
		for (String featureName : featureCounter.keySet()) {
			if (needed.contains(featureName)) {
				int value = featureCounter.get(featureName);
				if (value == 0) {
					neg += 1;
				} else {
					pos += 1;
				}
			}
		}
		return neg > 0 ? -neg : pos;
	}
	
	protected void doPrint(org.eclipse.emf.ecore.EObject element, java.io.PrintWriter out, String globaltab) {
		if (element == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write.");
		}
		if (out == null) {
			throw new java.lang.IllegalArgumentException("Nothing to write on.");
		}
		
		if (element instanceof org.eclipse.emf.ecore.EClass) {
			print_org_eclipse_emf_ecore_EClass((org.eclipse.emf.ecore.EClass) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.emf.ecore.EAttribute) {
			print_org_eclipse_emf_ecore_EAttribute((org.eclipse.emf.ecore.EAttribute) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.emf.ecore.EReference) {
			print_org_eclipse_emf_ecore_EReference((org.eclipse.emf.ecore.EReference) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.emf.ecore.EOperation) {
			print_org_eclipse_emf_ecore_EOperation((org.eclipse.emf.ecore.EOperation) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.emf.ecore.EParameter) {
			print_org_eclipse_emf_ecore_EParameter((org.eclipse.emf.ecore.EParameter) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.emf.ecore.EAnnotation) {
			print_org_eclipse_emf_ecore_EAnnotation((org.eclipse.emf.ecore.EAnnotation) element, globaltab, out);
			return;
		}
		if (element instanceof org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl) {
			print_java_util_Map_Entry((org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.TestModel) {
			print_edu_ustb_sei_mde_testing_testdefinition_TestModel((edu.ustb.sei.mde.testing.testdefinition.TestModel) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.TestScenario) {
			print_edu_ustb_sei_mde_testing_testdefinition_TestScenario((edu.ustb.sei.mde.testing.testdefinition.TestScenario) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.Variable) {
			print_edu_ustb_sei_mde_testing_testdefinition_Variable((edu.ustb.sei.mde.testing.testdefinition.Variable) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification) {
			print_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification((edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification) {
			print_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification((edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification) {
			print_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification((edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification) {
			print_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification((edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.Oracle) {
			print_edu_ustb_sei_mde_testing_testdefinition_Oracle((edu.ustb.sei.mde.testing.testdefinition.Oracle) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testcase.TestCaseModel) {
			print_edu_ustb_sei_mde_testing_testcase_TestCaseModel((edu.ustb.sei.mde.testing.testcase.TestCaseModel) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testcase.TestCase) {
			print_edu_ustb_sei_mde_testing_testcase_TestCase((edu.ustb.sei.mde.testing.testcase.TestCase) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification) {
			print_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification((edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification) element, globaltab, out);
			return;
		}
		if (element instanceof edu.ustb.sei.mde.testing.testdefinition.Script) {
			print_edu_ustb_sei_mde_testing_testdefinition_Script((edu.ustb.sei.mde.testing.testdefinition.Script) element, globaltab, out);
			return;
		}
		
		addWarningToResource("The printer can not handle " + element.eClass().getName() + " elements", element);
	}
	
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelReferenceResolverSwitch getReferenceResolverSwitch() {
		return (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelReferenceResolverSwitch) new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelMetaInformation().getReferenceResolverSwitch();
	}
	
	protected void addWarningToResource(final String errorMessage, org.eclipse.emf.ecore.EObject cause) {
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource = getResource();
		if (resource == null) {
			// the resource can be null if the printer is used stand alone
			return;
		}
		resource.addProblem(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelProblem(errorMessage, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType.PRINT_PROBLEM, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity.WARNING), cause);
	}
	
	public void setOptions(java.util.Map<?,?> options) {
		this.options = options;
	}
	
	public java.util.Map<?,?> getOptions() {
		return options;
	}
	
	public void setEncoding(String encoding) {
		if (encoding != null) {
			this.encoding = encoding;
		}
	}
	
	public String getEncoding() {
		return encoding;
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource getResource() {
		return resource;
	}
	
	/**
	 * Calls {@link #doPrint(EObject, PrintWriter, String)} and writes the result to
	 * the underlying output stream.
	 */
	public void print(org.eclipse.emf.ecore.EObject element) throws java.io.IOException {
		java.io.PrintWriter out = new java.io.PrintWriter(new java.io.OutputStreamWriter(new java.io.BufferedOutputStream(outputStream), encoding));
		doPrint(element, out, "");
		out.flush();
		out.close();
	}
	
	public void print_org_eclipse_emf_ecore_EClass(org.eclipse.emf.ecore.EClass element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(24);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INSTANCE_CLASS_NAME));
		printCountingMap.put("instanceClassName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INSTANCE_TYPE_NAME));
		printCountingMap.put("instanceTypeName", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__EPACKAGE));
		printCountingMap.put("ePackage", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ETYPE_PARAMETERS));
		printCountingMap.put("eTypeParameters", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ABSTRACT));
		printCountingMap.put("abstract", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INTERFACE));
		printCountingMap.put("interface", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES));
		printCountingMap.put("eSuperTypes", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__EOPERATIONS));
		printCountingMap.put("eOperations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESTRUCTURAL_FEATURES));
		printCountingMap.put("eStructuralFeatures", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__EGENERIC_SUPER_TYPES));
		printCountingMap.put("eGenericSuperTypes", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eAnnotations");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__EANNOTATIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eAnnotations", 0);
		}
		// DEFINITION PART BEGINS (BooleanTerminal)
		count = printCountingMap.get("interface");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INTERFACE));
			if (o != null) {
			}
			printCountingMap.put("interface", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_emf_ecore_EClass_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eStructuralFeatures");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESTRUCTURAL_FEATURES));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eStructuralFeatures", 0);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eOperations");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__EOPERATIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eOperations", 0);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_org_eclipse_emf_ecore_EClass_0(org.eclipse.emf.ecore.EClass element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("extends");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("eSuperTypes");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEClassESuperTypesReferenceResolver().deResolve((org.eclipse.emf.ecore.EClass) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES)), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), element));
				out.print(" ");
			}
			printCountingMap.put("eSuperTypes", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_emf_ecore_EClass_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_org_eclipse_emf_ecore_EClass_0_0(org.eclipse.emf.ecore.EClass element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("eSuperTypes");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEClassESuperTypesReferenceResolver().deResolve((org.eclipse.emf.ecore.EClass) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES)), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), element));
				out.print(" ");
			}
			printCountingMap.put("eSuperTypes", count - 1);
		}
	}
	
	
	public void print_org_eclipse_emf_ecore_EAttribute(org.eclipse.emf.ecore.EAttribute element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(20);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ORDERED));
		printCountingMap.put("ordered", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UNIQUE));
		printCountingMap.put("unique", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__LOWER_BOUND));
		printCountingMap.put("lowerBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UPPER_BOUND));
		printCountingMap.put("upperBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE));
		printCountingMap.put("eType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__EGENERIC_TYPE));
		printCountingMap.put("eGenericType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__CHANGEABLE));
		printCountingMap.put("changeable", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__VOLATILE));
		printCountingMap.put("volatile", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__TRANSIENT));
		printCountingMap.put("transient", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__DEFAULT_VALUE_LITERAL));
		printCountingMap.put("defaultValueLiteral", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UNSETTABLE));
		printCountingMap.put("unsettable", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__DERIVED));
		printCountingMap.put("derived", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ECONTAINING_CLASS));
		printCountingMap.put("eContainingClass", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ID));
		printCountingMap.put("iD", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eAnnotations");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__EANNOTATIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eAnnotations", 0);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("eType");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver().deResolve((org.eclipse.emf.ecore.EClassifier) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE)), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE), element));
				out.print(" ");
			}
			printCountingMap.put("eType", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_emf_ecore_EAttribute_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
	}
	
	public void print_org_eclipse_emf_ecore_EAttribute_0(org.eclipse.emf.ecore.EAttribute element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("lowerBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__LOWER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__LOWER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("lowerBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("..");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("upperBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UPPER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UPPER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("upperBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	
	public void print_org_eclipse_emf_ecore_EReference(org.eclipse.emf.ecore.EReference element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(24);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ORDERED));
		printCountingMap.put("ordered", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UNIQUE));
		printCountingMap.put("unique", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__LOWER_BOUND));
		printCountingMap.put("lowerBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UPPER_BOUND));
		printCountingMap.put("upperBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE));
		printCountingMap.put("eType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__EGENERIC_TYPE));
		printCountingMap.put("eGenericType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__CHANGEABLE));
		printCountingMap.put("changeable", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__VOLATILE));
		printCountingMap.put("volatile", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__TRANSIENT));
		printCountingMap.put("transient", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__DEFAULT_VALUE_LITERAL));
		printCountingMap.put("defaultValueLiteral", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UNSETTABLE));
		printCountingMap.put("unsettable", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__DERIVED));
		printCountingMap.put("derived", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ECONTAINING_CLASS));
		printCountingMap.put("eContainingClass", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__CONTAINMENT));
		printCountingMap.put("containment", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__RESOLVE_PROXIES));
		printCountingMap.put("resolveProxies", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__EOPPOSITE));
		printCountingMap.put("eOpposite", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__EKEYS));
		printCountingMap.put("eKeys", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eAnnotations");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__EANNOTATIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eAnnotations", 0);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("eType");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver().deResolve((org.eclipse.emf.ecore.EClassifier) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE)), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE), element));
				out.print(" ");
			}
			printCountingMap.put("eType", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("*");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_emf_ecore_EReference_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
	}
	
	public void print_org_eclipse_emf_ecore_EReference_0(org.eclipse.emf.ecore.EReference element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("lowerBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__LOWER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__LOWER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("lowerBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("..");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("upperBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UPPER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UPPER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("upperBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	
	public void print_org_eclipse_emf_ecore_EOperation(org.eclipse.emf.ecore.EOperation element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(15);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ORDERED));
		printCountingMap.put("ordered", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UNIQUE));
		printCountingMap.put("unique", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__LOWER_BOUND));
		printCountingMap.put("lowerBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UPPER_BOUND));
		printCountingMap.put("upperBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE));
		printCountingMap.put("eType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EGENERIC_TYPE));
		printCountingMap.put("eGenericType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ECONTAINING_CLASS));
		printCountingMap.put("eContainingClass", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE_PARAMETERS));
		printCountingMap.put("eTypeParameters", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EPARAMETERS));
		printCountingMap.put("eParameters", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EEXCEPTIONS));
		printCountingMap.put("eExceptions", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EGENERIC_EXCEPTIONS));
		printCountingMap.put("eGenericExceptions", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eAnnotations");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EANNOTATIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eAnnotations", 0);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("eType");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver().deResolve((org.eclipse.emf.ecore.EClassifier) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE)), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE), element));
				out.print(" ");
			}
			printCountingMap.put("eType", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_emf_ecore_EOperation_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_emf_ecore_EOperation_1(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
	}
	
	public void print_org_eclipse_emf_ecore_EOperation_0(org.eclipse.emf.ecore.EOperation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("lowerBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__LOWER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__LOWER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("lowerBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("..");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("upperBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UPPER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UPPER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("upperBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	public void print_org_eclipse_emf_ecore_EOperation_1(org.eclipse.emf.ecore.EOperation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eParameters");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EPARAMETERS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eParameters", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_emf_ecore_EOperation_1_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_org_eclipse_emf_ecore_EOperation_1_0(org.eclipse.emf.ecore.EOperation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eParameters");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__EPARAMETERS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eParameters", count - 1);
		}
	}
	
	
	public void print_org_eclipse_emf_ecore_EParameter(org.eclipse.emf.ecore.EParameter element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(11);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ORDERED));
		printCountingMap.put("ordered", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UNIQUE));
		printCountingMap.put("unique", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__LOWER_BOUND));
		printCountingMap.put("lowerBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UPPER_BOUND));
		printCountingMap.put("upperBound", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE));
		printCountingMap.put("eType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__EGENERIC_TYPE));
		printCountingMap.put("eGenericType", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__EOPERATION));
		printCountingMap.put("eOperation", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("eAnnotations");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__EANNOTATIONS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("eAnnotations", 0);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("eType");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver().deResolve((org.eclipse.emf.ecore.EClassifier) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE)), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE), element));
				out.print(" ");
			}
			printCountingMap.put("eType", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_emf_ecore_EParameter_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
	}
	
	public void print_org_eclipse_emf_ecore_EParameter_0(org.eclipse.emf.ecore.EParameter element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("lowerBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__LOWER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__LOWER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("lowerBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("..");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("upperBound");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UPPER_BOUND));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("NUMBER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UPPER_BOUND), element));
				out.print(" ");
			}
			printCountingMap.put("upperBound", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	
	public void print_org_eclipse_emf_ecore_EAnnotation(org.eclipse.emf.ecore.EAnnotation element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(6);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__EANNOTATIONS));
		printCountingMap.put("eAnnotations", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__SOURCE));
		printCountingMap.put("source", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__DETAILS));
		printCountingMap.put("details", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__EMODEL_ELEMENT));
		printCountingMap.put("eModelElement", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__CONTENTS));
		printCountingMap.put("contents", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__REFERENCES));
		printCountingMap.put("references", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("source");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__SOURCE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("ANNOTATION");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__SOURCE), element));
				out.print(" ");
			}
			printCountingMap.put("source", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_org_eclipse_emf_ecore_EAnnotation_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
	}
	
	public void print_org_eclipse_emf_ecore_EAnnotation_0(org.eclipse.emf.ecore.EAnnotation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("details");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__DETAILS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("details", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_org_eclipse_emf_ecore_EAnnotation_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
	}
	
	public void print_org_eclipse_emf_ecore_EAnnotation_0_0(org.eclipse.emf.ecore.EAnnotation element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("details");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__DETAILS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("details", count - 1);
		}
	}
	
	
	public void print_java_util_Map_Entry(org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.getKey();
		printCountingMap.put("key", temp == null ? 0 : 1);
		temp = element.getValue();
		printCountingMap.put("value", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("key");
		if (count > 0) {
			Object o = element.getKey();
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__KEY), element));
				out.print(" ");
			}
			printCountingMap.put("key", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("value");
		if (count > 0) {
			Object o = element.getValue();
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__VALUE), element));
				out.print(" ");
			}
			printCountingMap.put("value", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_TestModel(edu.ustb.sei.mde.testing.testdefinition.TestModel element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__CLASSIFIERS));
		printCountingMap.put("classifiers", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__SCENARIOS));
		printCountingMap.put("scenarios", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("model");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("classifiers");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__CLASSIFIERS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("classifiers", 0);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("scenarios");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__SCENARIOS));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("scenarios", 0);
		}
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_TestScenario(edu.ustb.sei.mde.testing.testdefinition.TestScenario element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__VARIABLES));
		printCountingMap.put("variables", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__ASSERTION));
		printCountingMap.put("assertion", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__DO));
		printCountingMap.put("do", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("test");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_mde_testing_testdefinition_TestScenario_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testdefinition_TestScenario_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("assertion");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__ASSERTION));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("assertion", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_TestScenario_0(edu.ustb.sei.mde.testing.testdefinition.TestScenario element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("variables");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__VARIABLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("variables", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_TestScenario_1(edu.ustb.sei.mde.testing.testdefinition.TestScenario element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("do");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("do");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__DO));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("do", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_Script(edu.ustb.sei.mde.testing.testdefinition.Script element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__SCRIPT));
		printCountingMap.put("script", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__LANGUAGE));
		printCountingMap.put("language", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testdefinition_Script_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("script");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__SCRIPT));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__SCRIPT), element));
				out.print(" ");
			}
			printCountingMap.put("script", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_Script_0(edu.ustb.sei.mde.testing.testdefinition.Script element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("language");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__LANGUAGE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__LANGUAGE), element));
				out.print(" ");
			}
			printCountingMap.put("language", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_Variable(edu.ustb.sei.mde.testing.testdefinition.Variable element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__VALUE));
		printCountingMap.put("value", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE));
		printCountingMap.put("type", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("var");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("type");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableTypeReferenceResolver().deResolve((org.eclipse.emf.ecore.EClassifier) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE), element));
				out.print(" ");
			}
			printCountingMap.put("type", count - 1);
		}
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("value");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__VALUE));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("value", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification(edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL));
		printCountingMap.put("minLiteral", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL));
		printCountingMap.put("maxLiteral", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("in");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("[");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("minLiteral");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL), element));
				out.print(" ");
			}
			printCountingMap.put("minLiteral", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("..");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("maxLiteral");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL), element));
				out.print(" ");
			}
			printCountingMap.put("maxLiteral", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("]");
		out.print(" ");
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification(edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS));
		printCountingMap.put("literals", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("in");
		out.print(" ");
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("literals");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS), element));
				out.print(" ");
			}
			printCountingMap.put("literals", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification_0(edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("literals");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS), element));
				out.print(" ");
			}
			printCountingMap.put("literals", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification(edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__SCRIPT));
		printCountingMap.put("script", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__LANGUAGE));
		printCountingMap.put("language", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("script");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__SCRIPT));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__SCRIPT), element));
				out.print(" ");
			}
			printCountingMap.put("script", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_0(edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("language");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__LANGUAGE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__LANGUAGE), element));
				out.print(" ");
			}
			printCountingMap.put("language", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification(edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(3);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE));
		printCountingMap.put("interface", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION));
		printCountingMap.put("operation", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS));
		printCountingMap.put("actualParameters", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("interface");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationInterfaceReferenceResolver().deResolve((org.eclipse.emf.ecore.EClass) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE), element));
				out.print(" ");
			}
			printCountingMap.put("interface", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(".");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("operation");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationOperationReferenceResolver().deResolve((org.eclipse.emf.ecore.EOperation) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION), element));
				out.print(" ");
			}
			printCountingMap.put("operation", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("(");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(")");
		out.print(" ");
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_0(edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("actualParameters");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationActualParametersReferenceResolver().deResolve((edu.ustb.sei.mde.testing.testdefinition.Variable) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), element));
				out.print(" ");
			}
			printCountingMap.put("actualParameters", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_0_0(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_0_0(edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print(",");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("actualParameters");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationActualParametersReferenceResolver().deResolve((edu.ustb.sei.mde.testing.testdefinition.Variable) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), element));
				out.print(" ");
			}
			printCountingMap.put("actualParameters", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_Oracle(edu.ustb.sei.mde.testing.testdefinition.Oracle element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(2);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__SCRIPT));
		printCountingMap.put("script", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__LANGUAGE));
		printCountingMap.put("language", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("assert");
		out.print(" ");
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testdefinition_Oracle_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("script");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__SCRIPT));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__SCRIPT), element));
				out.print(" ");
			}
			printCountingMap.put("script", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_mde_testing_testdefinition_Oracle_0(edu.ustb.sei.mde.testing.testdefinition.Oracle element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("language");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__LANGUAGE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__LANGUAGE), element));
				out.print(" ");
			}
			printCountingMap.put("language", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(":");
		out.print(" ");
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testcase_TestCaseModel(edu.ustb.sei.mde.testing.testcase.TestCaseModel element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(4);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTCASES));
		printCountingMap.put("testcases", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL));
		printCountingMap.put("testmodel", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE));
		printCountingMap.put("ePackage", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("module");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testcase_TestCaseModel_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testcase_TestCaseModel_1(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("testcases");
		if (count > 0) {
			java.util.List<?> list = (java.util.List<?>)element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTCASES));
			int index  = list.size() - count;
			if (index < 0) {
				index = 0;
			}
			java.util.ListIterator<?> it  = list.listIterator(index);
			while (it.hasNext()) {
				Object o = it.next();
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("testcases", 0);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_edu_ustb_sei_mde_testing_testcase_TestCaseModel_0(edu.ustb.sei.mde.testing.testcase.TestCaseModel element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("based on");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("testmodel");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseModelTestmodelReferenceResolver().deResolve((edu.ustb.sei.mde.testing.testdefinition.TestModel) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL), element));
				out.print(" ");
			}
			printCountingMap.put("testmodel", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_mde_testing_testcase_TestCaseModel_1(edu.ustb.sei.mde.testing.testcase.TestCaseModel element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("import");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("ePackage");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseModelEPackageReferenceResolver().deResolve((org.eclipse.emf.ecore.EPackage) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE), element));
				out.print(" ");
			}
			printCountingMap.put("ePackage", count - 1);
		}
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testcase_TestCase(edu.ustb.sei.mde.testing.testcase.TestCase element, String outertab, java.io.PrintWriter out) {
		String localtab = outertab;
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(5);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__NAME));
		printCountingMap.put("name", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__VARIABLES));
		printCountingMap.put("variables", temp == null ? 0 : ((java.util.Collection<?>) temp).size());
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__ASSERTION));
		printCountingMap.put("assertion", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__DO));
		printCountingMap.put("do", temp == null ? 0 : 1);
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO));
		printCountingMap.put("scenario", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		boolean iterate = true;
		java.io.StringWriter sWriter = null;
		java.io.PrintWriter out1 = null;
		java.util.Map<String, Integer> printCountingMap1 = null;
		// DEFINITION PART BEGINS (CsString)
		out.print("test");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("name");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__NAME));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__NAME), element));
				out.print(" ");
			}
			printCountingMap.put("name", count - 1);
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testcase_TestCase_0(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print("{");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CompoundDefinition)
		iterate = true;
		while (iterate) {
			sWriter = new java.io.StringWriter();
			out1 = new java.io.PrintWriter(sWriter);
			printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
			print_edu_ustb_sei_mde_testing_testcase_TestCase_1(element, localtab, out1, printCountingMap1);
			if (printCountingMap.equals(printCountingMap1)) {
				iterate = false;
				out1.close();
			} else {
				out1.flush();
				out1.close();
				out.print(sWriter.toString());
				printCountingMap.putAll(printCountingMap1);
			}
		}
		// DEFINITION PART BEGINS (CompoundDefinition)
		sWriter = new java.io.StringWriter();
		out1 = new java.io.PrintWriter(sWriter);
		printCountingMap1 = new java.util.LinkedHashMap<String, Integer>(printCountingMap);
		print_edu_ustb_sei_mde_testing_testcase_TestCase_2(element, localtab, out1, printCountingMap1);
		if (printCountingMap.equals(printCountingMap1)) {
			out1.close();
		} else {
			out1.flush();
			out1.close();
			out.print(sWriter.toString());
			printCountingMap.putAll(printCountingMap1);
		}
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("assertion");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__ASSERTION));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("assertion", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
		// DEFINITION PART BEGINS (CsString)
		out.print("}");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_edu_ustb_sei_mde_testing_testcase_TestCase_0(edu.ustb.sei.mde.testing.testcase.TestCase element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("based on");
		// DEFINITION PART BEGINS (WhiteSpaces)
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderUsingSpecifiedToken)
		count = printCountingMap.get("scenario");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseScenarioReferenceResolver().deResolve((edu.ustb.sei.mde.testing.testdefinition.TestScenario) o, element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO)), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO), element));
				out.print(" ");
			}
			printCountingMap.put("scenario", count - 1);
		}
	}
	
	public void print_edu_ustb_sei_mde_testing_testcase_TestCase_1(edu.ustb.sei.mde.testing.testcase.TestCase element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("variables");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__VARIABLES));
			java.util.List<?> list = (java.util.List<?>) o;
			int index = list.size() - count;
			if (index >= 0) {
				o = list.get(index);
			} else {
				o = null;
			}
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("variables", count - 1);
		}
		// DEFINITION PART BEGINS (CsString)
		out.print(";");
		out.print(" ");
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	public void print_edu_ustb_sei_mde_testing_testcase_TestCase_2(edu.ustb.sei.mde.testing.testcase.TestCase element, String outertab, java.io.PrintWriter out, java.util.Map<String, Integer> printCountingMap) {
		String localtab = outertab;
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("do");
		out.print(" ");
		// DEFINITION PART BEGINS (Containment)
		count = printCountingMap.get("do");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__DO));
			if (o != null) {
				doPrint((org.eclipse.emf.ecore.EObject) o, out, localtab);
			}
			printCountingMap.put("do", count - 1);
		}
		// DEFINITION PART BEGINS (LineBreak)
		out.println();
		out.print(localtab);
	}
	
	
	public void print_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification(edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification element, String outertab, java.io.PrintWriter out) {
		// The printCountingMap contains a mapping from feature names to the number of
		// remaining elements that still need to be printed. The map is initialized with
		// the number of elements stored in each structural feature. For lists this is the
		// list size. For non-multiple features it is either 1 (if the feature is set) or
		// 0 (if the feature is null).
		java.util.Map<String, Integer> printCountingMap = new java.util.LinkedHashMap<String, Integer>(1);
		Object temp;
		temp = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.VALUE_LITERAL_SPECIFICATION__LITERAL));
		printCountingMap.put("literal", temp == null ? 0 : 1);
		// print collected hidden tokens
		int count;
		// DEFINITION PART BEGINS (CsString)
		out.print("=");
		out.print(" ");
		// DEFINITION PART BEGINS (PlaceholderInQuotes)
		count = printCountingMap.get("literal");
		if (count > 0) {
			Object o = element.eGet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.VALUE_LITERAL_SPECIFICATION__LITERAL));
			if (o != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				resolver.setOptions(getOptions());
				out.print(resolver.deResolve((Object) o, element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.VALUE_LITERAL_SPECIFICATION__LITERAL), element));
				out.print(" ");
			}
			printCountingMap.put("literal", count - 1);
		}
	}
	
	
}
