/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel;

/**
 * This interface is extended by some other generated classes. It provides access
 * to the plug-in meta information.
 */
public interface ITestmodelTextResourcePluginPart {
	
	/**
	 * Returns a meta information object for the language plug-in that contains this
	 * part.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelMetaInformation getMetaInformation();
	
}
