grammar Testmodel;

options {
	superClass = TestmodelANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;
}

@lexer::members {
	public java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions  = new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>();
	public java.util.List<Integer> lexerExceptionsPosition = new java.util.ArrayList<Integer>();
	
	public void reportError(org.antlr.runtime3_4_0.RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionsPosition.add(((org.antlr.runtime3_4_0.ANTLRStringStream) input).index());
	}
}
@header{
	package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;
}

@members{
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolverFactory tokenResolverFactory = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> expectedElements = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelProblem() {
					public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity getSeverity() {
						return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity.ERROR;
					}
					public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType getType() {
						return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement terminal = edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelFollowSetProvider.TERMINALS[terminalID];
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[] containmentFeatures = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelFollowSetProvider.LINKS[ids[i]];
		}
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace containmentTrace = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace(eClass, containmentFeatures);
		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal expectedElement = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
	}
	
	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelLocationMap locationMap = resource.getLocationMap();
				if (locationMap == null) {
					// the locationMap can be null if the parser is used for code completion
					return true;
				}
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new TestmodelParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TestmodelLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
			} else {
				return new TestmodelParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TestmodelLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (java.io.IOException e) {
			new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public TestmodelParser() {
		super(null);
	}
	
	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((TestmodelLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((TestmodelLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
			if (type.getInstanceClass() == org.eclipse.emf.ecore.EClass.class) {
				return parse_org_eclipse_emf_ecore_EClass();
			}
			if (type.getInstanceClass() == org.eclipse.emf.ecore.EAttribute.class) {
				return parse_org_eclipse_emf_ecore_EAttribute();
			}
			if (type.getInstanceClass() == org.eclipse.emf.ecore.EReference.class) {
				return parse_org_eclipse_emf_ecore_EReference();
			}
			if (type.getInstanceClass() == org.eclipse.emf.ecore.EOperation.class) {
				return parse_org_eclipse_emf_ecore_EOperation();
			}
			if (type.getInstanceClass() == org.eclipse.emf.ecore.EParameter.class) {
				return parse_org_eclipse_emf_ecore_EParameter();
			}
			if (type.getInstanceClass() == org.eclipse.emf.ecore.EAnnotation.class) {
				return parse_org_eclipse_emf_ecore_EAnnotation();
			}
			if (type.getInstanceClass() == java.util.Map.Entry.class) {
				return parse_java_util_Map_Entry();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.TestModel.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_TestModel();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.TestScenario.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.Script.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_Script();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.Variable.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_Variable();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.Oracle.class) {
				return parse_edu_ustb_sei_mde_testing_testdefinition_Oracle();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testcase.TestCaseModel.class) {
				return parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testcase.TestCase.class) {
				return parse_edu_ustb_sei_mde_testing_testcase_TestCase();
			}
			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification.class) {
				return parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification();
			}
		}
		throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		java.util.Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelParseResult parse() {
		terminateParsing = false;
		postParseCommands = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>>();
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelParseResult parseResult = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelParseResult();
		try {
			org.eclipse.emf.ecore.EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
			}
		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
			reportError(re);
		} catch (java.lang.IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
			reportLexicalError(re);
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelParseResult result = parse();
		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			org.eclipse.emf.ecore.EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		java.util.Set<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal>();
		java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> newFollowSet = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 154;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]> newFollowerPair : newFollowers) {
							edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement newFollower = newFollowerPair.getLeft();
							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
							edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace containmentTrace = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace(null, newFollowerPair.getRight());
							edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal newFollowTerminal = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	/**
	 * Translates errors thrown by the parser into human readable messages.
	 */
	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = e.getMessage();
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			String expectedTokenName = formatTokenName(mte.expecting);
			String actualTokenName = formatTokenName(e.token.getType());
			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
			String expectedTokenName = formatTokenName(mtne.expecting);
			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
		}
		// the resource may be null if the parser is used for code completion
		final String finalMessage = message;
		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
		} else {
			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
		}
	}
	
	/**
	 * Translates errors thrown by the lexer into human readable messages.
	 */
	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
		String message = "";
		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
		}
		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ org.eclipse.emf.ecore.EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[0]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[1]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_edu_ustb_sei_mde_testing_testdefinition_TestModel{ element = c0; }
		|  		c1 = parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel{ element = c1; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_org_eclipse_emf_ecore_EClass returns [org.eclipse.emf.ecore.EClass element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_emf_ecore_EAnnotation			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__EANNOTATIONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[2]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[3]);
	}
	
	(
		(
			a1 = 'interface' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_1, true, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
				// set value of boolean attribute
				Object value = true;
				element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INTERFACE), value);
				completedElement(value, false);
			}
			|			a2 = 'type' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_1, false, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
				// set value of boolean attribute
				Object value = false;
				element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INTERFACE), value);
				completedElement(value, false);
			}
		)
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[4]);
	}
	
	(
		a4 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[5]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[6]);
	}
	
	(
		(
			a5 = 'extends' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[7]);
			}
			
			(
				a6 = IDENTIFIER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
						startIncompleteElement(element);
					}
					if (a6 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEClassESuperTypesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_1, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[8]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[9]);
			}
			
			(
				(
					a7 = ',' {
						if (element == null) {
							element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_2_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[10]);
					}
					
					(
						a8 = IDENTIFIER						
						{
							if (terminateParsing) {
								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
							}
							if (element == null) {
								element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
								startIncompleteElement(element);
							}
							if (a8 != null) {
								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
								tokenResolver.setOptions(getOptions());
								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
								tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), result);
								Object resolvedObject = result.getResolvedToken();
								if (resolvedObject == null) {
									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
								}
								String resolved = (String) resolvedObject;
								org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
								collectHiddenTokens(element);
								registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEClassESuperTypesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), resolved, proxy);
								if (proxy != null) {
									Object value = proxy;
									addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES, value);
									completedElement(value, false);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_2_0_0_1, proxy, true);
								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, proxy);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[11]);
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[12]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[13]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[14]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[15]);
	}
	
	a9 = '{' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[16]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[17]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[18]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[19]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[20]);
	}
	
	(
		(
			a10_0 = parse_org_eclipse_emf_ecore_EStructuralFeature			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
					startIncompleteElement(element);
				}
				if (a10_0 != null) {
					if (a10_0 != null) {
						Object value = a10_0;
						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__ESTRUCTURAL_FEATURES, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_5, a10_0, true);
					copyLocalizationInfos(a10_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[21]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[22]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[23]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[24]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[25]);
	}
	
	(
		(
			a11_0 = parse_org_eclipse_emf_ecore_EOperation			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
					startIncompleteElement(element);
				}
				if (a11_0 != null) {
					if (a11_0 != null) {
						Object value = a11_0;
						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__EOPERATIONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_6, a11_0, true);
					copyLocalizationInfos(a11_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[26]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[27]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[28]);
	}
	
	a12 = '}' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[29]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[30]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[31]);
	}
	
;

parse_org_eclipse_emf_ecore_EAttribute returns [org.eclipse.emf.ecore.EAttribute element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_emf_ecore_EAnnotation			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__EANNOTATIONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAttribute(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[32]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[33]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[34]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[35]);
	}
	
	(
		(
			a2 = '[' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[36]);
			}
			
			(
				a3 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__LOWER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__LOWER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[37]);
			}
			
			a4 = '..' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[38]);
			}
			
			(
				a5 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
						startIncompleteElement(element);
					}
					if (a5 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UPPER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UPPER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_3, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[39]);
			}
			
			a6 = ']' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_4, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[40]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[41]);
	}
	
	(
		a7 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
				startIncompleteElement(element);
			}
			if (a7 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_3, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[42]);
	}
	
	a8 = ';' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[43]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[44]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[45]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[46]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[47]);
	}
	
;

parse_org_eclipse_emf_ecore_EReference returns [org.eclipse.emf.ecore.EReference element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_emf_ecore_EAnnotation			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EREFERENCE__EANNOTATIONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEReference(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[48]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[49]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[50]);
	}
	
	a2 = '*' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[51]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[52]);
	}
	
	(
		(
			a3 = '[' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[53]);
			}
			
			(
				a4 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__LOWER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__LOWER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[54]);
			}
			
			a5 = '..' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[55]);
			}
			
			(
				a6 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
						startIncompleteElement(element);
					}
					if (a6 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UPPER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UPPER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_3, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[56]);
			}
			
			a7 = ']' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_4, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[57]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[58]);
	}
	
	(
		a8 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
				startIncompleteElement(element);
			}
			if (a8 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[59]);
	}
	
	a9 = ';' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[60]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[61]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[62]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[63]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[64]);
	}
	
;

parse_org_eclipse_emf_ecore_EOperation returns [org.eclipse.emf.ecore.EOperation element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_emf_ecore_EAnnotation			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EOPERATION__EANNOTATIONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[65]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[66]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[67]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[68]);
	}
	
	(
		(
			a2 = '[' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[69]);
			}
			
			(
				a3 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__LOWER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__LOWER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[70]);
			}
			
			a4 = '..' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[71]);
			}
			
			(
				a5 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
						startIncompleteElement(element);
					}
					if (a5 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UPPER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UPPER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_3, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[72]);
			}
			
			a6 = ']' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_4, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[73]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[74]);
	}
	
	(
		a7 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
				startIncompleteElement(element);
			}
			if (a7 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_3, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[75]);
	}
	
	a8 = '(' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[76]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[77]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[78]);
	}
	
	(
		(
			(
				a9_0 = parse_org_eclipse_emf_ecore_EParameter				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
						startIncompleteElement(element);
					}
					if (a9_0 != null) {
						if (a9_0 != null) {
							Object value = a9_0;
							addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EOPERATION__EPARAMETERS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_5_0_0_0, a9_0, true);
						copyLocalizationInfos(a9_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[79]);
				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[80]);
				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[81]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[82]);
			}
			
			(
				(
					a10 = ',' {
						if (element == null) {
							element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_5_0_0_1_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[83]);
						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[84]);
					}
					
					(
						a11_0 = parse_org_eclipse_emf_ecore_EParameter						{
							if (terminateParsing) {
								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
							}
							if (element == null) {
								element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
								startIncompleteElement(element);
							}
							if (a11_0 != null) {
								if (a11_0 != null) {
									Object value = a11_0;
									addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EOPERATION__EPARAMETERS, value);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_5_0_0_1_0_0_1, a11_0, true);
								copyLocalizationInfos(a11_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[85]);
						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[86]);
						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[87]);
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[88]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[89]);
				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[90]);
				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[91]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[92]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[93]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[94]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[95]);
	}
	
	a12 = ')' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[96]);
	}
	
	a13 = ';' {
		if (element == null) {
			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_7, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[97]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[98]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[99]);
	}
	
;

parse_org_eclipse_emf_ecore_EParameter returns [org.eclipse.emf.ecore.EParameter element = null]
@init{
}
:
	(
		(
			a0_0 = parse_org_eclipse_emf_ecore_EAnnotation			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
					startIncompleteElement(element);
				}
				if (a0_0 != null) {
					if (a0_0 != null) {
						Object value = a0_0;
						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EPARAMETER__EANNOTATIONS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_0, a0_0, true);
					copyLocalizationInfos(a0_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEParameter(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[100]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[101]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[102]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[103]);
	}
	
	(
		(
			a2 = '[' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[104]);
			}
			
			(
				a3 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__LOWER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__LOWER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[105]);
			}
			
			a4 = '..' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_2, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[106]);
			}
			
			(
				a5 = NUMBER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
						startIncompleteElement(element);
					}
					if (a5 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UPPER_BOUND), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
						}
						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UPPER_BOUND), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_3, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[107]);
			}
			
			a6 = ']' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_4, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[108]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[109]);
	}
	
	(
		a7 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
				startIncompleteElement(element);
			}
			if (a7 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_3, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[110]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[111]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[112]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[113]);
	}
	
;

parse_org_eclipse_emf_ecore_EAnnotation returns [org.eclipse.emf.ecore.EAnnotation element = null]
@init{
}
:
	(
		a0 = ANNOTATION		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("ANNOTATION");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__SOURCE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__SOURCE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[114]);
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[115]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[116]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[117]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[118]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[119]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[120]);
	}
	
	(
		(
			a1 = '(' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAnnotation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[121]);
			}
			
			(
				a2_0 = parse_java_util_Map_Entry				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
						startIncompleteElement(element);
					}
					if (a2_0 != null) {
						if (a2_0 != null) {
							Object value = a2_0;
							addMapEntry(element, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__DETAILS), a2_0);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_1, a2_0, true);
						copyLocalizationInfos(a2_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[122]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[123]);
			}
			
			(
				(
					a3 = ',' {
						if (element == null) {
							element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_2_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAnnotation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[124]);
					}
					
					(
						a4_0 = parse_java_util_Map_Entry						{
							if (terminateParsing) {
								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
							}
							if (element == null) {
								element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
								startIncompleteElement(element);
							}
							if (a4_0 != null) {
								if (a4_0 != null) {
									Object value = a4_0;
									addMapEntry(element, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__DETAILS), a4_0);
									completedElement(value, true);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_2_0_0_1, a4_0, true);
								copyLocalizationInfos(a4_0, element);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[125]);
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[126]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[127]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[128]);
			}
			
			a5 = ')' {
				if (element == null) {
					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_3, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[129]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[130]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[131]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[132]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[133]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[134]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[135]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[136]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[137]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[138]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[139]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[140]);
	}
	
;

parse_java_util_Map_Entry returns [edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject element = null]
@init{
}
:
	(
		a0 = QUOTED_34_34_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject(org.eclipse.emf.ecore.impl.EcorePackageImpl.eINSTANCE.getEStringToStringMapEntry(),"EStringToStringMapEntry");
				startIncompleteElement(element);
			}
			if (a0 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__KEY), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__KEY), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_6_0_0_0, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[141]);
	}
	
	a1 = '=' {
		if (element == null) {
			element = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject(org.eclipse.emf.ecore.impl.EcorePackageImpl.eINSTANCE.getEStringToStringMapEntry(),"EStringToStringMapEntry");
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_6_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[142]);
	}
	
	(
		a2 = QUOTED_34_34_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject(org.eclipse.emf.ecore.impl.EcorePackageImpl.eINSTANCE.getEStringToStringMapEntry(),"EStringToStringMapEntry");
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__VALUE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__VALUE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_6_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[143]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[144]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_TestModel returns [edu.ustb.sei.mde.testing.testdefinition.TestModel element = null]
@init{
}
:
	a0 = 'model' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[145]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[146]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[147]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[148]);
	}
	
	(
		(
			a2_0 = parse_org_eclipse_emf_ecore_EClass			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
					startIncompleteElement(element);
				}
				if (a2_0 != null) {
					if (a2_0 != null) {
						Object value = a2_0;
						addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__CLASSIFIERS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_2, a2_0, true);
					copyLocalizationInfos(a2_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[149]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[150]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[151]);
	}
	
	(
		(
			a3_0 = parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
					startIncompleteElement(element);
				}
				if (a3_0 != null) {
					if (a3_0 != null) {
						Object value = a3_0;
						addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__SCENARIOS, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_3, a3_0, true);
					copyLocalizationInfos(a3_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[152]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario returns [edu.ustb.sei.mde.testing.testdefinition.TestScenario element = null]
@init{
}
:
	a0 = 'test' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[153]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[154]);
	}
	
	a2 = '{' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[155]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[156]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[157]);
	}
	
	(
		(
			(
				a3_0 = parse_edu_ustb_sei_mde_testing_testdefinition_Variable				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__VARIABLES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_4_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[158]);
			}
			
			a4 = ';' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_4_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[159]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[160]);
				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[161]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[162]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[163]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[164]);
	}
	
	(
		(
			a5 = 'do' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_5_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[165]);
				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[166]);
				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[167]);
				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[168]);
			}
			
			(
				a6_0 = parse_edu_ustb_sei_mde_testing_testdefinition_Script				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
						startIncompleteElement(element);
					}
					if (a6_0 != null) {
						if (a6_0 != null) {
							Object value = a6_0;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__DO), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_5_0_0_1, a6_0, true);
						copyLocalizationInfos(a6_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[169]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[170]);
	}
	
	(
		a7_0 = parse_edu_ustb_sei_mde_testing_testdefinition_Oracle		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
				startIncompleteElement(element);
			}
			if (a7_0 != null) {
				if (a7_0 != null) {
					Object value = a7_0;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__ASSERTION), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_6, a7_0, true);
				copyLocalizationInfos(a7_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[171]);
	}
	
	a8 = '}' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_8, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[172]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_Script returns [edu.ustb.sei.mde.testing.testdefinition.Script element = null]
@init{
}
:
	(
		(
			(
				a0 = IDENTIFIER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createScript();
						startIncompleteElement(element);
					}
					if (a0 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__LANGUAGE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__LANGUAGE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_9_0_0_0_0_0_0, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[173]);
			}
			
			a1 = ':' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createScript();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_9_0_0_0_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[174]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[175]);
	}
	
	(
		a2 = QUOTED_6037_3762_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createScript();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__SCRIPT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__SCRIPT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_9_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[176]);
	}
	
	|//derived choice rules for sub-classes: 
	
	c0 = parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_edu_ustb_sei_mde_testing_testdefinition_Oracle{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_Variable returns [edu.ustb.sei.mde.testing.testdefinition.Variable element = null]
@init{
}
:
	a0 = 'var' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[177]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.Variable, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[178]);
	}
	
	(
		a2 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[179]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[180]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[181]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[182]);
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[183]);
	}
	
	(
		a3_0 = parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
				startIncompleteElement(element);
			}
			if (a3_0 != null) {
				if (a3_0 != null) {
					Object value = a3_0;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__VALUE), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_3, a3_0, true);
				copyLocalizationInfos(a3_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[184]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[185]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification element = null]
@init{
}
:
	a0 = 'in' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[186]);
	}
	
	a1 = '[' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[187]);
	}
	
	(
		a2 = QUOTED_34_34_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[188]);
	}
	
	a3 = '..' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_3, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[189]);
	}
	
	(
		a4 = QUOTED_34_34_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
				startIncompleteElement(element);
			}
			if (a4 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_4, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[190]);
	}
	
	a5 = ']' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_5, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[191]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[192]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification element = null]
@init{
}
:
	a0 = 'in' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[193]);
	}
	
	a1 = '{' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_1, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[194]);
	}
	
	(
		a2 = QUOTED_34_34_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
				startIncompleteElement(element);
			}
			if (a2 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS, value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[195]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[196]);
	}
	
	(
		(
			a3 = ',' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[197]);
			}
			
			(
				a4 = QUOTED_34_34_92				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
						startIncompleteElement(element);
					}
					if (a4 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_3_0_0_1, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[198]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[199]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[200]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[201]);
	}
	
	a5 = '}' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[202]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[203]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification element = null]
@init{
}
:
	a0 = '=' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[204]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[205]);
	}
	
	(
		(
			(
				a1 = IDENTIFIER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
						startIncompleteElement(element);
					}
					if (a1 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__LANGUAGE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__LANGUAGE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_1_0_0_0, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[206]);
			}
			
			a2 = ':' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_1_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[207]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[208]);
	}
	
	(
		a3 = QUOTED_6037_3762_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__SCRIPT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__SCRIPT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[209]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[210]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification element = null]
@init{
}
:
	a0 = '=' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[211]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationInterfaceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_1, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[212]);
	}
	
	a2 = '.' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_2, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[213]);
	}
	
	(
		a3 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				String resolved = (String) resolvedObject;
				org.eclipse.emf.ecore.EOperation proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
				collectHiddenTokens(element);
				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EOperation>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationOperationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION), resolved, proxy);
				if (proxy != null) {
					Object value = proxy;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_3, proxy, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[214]);
	}
	
	a4 = '(' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[215]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[216]);
	}
	
	(
		(
			(
				a5 = IDENTIFIER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
						startIncompleteElement(element);
					}
					if (a5 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.mde.testing.testdefinition.Variable proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationActualParametersReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS, value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_5_0_0_0, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[217]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[218]);
			}
			
			(
				(
					a6 = ',' {
						if (element == null) {
							element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
							startIncompleteElement(element);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_5_0_0_1_0_0_0, null, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
					}
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[219]);
					}
					
					(
						a7 = IDENTIFIER						
						{
							if (terminateParsing) {
								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
							}
							if (element == null) {
								element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
								startIncompleteElement(element);
							}
							if (a7 != null) {
								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
								tokenResolver.setOptions(getOptions());
								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
								tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), result);
								Object resolvedObject = result.getResolvedToken();
								if (resolvedObject == null) {
									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
								}
								String resolved = (String) resolvedObject;
								edu.ustb.sei.mde.testing.testdefinition.Variable proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
								collectHiddenTokens(element);
								registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationActualParametersReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), resolved, proxy);
								if (proxy != null) {
									Object value = proxy;
									addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS, value);
									completedElement(value, false);
								}
								collectHiddenTokens(element);
								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_5_0_0_1_0_0_1, proxy, true);
								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
							}
						}
					)
					{
						// expected elements (follow set)
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[220]);
						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[221]);
					}
					
				)
				
			)*			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[222]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[223]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[224]);
	}
	
	a8 = ')' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_6, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[225]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[226]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_Oracle returns [edu.ustb.sei.mde.testing.testdefinition.Oracle element = null]
@init{
}
:
	a0 = 'assert' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[227]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[228]);
	}
	
	(
		(
			(
				a1 = IDENTIFIER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
						startIncompleteElement(element);
					}
					if (a1 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__LANGUAGE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
						}
						java.lang.String resolved = (java.lang.String) resolvedObject;
						if (resolved != null) {
							Object value = resolved;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__LANGUAGE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_1_0_0_0, resolved, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[229]);
			}
			
			a2 = ':' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_1_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[230]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[231]);
	}
	
	(
		a3 = QUOTED_6037_3762_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
				startIncompleteElement(element);
			}
			if (a3 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__SCRIPT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__SCRIPT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[232]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel returns [edu.ustb.sei.mde.testing.testcase.TestCaseModel element = null]
@init{
}
:
	a0 = 'module' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[233]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[234]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[235]);
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[236]);
	}
	
	(
		(
			a2 = 'based on' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_2_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[237]);
			}
			
			(
				a3 = QUOTED_34_34_92				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.mde.testing.testdefinition.TestModel proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testcase.TestCaseModel, edu.ustb.sei.mde.testing.testdefinition.TestModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseModelTestmodelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_2_0_0_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[238]);
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[239]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[240]);
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[241]);
	}
	
	(
		(
			a4 = 'import' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[242]);
			}
			
			(
				a5 = QUOTED_34_34_92				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
						startIncompleteElement(element);
					}
					if (a5 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						org.eclipse.emf.ecore.EPackage proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEPackage();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testcase.TestCaseModel, org.eclipse.emf.ecore.EPackage>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseModelEPackageReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_3_0_0_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[243]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[244]);
	}
	
	(
		(
			a6_0 = parse_edu_ustb_sei_mde_testing_testcase_TestCase			{
				if (terminateParsing) {
					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
				}
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
					startIncompleteElement(element);
				}
				if (a6_0 != null) {
					if (a6_0 != null) {
						Object value = a6_0;
						addObjectToList(element, edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTCASES, value);
						completedElement(value, true);
					}
					collectHiddenTokens(element);
					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_5, a6_0, true);
					copyLocalizationInfos(a6_0, element);
				}
			}
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[245]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testcase_TestCase returns [edu.ustb.sei.mde.testing.testcase.TestCase element = null]
@init{
}
:
	a0 = 'test' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[246]);
	}
	
	(
		a1 = IDENTIFIER		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__NAME), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__NAME), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_2, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[247]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[248]);
	}
	
	(
		(
			a2 = 'based on' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_3_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[249]);
			}
			
			(
				a3 = IDENTIFIER				
				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
						startIncompleteElement(element);
					}
					if (a3 != null) {
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
						tokenResolver.setOptions(getOptions());
						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO), result);
						Object resolvedObject = result.getResolvedToken();
						if (resolvedObject == null) {
							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
						}
						String resolved = (String) resolvedObject;
						edu.ustb.sei.mde.testing.testdefinition.TestScenario proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
						collectHiddenTokens(element);
						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testcase.TestCase, edu.ustb.sei.mde.testing.testdefinition.TestScenario>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseScenarioReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO), resolved, proxy);
						if (proxy != null) {
							Object value = proxy;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO), value);
							completedElement(value, false);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_3_0_0_2, proxy, true);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[250]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[251]);
	}
	
	a4 = '{' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_4, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[252]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[253]);
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[254]);
	}
	
	(
		(
			(
				a5_0 = parse_edu_ustb_sei_mde_testing_testdefinition_Variable				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
						startIncompleteElement(element);
					}
					if (a5_0 != null) {
						if (a5_0 != null) {
							Object value = a5_0;
							addObjectToList(element, edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__VARIABLES, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_6_0_0_0, a5_0, true);
						copyLocalizationInfos(a5_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[255]);
			}
			
			a6 = ';' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_6_0_0_1, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[256]);
				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[257]);
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[258]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[259]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[260]);
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[261]);
	}
	
	(
		(
			a7 = 'do' {
				if (element == null) {
					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
					startIncompleteElement(element);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_7_0_0_0, null, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
			}
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[262]);
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[263]);
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[264]);
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[265]);
			}
			
			(
				a8_0 = parse_edu_ustb_sei_mde_testing_testdefinition_Script				{
					if (terminateParsing) {
						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
					}
					if (element == null) {
						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
						startIncompleteElement(element);
					}
					if (a8_0 != null) {
						if (a8_0 != null) {
							Object value = a8_0;
							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__DO), value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_7_0_0_1, a8_0, true);
						copyLocalizationInfos(a8_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[266]);
			}
			
		)
		
	)?	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[267]);
	}
	
	(
		a9_0 = parse_edu_ustb_sei_mde_testing_testdefinition_Oracle		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
				startIncompleteElement(element);
			}
			if (a9_0 != null) {
				if (a9_0 != null) {
					Object value = a9_0;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__ASSERTION), value);
					completedElement(value, true);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_8, a9_0, true);
				copyLocalizationInfos(a9_0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[268]);
	}
	
	a10 = '}' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_10, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[269]);
	}
	
;

parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification returns [edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification element = null]
@init{
}
:
	a0 = '=' {
		if (element == null) {
			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_18_0_0_0, null, true);
		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[270]);
	}
	
	(
		a1 = QUOTED_34_34_92		
		{
			if (terminateParsing) {
				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
			}
			if (element == null) {
				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
				tokenResolver.setOptions(getOptions());
				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.VALUE_LITERAL_SPECIFICATION__LITERAL), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.VALUE_LITERAL_SPECIFICATION__LITERAL), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_18_0_0_1, resolved, true);
				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[271]);
		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[272]);
	}
	
;

parse_org_eclipse_emf_ecore_EStructuralFeature returns [org.eclipse.emf.ecore.EStructuralFeature element = null]
:
	c0 = parse_org_eclipse_emf_ecore_EAttribute{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_org_eclipse_emf_ecore_EReference{ element = c1; /* this is a subclass or primitive expression choice */ }
	
;

parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueSpecification element = null]
:
	c0 = parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification{ element = c3; /* this is a subclass or primitive expression choice */ }
	|	c4 = parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification{ element = c4; /* this is a subclass or primitive expression choice */ }
	
;

IDENTIFIER:
	(('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'0'..'9'|'_')*)
;
NUMBER:
	('-'?('0'..'9')+)
;
SL_COMMENT:
	( '//'(~('\n'|'\r'|'\uffff'))* )
	{ _channel = 99; }
;
ML_COMMENT:
	( '/*'.*'*/')
	{ _channel = 99; }
;
ANNOTATION:
	('@'('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'0'..'9'|'_')*)
;
TEXT:
	(('A'..'Z' | 'a'..'z' | '0'..'9' | '_' | '-' )+)
	{ _channel = 99; }
;
WHITESPACE:
	((' ' | '\t' | '\f'))
	{ _channel = 99; }
;
LINEBREAK:
	(('\r\n' | '\r' | '\n'))
	{ _channel = 99; }
;
QUOTED_34_34_92:
	(('"')(('\\''"')|('\\''\\')|(~('"'|'\\')))*('"'))
;
QUOTED_6037_3762_92:
	(('<%')(('\\''%>')|('\\''\\')|(~('%'|'\\')|'%'~('>')))*('%>'))
;

