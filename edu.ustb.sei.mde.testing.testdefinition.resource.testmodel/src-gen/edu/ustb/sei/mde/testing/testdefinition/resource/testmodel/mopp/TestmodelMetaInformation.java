/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelMetaInformation implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelMetaInformation {
	
	public String getSyntaxName() {
		return "testmodel";
	}
	
	public String getURI() {
		return "http://www.ustb.edu.cn/sei/mde/testing/testdefinition";
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextScanner createLexer() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelAntlrScanner(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelLexer());
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextParser createParser(java.io.InputStream inputStream, String encoding) {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelParser().createInstance(inputStream, encoding);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextPrinter createPrinter(java.io.OutputStream outputStream, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelPrinter2(outputStream, resource);
	}
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolverSwitch getReferenceResolverSwitch() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelReferenceResolverSwitch();
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolverFactory getTokenResolverFactory() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "edu.ustb.sei.mde.testing/model/testmodel.cs";
	}
	
	public String[] getTokenNames() {
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelParser.tokenNames;
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenStyle getDefaultTokenStyle(String tokenName) {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelBracketPair> getBracketPairs() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelBracketInformationProvider().getBracketPairs();
	}
	
	public org.eclipse.emf.ecore.EClass[] getFoldableClasses() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelFoldingInformationProvider().getFoldableClasses();
	}
	
	public org.eclipse.emf.ecore.resource.Resource.Factory createResourceFactory() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResourceFactory();
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelNewFileContentProvider getNewFileContentProvider() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResourceFactory());
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ui.launchConfigurationType";
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelNameProvider createNameProvider() {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelAntlrTokenHelper tokenHelper = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelAntlrTokenHelper();
		java.util.List<String> highlightableTokens = new java.util.ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
