/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

/**
 * A basic implementation of the
 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelURIMapping
 * interface that can map identifiers to URIs.
 * 
 * @param <ReferenceType> unused type parameter which is needed to implement
 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelURIMapping.
 */
public class TestmodelURIMapping<ReferenceType> implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelURIMapping<ReferenceType> {
	
	private org.eclipse.emf.common.util.URI uri;
	private String identifier;
	private String warning;
	
	public TestmodelURIMapping(String identifier, org.eclipse.emf.common.util.URI newIdentifier, String warning) {
		super();
		this.uri = newIdentifier;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public org.eclipse.emf.common.util.URI getTargetIdentifier() {
		return uri;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
