/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

/**
 * A basic implementation of the
 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelElementMapp
 * ing interface.
 * 
 * @param <ReferenceType> the type of the reference that can be mapped to
 */
public class TestmodelElementMapping<ReferenceType> implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelElementMapping<ReferenceType> {
	
	private final ReferenceType target;
	private String identifier;
	private String warning;
	
	public TestmodelElementMapping(String identifier, ReferenceType target, String warning) {
		super();
		this.target = target;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public ReferenceType getTargetElement() {
		return target;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
