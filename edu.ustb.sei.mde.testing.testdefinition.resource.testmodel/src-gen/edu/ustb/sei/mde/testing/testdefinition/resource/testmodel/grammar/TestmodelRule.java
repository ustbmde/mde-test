/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

/**
 * A class to represent a rules in the grammar.
 */
public class TestmodelRule extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement {
	
	private final org.eclipse.emf.ecore.EClass metaclass;
	
	public TestmodelRule(org.eclipse.emf.ecore.EClass metaclass, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelChoice choice, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality) {
		super(cardinality, new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return metaclass;
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelChoice getDefinition() {
		return (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelChoice) getChildren()[0];
	}
	
}

