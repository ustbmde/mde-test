/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

public class TestmodelLineBreak extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelFormattingElement {
	
	private final int tabs;
	
	public TestmodelLineBreak(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
