/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelSyntaxCoverageInformationProvider {
	
	public org.eclipse.emf.ecore.EClass[] getClassesWithSyntax() {
		return new org.eclipse.emf.ecore.EClass[] {
			org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(),
			org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAttribute(),
			org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEReference(),
			org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(),
			org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEParameter(),
			org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAnnotation(),
			org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEStringToStringMapEntry(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getScript(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getValueRangeSpecification(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getValueSetSpecification(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getValueScriptSpecification(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification(),
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getOracle(),
			edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(),
			edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(),
			edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getValueLiteralSpecification(),
		};
	}
	
	public org.eclipse.emf.ecore.EClass[] getStartSymbols() {
		return new org.eclipse.emf.ecore.EClass[] {
			edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(),
			edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(),
		};
	}
	
}
