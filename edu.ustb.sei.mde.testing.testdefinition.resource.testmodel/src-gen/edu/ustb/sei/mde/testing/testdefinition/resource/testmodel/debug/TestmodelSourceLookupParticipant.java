/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug;

public class TestmodelSourceLookupParticipant extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant {
	
	public String getSourceName(Object object) throws org.eclipse.core.runtime.CoreException {
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.TestmodelStackFrame) {
			edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.TestmodelStackFrame frame = (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.TestmodelStackFrame) object;
			return frame.getResourceURI();
		}
		return null;
	}
	
}
