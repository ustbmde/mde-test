/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

public abstract class TestmodelFormattingElement extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement {
	
	public TestmodelFormattingElement(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality) {
		super(cardinality, null);
	}
	
}
