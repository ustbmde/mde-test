/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

public class TestmodelContainment extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelTerminal {
	
	private final org.eclipse.emf.ecore.EClass[] allowedTypes;
	
	public TestmodelContainment(org.eclipse.emf.ecore.EStructuralFeature feature, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality, org.eclipse.emf.ecore.EClass[] allowedTypes, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.allowedTypes = allowedTypes;
	}
	
	public org.eclipse.emf.ecore.EClass[] getAllowedTypes() {
		return allowedTypes;
	}
	
	public String toString() {
		String typeRestrictions = null;
		if (allowedTypes != null && allowedTypes.length > 0) {
			typeRestrictions = edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelStringUtil.explode(allowedTypes, ", ", new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelFunction1<String, org.eclipse.emf.ecore.EClass>() {
				public String execute(org.eclipse.emf.ecore.EClass eClass) {
					return eClass.getName();
				}
			});
		}
		return getFeature().getName() + (typeRestrictions == null ? "" : "[" + typeRestrictions + "]");
	}
	
}
