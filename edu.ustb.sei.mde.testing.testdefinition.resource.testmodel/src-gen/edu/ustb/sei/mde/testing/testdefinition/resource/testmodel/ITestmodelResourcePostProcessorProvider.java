/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel;

/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface ITestmodelResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelResourcePostProcessor getResourcePostProcessor();
	
}
