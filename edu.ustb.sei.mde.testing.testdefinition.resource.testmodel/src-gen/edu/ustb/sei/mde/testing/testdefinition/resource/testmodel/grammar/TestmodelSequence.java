/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

public class TestmodelSequence extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement {
	
	public TestmodelSequence(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelStringUtil.explode(getChildren(), " ");
	}
	
}
