/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelAntlrScanner implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextScanner {
	
	private org.antlr.runtime3_4_0.Lexer antlrLexer;
	
	public TestmodelAntlrScanner(org.antlr.runtime3_4_0.Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final org.antlr.runtime3_4_0.Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextToken result = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new org.antlr.runtime3_4_0.ANTLRStringStream(text));
	}
	
}
