/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel;

/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface ITestmodelCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
