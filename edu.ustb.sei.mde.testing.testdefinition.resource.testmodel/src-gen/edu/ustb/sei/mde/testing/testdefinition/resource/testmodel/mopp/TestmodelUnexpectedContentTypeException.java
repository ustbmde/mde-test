/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

/**
 * An Excpetion to represent invalid content types for parser instances.
 * 
 * @see
 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelOptions.RES
 * OURCE_CONTENT_TYPE
 */
public class TestmodelUnexpectedContentTypeException extends org.antlr.runtime3_4_0.RecognitionException {
	
	private static final long serialVersionUID = 4791359811519433999L;
	
	private Object contentType = null;
	
	public  TestmodelUnexpectedContentTypeException(Object contentType) {
		this.contentType = contentType;
	}
	
	public Object getContentType() {
		return contentType;
	}
	
}
