/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class TestmodelSyntaxElement {
	
	private TestmodelSyntaxElement[] children;
	private TestmodelSyntaxElement parent;
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality;
	
	public TestmodelSyntaxElement(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality, TestmodelSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (TestmodelSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(TestmodelSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public TestmodelSyntaxElement getParent() {
		return parent;
	}
	
	public TestmodelSyntaxElement[] getChildren() {
		if (children == null) {
			return new TestmodelSyntaxElement[0];
		}
		return children;
	}
	
	public org.eclipse.emf.ecore.EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality getCardinality() {
		return cardinality;
	}
	
}
