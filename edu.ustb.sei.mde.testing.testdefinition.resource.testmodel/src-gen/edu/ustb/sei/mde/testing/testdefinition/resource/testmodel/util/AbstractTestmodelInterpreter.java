/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractTestmodelInterpreter<ResultType, ContextType> {
	
	private java.util.Stack<org.eclipse.emf.ecore.EObject> interpretationStack = new java.util.Stack<org.eclipse.emf.ecore.EObject>();
	private java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelInterpreterListener> listeners = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelInterpreterListener>();
	private org.eclipse.emf.ecore.EObject nextObjectToInterprete;
	private Object currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		org.eclipse.emf.ecore.EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (java.util.EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(org.eclipse.emf.ecore.EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.TestScenario) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_TestScenario((edu.ustb.sei.mde.testing.testdefinition.TestScenario) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.Variable) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_Variable((edu.ustb.sei.mde.testing.testdefinition.Variable) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.Oracle) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_Oracle((edu.ustb.sei.mde.testing.testdefinition.Oracle) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification((edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification((edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification((edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification((edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.ValueSpecification) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification((edu.ustb.sei.mde.testing.testdefinition.ValueSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.TestModel) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_TestModel((edu.ustb.sei.mde.testing.testdefinition.TestModel) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.Script) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_Script((edu.ustb.sei.mde.testing.testdefinition.Script) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition) {
			result = interprete_edu_ustb_sei_mde_testing_testdefinition_AbstractTestDefinition((edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EAttribute) {
			result = interprete_org_eclipse_emf_ecore_EAttribute((org.eclipse.emf.ecore.EAttribute) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EAnnotation) {
			result = interprete_org_eclipse_emf_ecore_EAnnotation((org.eclipse.emf.ecore.EAnnotation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EClass) {
			result = interprete_org_eclipse_emf_ecore_EClass((org.eclipse.emf.ecore.EClass) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EEnum) {
			result = interprete_org_eclipse_emf_ecore_EEnum((org.eclipse.emf.ecore.EEnum) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EDataType) {
			result = interprete_org_eclipse_emf_ecore_EDataType((org.eclipse.emf.ecore.EDataType) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EClassifier) {
			result = interprete_org_eclipse_emf_ecore_EClassifier((org.eclipse.emf.ecore.EClassifier) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EEnumLiteral) {
			result = interprete_org_eclipse_emf_ecore_EEnumLiteral((org.eclipse.emf.ecore.EEnumLiteral) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EFactory) {
			result = interprete_org_eclipse_emf_ecore_EFactory((org.eclipse.emf.ecore.EFactory) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EOperation) {
			result = interprete_org_eclipse_emf_ecore_EOperation((org.eclipse.emf.ecore.EOperation) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EPackage) {
			result = interprete_org_eclipse_emf_ecore_EPackage((org.eclipse.emf.ecore.EPackage) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EParameter) {
			result = interprete_org_eclipse_emf_ecore_EParameter((org.eclipse.emf.ecore.EParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EReference) {
			result = interprete_org_eclipse_emf_ecore_EReference((org.eclipse.emf.ecore.EReference) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EStructuralFeature) {
			result = interprete_org_eclipse_emf_ecore_EStructuralFeature((org.eclipse.emf.ecore.EStructuralFeature) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.ETypedElement) {
			result = interprete_org_eclipse_emf_ecore_ETypedElement((org.eclipse.emf.ecore.ETypedElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.ENamedElement) {
			result = interprete_org_eclipse_emf_ecore_ENamedElement((org.eclipse.emf.ecore.ENamedElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EModelElement) {
			result = interprete_org_eclipse_emf_ecore_EModelElement((org.eclipse.emf.ecore.EModelElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EObject) {
			result = interprete_org_eclipse_emf_ecore_EObject((org.eclipse.emf.ecore.EObject) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof java.util.Map.Entry<?,?>) {
			result = interprete_java_util_Map_Entry((java.util.Map.Entry<?,?>) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.EGenericType) {
			result = interprete_org_eclipse_emf_ecore_EGenericType((org.eclipse.emf.ecore.EGenericType) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof org.eclipse.emf.ecore.ETypeParameter) {
			result = interprete_org_eclipse_emf_ecore_ETypeParameter((org.eclipse.emf.ecore.ETypeParameter) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testcase.TestCase) {
			result = interprete_edu_ustb_sei_mde_testing_testcase_TestCase((edu.ustb.sei.mde.testing.testcase.TestCase) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testcase.TestCaseModel) {
			result = interprete_edu_ustb_sei_mde_testing_testcase_TestCaseModel((edu.ustb.sei.mde.testing.testcase.TestCaseModel) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.NamedElement) {
			result = interprete_edu_ustb_sei_mde_testing_NamedElement((edu.ustb.sei.mde.testing.NamedElement) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification) {
			result = interprete_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification((edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_TestScenario(edu.ustb.sei.mde.testing.testdefinition.TestScenario testScenario, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_Variable(edu.ustb.sei.mde.testing.testdefinition.Variable variable, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_Oracle(edu.ustb.sei.mde.testing.testdefinition.Oracle oracle, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification(edu.ustb.sei.mde.testing.testdefinition.ValueSpecification valueSpecification, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification(edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification valueRangeSpecification, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification(edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification valueSetSpecification, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification(edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification valueScriptSpecification, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification(edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification returnValueSpecification, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_TestModel(edu.ustb.sei.mde.testing.testdefinition.TestModel testModel, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_Script(edu.ustb.sei.mde.testing.testdefinition.Script script, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testdefinition_AbstractTestDefinition(edu.ustb.sei.mde.testing.testdefinition.AbstractTestDefinition abstractTestDefinition, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EAttribute(org.eclipse.emf.ecore.EAttribute eAttribute, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EAnnotation(org.eclipse.emf.ecore.EAnnotation eAnnotation, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EClass(org.eclipse.emf.ecore.EClass eClass, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EClassifier(org.eclipse.emf.ecore.EClassifier eClassifier, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EDataType(org.eclipse.emf.ecore.EDataType eDataType, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EEnum(org.eclipse.emf.ecore.EEnum eEnum, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EEnumLiteral(org.eclipse.emf.ecore.EEnumLiteral eEnumLiteral, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EFactory(org.eclipse.emf.ecore.EFactory eFactory, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EModelElement(org.eclipse.emf.ecore.EModelElement eModelElement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_ENamedElement(org.eclipse.emf.ecore.ENamedElement eNamedElement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EObject(org.eclipse.emf.ecore.EObject eObject, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EOperation(org.eclipse.emf.ecore.EOperation eOperation, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EPackage(org.eclipse.emf.ecore.EPackage ePackage, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EParameter(org.eclipse.emf.ecore.EParameter eParameter, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EReference(org.eclipse.emf.ecore.EReference eReference, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EStructuralFeature(org.eclipse.emf.ecore.EStructuralFeature eStructuralFeature, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_ETypedElement(org.eclipse.emf.ecore.ETypedElement eTypedElement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_java_util_Map_Entry(java.util.Map.Entry<?,?> eStringToStringMapEntry, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_EGenericType(org.eclipse.emf.ecore.EGenericType eGenericType, ContextType context) {
		return null;
	}
	
	public ResultType interprete_org_eclipse_emf_ecore_ETypeParameter(org.eclipse.emf.ecore.ETypeParameter eTypeParameter, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_NamedElement(edu.ustb.sei.mde.testing.NamedElement namedElement, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testcase_TestCase(edu.ustb.sei.mde.testing.testcase.TestCase testCase, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testcase_TestCaseModel(edu.ustb.sei.mde.testing.testcase.TestCaseModel testCaseModel, ContextType context) {
		return null;
	}
	
	public ResultType interprete_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification(edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification valueLiteralSpecification, ContextType context) {
		return null;
	}
	
	private void notifyListeners(org.eclipse.emf.ecore.EObject element) {
		for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(org.eclipse.emf.ecore.EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		for (org.eclipse.emf.ecore.EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(java.util.Collection<? extends org.eclipse.emf.ecore.EObject> objects) {
		java.util.List<org.eclipse.emf.ecore.EObject> reverse = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>(objects.size());
		reverse.addAll(objects);
		java.util.Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(org.eclipse.emf.ecore.EObject root) {
		java.util.List<org.eclipse.emf.ecore.EObject> objects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
		objects.add(root);
		java.util.Iterator<org.eclipse.emf.ecore.EObject> it = root.eAllContents();
		while (it.hasNext()) {
			org.eclipse.emf.ecore.EObject eObject = (org.eclipse.emf.ecore.EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public org.eclipse.emf.ecore.EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public java.util.Stack<org.eclipse.emf.ecore.EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public Object getCurrentContext() {
		return currentContext;
	}
	
}
