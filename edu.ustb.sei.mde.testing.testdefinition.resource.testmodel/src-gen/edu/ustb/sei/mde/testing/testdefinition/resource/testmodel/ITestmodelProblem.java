/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel;

public interface ITestmodelProblem {
	public String getMessage();
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity getSeverity();
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType getType();
	public java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix> getQuickFixes();
}
