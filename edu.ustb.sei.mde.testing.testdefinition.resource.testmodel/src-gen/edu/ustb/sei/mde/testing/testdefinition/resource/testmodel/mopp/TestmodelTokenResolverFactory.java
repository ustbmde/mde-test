/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

/**
 * The TestmodelTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class TestmodelTokenResolverFactory implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolverFactory {
	
	private java.util.Map<String, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver> tokenName2TokenResolver;
	private java.util.Map<String, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver> featureName2CollectInTokenResolver;
	private static edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver defaultResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelDefaultTokenResolver();
	
	public TestmodelTokenResolverFactory() {
		tokenName2TokenResolver = new java.util.LinkedHashMap<String, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver>();
		featureName2CollectInTokenResolver = new java.util.LinkedHashMap<String, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver>();
		registerTokenResolver("IDENTIFIER", new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelIDENTIFIERTokenResolver());
		registerTokenResolver("NUMBER", new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelNUMBERTokenResolver());
		registerTokenResolver("ANNOTATION", new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelANNOTATIONTokenResolver());
		registerTokenResolver("QUOTED_34_34_92", new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelQUOTED_34_34_92TokenResolver());
		registerTokenResolver("QUOTED_6037_3762_92", new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestmodelQUOTED_6037_3762_92TokenResolver());
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver internalCreateResolver(java.util.Map<String, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(java.util.Map<String, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver> resolverMap, String key, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
