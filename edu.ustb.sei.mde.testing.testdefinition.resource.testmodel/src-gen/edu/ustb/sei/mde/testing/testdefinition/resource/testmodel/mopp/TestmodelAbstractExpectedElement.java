/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class TestmodelAbstractExpectedElement implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement {
	
	private org.eclipse.emf.ecore.EClass ruleMetaclass;
	
	private java.util.Set<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]>> followers = new java.util.LinkedHashSet<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]>>();
	
	public TestmodelAbstractExpectedElement(org.eclipse.emf.ecore.EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public org.eclipse.emf.ecore.EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement follower, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[] path) {
		followers.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]>(follower, path));
	}
	
	public java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
