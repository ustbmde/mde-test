/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel;

public interface ITestmodelInterpreterListener {
	
	public void handleInterpreteObject(org.eclipse.emf.ecore.EObject element);
}
