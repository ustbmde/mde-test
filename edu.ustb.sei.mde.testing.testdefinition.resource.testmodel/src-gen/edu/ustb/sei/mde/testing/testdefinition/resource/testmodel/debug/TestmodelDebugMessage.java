/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug;

/**
 * DebugMessages are exchanged between the debug server (the Eclipse debug
 * framework) and the debug client (a running process or interpreter). To exchange
 * messages they are serialized and sent over sockets.
 */
public class TestmodelDebugMessage {
	
	private static final char DELIMITER = ':';
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.ETestmodelDebugMessageTypes messageType;
	private String[] arguments;
	
	public TestmodelDebugMessage(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.ETestmodelDebugMessageTypes messageType, String[] arguments) {
		super();
		this.messageType = messageType;
		this.arguments = arguments;
	}
	
	public TestmodelDebugMessage(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.ETestmodelDebugMessageTypes messageType, java.util.List<String> arguments) {
		super();
		this.messageType = messageType;
		this.arguments = new String[arguments.size()];
		for (int i = 0; i < arguments.size(); i++) {
			this.arguments[i] = arguments.get(i);
		}
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.ETestmodelDebugMessageTypes getMessageType() {
		return messageType;
	}
	
	public String[] getArguments() {
		return arguments;
	}
	
	public String serialize() {
		java.util.List<String> parts = new java.util.ArrayList<String>();
		parts.add(messageType.name());
		for (String argument : arguments) {
			parts.add(argument);
		}
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelStringUtil.encode(DELIMITER, parts);
	}
	
	public static TestmodelDebugMessage deserialize(String response) {
		java.util.List<String> parts = edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelStringUtil.decode(response, DELIMITER);
		String messageType = parts.get(0);
		String[] arguments = new String[parts.size() - 1];
		for (int i = 1; i < parts.size(); i++) {
			arguments[i - 1] = parts.get(i);
		}
		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.ETestmodelDebugMessageTypes type = edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.ETestmodelDebugMessageTypes.valueOf(messageType);
		TestmodelDebugMessage message = new TestmodelDebugMessage(type, arguments);
		return message;
	}
	
	public boolean hasType(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.ETestmodelDebugMessageTypes type) {
		return this.messageType == type;
	}
	
	public String getArgument(int index) {
		return getArguments()[index];
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + "[" + messageType.name() + ": " + edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelStringUtil.explode(arguments, ", ") + "]";
	}
	
}
