/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelReferenceResolverSwitch implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private java.util.Map<Object, Object> options;
	
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.EClassESuperTypesReferenceResolver eClassESuperTypesReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.EClassESuperTypesReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ETypedElementETypeReferenceResolver eTypedElementETypeReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ETypedElementETypeReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.VariableTypeReferenceResolver variableTypeReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.VariableTypeReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ReturnValueSpecificationInterfaceReferenceResolver returnValueSpecificationInterfaceReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ReturnValueSpecificationInterfaceReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ReturnValueSpecificationOperationReferenceResolver returnValueSpecificationOperationReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ReturnValueSpecificationOperationReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ReturnValueSpecificationActualParametersReferenceResolver returnValueSpecificationActualParametersReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.ReturnValueSpecificationActualParametersReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestCaseModelTestmodelReferenceResolver testCaseModelTestmodelReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestCaseModelTestmodelReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestCaseModelEPackageReferenceResolver testCaseModelEPackageReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestCaseModelEPackageReferenceResolver();
	protected edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestCaseScenarioReferenceResolver testCaseScenarioReferenceResolver = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.analysis.TestCaseScenarioReferenceResolver();
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EClass> getEClassESuperTypesReferenceResolver() {
		return getResolverChain(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass_ESuperTypes(), eClassESuperTypesReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier> getETypedElementETypeReferenceResolver() {
		return getResolverChain(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getETypedElement_EType(), eTypedElementETypeReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.Variable, org.eclipse.emf.ecore.EClassifier> getVariableTypeReferenceResolver() {
		return getResolverChain(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable_Type(), variableTypeReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EClass> getReturnValueSpecificationInterfaceReferenceResolver() {
		return getResolverChain(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification_Interface(), returnValueSpecificationInterfaceReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EOperation> getReturnValueSpecificationOperationReferenceResolver() {
		return getResolverChain(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification_Operation(), returnValueSpecificationOperationReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable> getReturnValueSpecificationActualParametersReferenceResolver() {
		return getResolverChain(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification_ActualParameters(), returnValueSpecificationActualParametersReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testcase.TestCaseModel, edu.ustb.sei.mde.testing.testdefinition.TestModel> getTestCaseModelTestmodelReferenceResolver() {
		return getResolverChain(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel_Testmodel(), testCaseModelTestmodelReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testcase.TestCaseModel, org.eclipse.emf.ecore.EPackage> getTestCaseModelEPackageReferenceResolver() {
		return getResolverChain(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel_EPackage(), testCaseModelEPackageReferenceResolver);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<edu.ustb.sei.mde.testing.testcase.TestCase, edu.ustb.sei.mde.testing.testdefinition.TestScenario> getTestCaseScenarioReferenceResolver() {
		return getResolverChain(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase_Scenario(), testCaseScenarioReferenceResolver);
	}
	
	public void setOptions(java.util.Map<?, ?> options) {
		if (options != null) {
			this.options = new java.util.LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
		eClassESuperTypesReferenceResolver.setOptions(options);
		eTypedElementETypeReferenceResolver.setOptions(options);
		variableTypeReferenceResolver.setOptions(options);
		returnValueSpecificationInterfaceReferenceResolver.setOptions(options);
		returnValueSpecificationOperationReferenceResolver.setOptions(options);
		returnValueSpecificationActualParametersReferenceResolver.setOptions(options);
		testCaseModelTestmodelReferenceResolver.setOptions(options);
		testCaseModelEPackageReferenceResolver.setOptions(options);
		testCaseScenarioReferenceResolver.setOptions(options);
	}
	
	public void resolveFuzzy(String identifier, org.eclipse.emf.ecore.EObject container, org.eclipse.emf.ecore.EReference reference, int position, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolveResult<org.eclipse.emf.ecore.EObject> result) {
		if (container == null) {
			return;
		}
		if (org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass().isInstance(container)) {
			TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClass> frr = new TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClass>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("eSuperTypes")) {
				eClassESuperTypesReferenceResolver.resolve(identifier, (org.eclipse.emf.ecore.EClass) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getETypedElement().isInstance(container)) {
			TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClassifier> frr = new TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClassifier>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("eType")) {
				eTypedElementETypeReferenceResolver.resolve(identifier, (org.eclipse.emf.ecore.ETypedElement) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable().isInstance(container)) {
			TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClassifier> frr = new TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClassifier>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("type")) {
				variableTypeReferenceResolver.resolve(identifier, (edu.ustb.sei.mde.testing.testdefinition.Variable) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification().isInstance(container)) {
			TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClass> frr = new TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EClass>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("interface")) {
				returnValueSpecificationInterfaceReferenceResolver.resolve(identifier, (edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification().isInstance(container)) {
			TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EOperation> frr = new TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EOperation>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("operation")) {
				returnValueSpecificationOperationReferenceResolver.resolve(identifier, (edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification().isInstance(container)) {
			TestmodelFuzzyResolveResult<edu.ustb.sei.mde.testing.testdefinition.Variable> frr = new TestmodelFuzzyResolveResult<edu.ustb.sei.mde.testing.testdefinition.Variable>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("actualParameters")) {
				returnValueSpecificationActualParametersReferenceResolver.resolve(identifier, (edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel().isInstance(container)) {
			TestmodelFuzzyResolveResult<edu.ustb.sei.mde.testing.testdefinition.TestModel> frr = new TestmodelFuzzyResolveResult<edu.ustb.sei.mde.testing.testdefinition.TestModel>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("testmodel")) {
				testCaseModelTestmodelReferenceResolver.resolve(identifier, (edu.ustb.sei.mde.testing.testcase.TestCaseModel) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel().isInstance(container)) {
			TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EPackage> frr = new TestmodelFuzzyResolveResult<org.eclipse.emf.ecore.EPackage>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("ePackage")) {
				testCaseModelEPackageReferenceResolver.resolve(identifier, (edu.ustb.sei.mde.testing.testcase.TestCaseModel) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
		if (edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase().isInstance(container)) {
			TestmodelFuzzyResolveResult<edu.ustb.sei.mde.testing.testdefinition.TestScenario> frr = new TestmodelFuzzyResolveResult<edu.ustb.sei.mde.testing.testdefinition.TestScenario>(result);
			String referenceName = reference.getName();
			org.eclipse.emf.ecore.EStructuralFeature feature = container.eClass().getEStructuralFeature(referenceName);
			if (feature != null && feature instanceof org.eclipse.emf.ecore.EReference && referenceName != null && referenceName.equals("scenario")) {
				testCaseScenarioReferenceResolver.resolve(identifier, (edu.ustb.sei.mde.testing.testcase.TestCase) container, (org.eclipse.emf.ecore.EReference) feature, position, true, frr);
			}
		}
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<? extends org.eclipse.emf.ecore.EObject, ? extends org.eclipse.emf.ecore.EObject> getResolver(org.eclipse.emf.ecore.EStructuralFeature reference) {
		if (reference == org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass_ESuperTypes()) {
			return getResolverChain(reference, eClassESuperTypesReferenceResolver);
		}
		if (reference == org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getETypedElement_EType()) {
			return getResolverChain(reference, eTypedElementETypeReferenceResolver);
		}
		if (reference == edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable_Type()) {
			return getResolverChain(reference, variableTypeReferenceResolver);
		}
		if (reference == edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification_Interface()) {
			return getResolverChain(reference, returnValueSpecificationInterfaceReferenceResolver);
		}
		if (reference == edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification_Operation()) {
			return getResolverChain(reference, returnValueSpecificationOperationReferenceResolver);
		}
		if (reference == edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getReturnValueSpecification_ActualParameters()) {
			return getResolverChain(reference, returnValueSpecificationActualParametersReferenceResolver);
		}
		if (reference == edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel_Testmodel()) {
			return getResolverChain(reference, testCaseModelTestmodelReferenceResolver);
		}
		if (reference == edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel_EPackage()) {
			return getResolverChain(reference, testCaseModelEPackageReferenceResolver);
		}
		if (reference == edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase_Scenario()) {
			return getResolverChain(reference, testCaseScenarioReferenceResolver);
		}
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})	
	public <ContainerType extends org.eclipse.emf.ecore.EObject, ReferenceType extends org.eclipse.emf.ecore.EObject> edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<ContainerType, ReferenceType> getResolverChain(org.eclipse.emf.ecore.EStructuralFeature reference, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof java.util.Map)) {
			// send this to the error log
			new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelRuntimeUtil().logWarning("Found value with invalid type for option " + edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + java.util.Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		java.util.Map<?,?> resolverMap = (java.util.Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver) {
			edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver replacingResolver = (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver) resolverValue;
			if (replacingResolver instanceof edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof java.util.Collection) {
			java.util.Collection replacingResolvers = (java.util.Collection) resolverValue;
			edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceCache) {
					edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver nextResolver = (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelReferenceResolver) next;
					if (nextResolver instanceof edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelRuntimeUtil().logWarning("Found value with invalid type in value map for option " + edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelRuntimeUtil().logWarning("Found value with invalid type in value map for option " + edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
