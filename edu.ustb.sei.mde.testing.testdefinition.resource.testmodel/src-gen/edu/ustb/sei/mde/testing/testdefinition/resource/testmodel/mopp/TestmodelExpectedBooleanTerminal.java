/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

/**
 * A representation for a range in a document where a boolean attribute is
 * expected.
 */
public class TestmodelExpectedBooleanTerminal extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelAbstractExpectedElement {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelBooleanTerminal booleanTerminal;
	
	public TestmodelExpectedBooleanTerminal(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelBooleanTerminal booleanTerminal) {
		super(booleanTerminal.getMetaclass());
		this.booleanTerminal = booleanTerminal;
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelBooleanTerminal getBooleanTerminal() {
		return booleanTerminal;
	}
	
	/**
	 * Returns the expected boolean terminal.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement getSymtaxElement() {
		return booleanTerminal;
	}
	
	private org.eclipse.emf.ecore.EStructuralFeature getFeature() {
		return booleanTerminal.getFeature();
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof TestmodelExpectedBooleanTerminal) {
			return getFeature().equals(((TestmodelExpectedBooleanTerminal) o).getFeature());
		}
		return false;
	}
	
	@Override	
	public int hashCode() {
		return getFeature().hashCode();
	}
	
	public java.util.Set<String> getTokenNames() {
		// BooleanTerminals are associated with two or one token(s)
		java.util.Set<String> tokenNames = new java.util.LinkedHashSet<String>(2);
		String trueLiteral = booleanTerminal.getTrueLiteral();
		if (!"".equals(trueLiteral)) {
			tokenNames.add("'" + trueLiteral + "'");
		}
		String falseLiteral = booleanTerminal.getFalseLiteral();
		if (!"".equals(falseLiteral)) {
			tokenNames.add("'" + falseLiteral + "'");
		}
		return tokenNames;
	}
	
}
