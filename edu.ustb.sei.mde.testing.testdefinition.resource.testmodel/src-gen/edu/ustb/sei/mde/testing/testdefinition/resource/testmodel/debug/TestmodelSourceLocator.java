/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug;

public class TestmodelSourceLocator extends org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector {
	
	public void initializeParticipants() {
		addParticipants(new org.eclipse.debug.core.sourcelookup.ISourceLookupParticipant[]{new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.TestmodelSourceLookupParticipant()});
	}
	
}
