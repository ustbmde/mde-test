/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelParseResult implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelParseResult {
	
	private org.eclipse.emf.ecore.EObject root;
	private java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>> commands = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>>();
	
	public TestmodelParseResult() {
		super();
	}
	
	public void setRoot(org.eclipse.emf.ecore.EObject root) {
		this.root = root;
	}
	
	public org.eclipse.emf.ecore.EObject getRoot() {
		return root;
	}
	
	public java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
