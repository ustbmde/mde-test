/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

/**
 * A class to represent a keyword in the grammar.
 */
public class TestmodelKeyword extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement {
	
	private final String value;
	
	public TestmodelKeyword(String value, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return value;
	}
	
}
