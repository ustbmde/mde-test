/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

public enum TestmodelCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
