/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("interface".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("type".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("extends".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("model".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("test".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("do".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("var".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("in".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("assert".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("module".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("based on".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("import".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x80, 0x00, 0x55}, null, true, false, false, false);
		}
		if ("QUOTED_34_34_92".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x2A, 0x00, 0xFF}, null, false, false, false, false);
		}
		if ("QUOTED_6037_3762_92".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x2A, 0x00, 0xFF}, null, false, false, false, false);
		}
		if ("TASK_ITEM".equals(tokenName)) {
			return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		return null;
	}
	
}
