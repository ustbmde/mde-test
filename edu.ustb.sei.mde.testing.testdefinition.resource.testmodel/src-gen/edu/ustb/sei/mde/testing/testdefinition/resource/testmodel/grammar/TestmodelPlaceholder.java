/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar;

/**
 * A class to represent placeholders in a grammar.
 */
public class TestmodelPlaceholder extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelTerminal {
	
	private final String tokenName;
	
	public TestmodelPlaceholder(org.eclipse.emf.ecore.EStructuralFeature feature, String tokenName, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
