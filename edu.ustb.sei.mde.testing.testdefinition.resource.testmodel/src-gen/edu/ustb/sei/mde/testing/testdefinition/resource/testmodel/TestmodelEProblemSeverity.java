/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel;

public enum TestmodelEProblemSeverity {
	WARNING, ERROR;
}
