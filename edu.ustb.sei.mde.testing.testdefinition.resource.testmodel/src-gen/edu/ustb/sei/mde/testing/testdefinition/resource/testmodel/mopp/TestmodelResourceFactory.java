/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelResourceFactory implements org.eclipse.emf.ecore.resource.Resource.Factory {
	
	public TestmodelResourceFactory() {
		super();
	}
	
	public org.eclipse.emf.ecore.resource.Resource createResource(org.eclipse.emf.common.util.URI uri) {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResource(uri);
	}
	
}
