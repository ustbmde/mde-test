// $ANTLR 3.4

	package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class TestmodelParser extends TestmodelANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ANNOTATION", "IDENTIFIER", "LINEBREAK", "ML_COMMENT", "NUMBER", "QUOTED_34_34_92", "QUOTED_6037_3762_92", "SL_COMMENT", "TEXT", "WHITESPACE", "'('", "')'", "'*'", "','", "'.'", "'..'", "':'", "';'", "'='", "'['", "']'", "'assert'", "'based on'", "'do'", "'extends'", "'import'", "'in'", "'interface'", "'model'", "'module'", "'test'", "'type'", "'var'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__19=19;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int ANNOTATION=4;
    public static final int IDENTIFIER=5;
    public static final int LINEBREAK=6;
    public static final int ML_COMMENT=7;
    public static final int NUMBER=8;
    public static final int QUOTED_34_34_92=9;
    public static final int QUOTED_6037_3762_92=10;
    public static final int SL_COMMENT=11;
    public static final int TEXT=12;
    public static final int WHITESPACE=13;

    // delegates
    public TestmodelANTLRParserBase[] getDelegates() {
        return new TestmodelANTLRParserBase[] {};
    }

    // delegators


    public TestmodelParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public TestmodelParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(64 + 1);
         

    }

    public String[] getTokenNames() { return TestmodelParser.tokenNames; }
    public String getGrammarFileName() { return "Testmodel.g"; }


    	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolverFactory tokenResolverFactory = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> expectedElements = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected java.util.List<org.antlr.runtime3_4_0.RecognitionException> lexerExceptions = java.util.Collections.synchronizedList(new java.util.ArrayList<org.antlr.runtime3_4_0.RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected java.util.List<Integer> lexerExceptionsPosition = java.util.Collections.synchronizedList(new java.util.ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	java.util.List<org.eclipse.emf.ecore.EObject> incompleteObjects = new java.util.ArrayList<org.eclipse.emf.ecore.EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
    			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelProblem() {
    					public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity getSeverity() {
    						return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity.ERROR;
    					}
    					public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType getType() {
    						return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	public void addExpectedElement(org.eclipse.emf.ecore.EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement terminal = edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelFollowSetProvider.TERMINALS[terminalID];
    		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[] containmentFeatures = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelFollowSetProvider.LINKS[ids[i]];
    		}
    		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace containmentTrace = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace(eClass, containmentFeatures);
    		org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal expectedElement = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(org.eclipse.emf.ecore.EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final org.eclipse.emf.ecore.EObject source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
    			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
    				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final org.antlr.runtime3_4_0.CommonToken source, final org.eclipse.emf.ecore.EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
    			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
    				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>> postParseCommands , final org.eclipse.emf.ecore.EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		postParseCommands.add(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>() {
    			public boolean execute(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource resource) {
    				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelLocationMap locationMap = resource.getLocationMap();
    				if (locationMap == null) {
    					// the locationMap can be null if the parser is used for code completion
    					return true;
    				}
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextParser createInstance(java.io.InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new TestmodelParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TestmodelLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream))));
    			} else {
    				return new TestmodelParser(new org.antlr.runtime3_4_0.CommonTokenStream(new TestmodelLexer(new org.antlr.runtime3_4_0.ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (java.io.IOException e) {
    			new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public TestmodelParser() {
    		super(null);
    	}
    	
    	protected org.eclipse.emf.ecore.EObject doParse() throws org.antlr.runtime3_4_0.RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((TestmodelLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((TestmodelLexer) getTokenStream().getTokenSource()).lexerExceptionsPosition = lexerExceptionsPosition;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof org.eclipse.emf.ecore.EClass) {
    			org.eclipse.emf.ecore.EClass type = (org.eclipse.emf.ecore.EClass) typeObject;
    			if (type.getInstanceClass() == org.eclipse.emf.ecore.EClass.class) {
    				return parse_org_eclipse_emf_ecore_EClass();
    			}
    			if (type.getInstanceClass() == org.eclipse.emf.ecore.EAttribute.class) {
    				return parse_org_eclipse_emf_ecore_EAttribute();
    			}
    			if (type.getInstanceClass() == org.eclipse.emf.ecore.EReference.class) {
    				return parse_org_eclipse_emf_ecore_EReference();
    			}
    			if (type.getInstanceClass() == org.eclipse.emf.ecore.EOperation.class) {
    				return parse_org_eclipse_emf_ecore_EOperation();
    			}
    			if (type.getInstanceClass() == org.eclipse.emf.ecore.EParameter.class) {
    				return parse_org_eclipse_emf_ecore_EParameter();
    			}
    			if (type.getInstanceClass() == org.eclipse.emf.ecore.EAnnotation.class) {
    				return parse_org_eclipse_emf_ecore_EAnnotation();
    			}
    			if (type.getInstanceClass() == java.util.Map.Entry.class) {
    				return parse_java_util_Map_Entry();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.TestModel.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_TestModel();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.TestScenario.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.Script.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_Script();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.Variable.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_Variable();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testdefinition.Oracle.class) {
    				return parse_edu_ustb_sei_mde_testing_testdefinition_Oracle();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testcase.TestCaseModel.class) {
    				return parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testcase.TestCase.class) {
    				return parse_edu_ustb_sei_mde_testing_testcase_TestCase();
    			}
    			if (type.getInstanceClass() == edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification.class) {
    				return parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification();
    			}
    		}
    		throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(org.antlr.runtime3_4_0.IntStream arg0, org.antlr.runtime3_4_0.RecognitionException arg1, int arg2, org.antlr.runtime3_4_0.BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		java.util.Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelParseResult parse() {
    		terminateParsing = false;
    		postParseCommands = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource>>();
    		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelParseResult parseResult = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelParseResult();
    		try {
    			org.eclipse.emf.ecore.EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    			}
    		} catch (org.antlr.runtime3_4_0.RecognitionException re) {
    			reportError(re);
    		} catch (java.lang.IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (org.antlr.runtime3_4_0.RecognitionException re : lexerExceptions) {
    			reportLexicalError(re);
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> parseToExpectedElements(org.eclipse.emf.ecore.EClass type, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final org.antlr.runtime3_4_0.CommonTokenStream tokenStream = (org.antlr.runtime3_4_0.CommonTokenStream) getTokenStream();
    		edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelParseResult result = parse();
    		for (org.eclipse.emf.ecore.EObject incompleteObject : incompleteObjects) {
    			org.antlr.runtime3_4_0.Lexer lexer = (org.antlr.runtime3_4_0.Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			org.eclipse.emf.ecore.EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelCommand<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		java.util.Set<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> currentFollowSet = new java.util.LinkedHashSet<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal>();
    		java.util.List<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal> newFollowSet = new java.util.ArrayList<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 154;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			org.antlr.runtime3_4_0.CommonToken nextToken = (org.antlr.runtime3_4_0.CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelPair<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContainedFeature[]> newFollowerPair : newFollowers) {
    							edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelExpectedElement newFollower = newFollowerPair.getLeft();
    							org.eclipse.emf.ecore.EObject container = getLastIncompleteElement();
    							edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace containmentTrace = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelContainmentTrace(null, newFollowerPair.getRight());
    							edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal newFollowTerminal = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			org.antlr.runtime3_4_0.CommonToken tokenAtIndex = (org.antlr.runtime3_4_0.CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(org.antlr.runtime3_4_0.IntStream input, int ttype, org.antlr.runtime3_4_0.BitSet follow) throws org.antlr.runtime3_4_0.RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the parser into human readable messages.
    	 */
    	public void reportError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = e.getMessage();
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			String expectedTokenName = formatTokenName(mte.expecting);
    			String actualTokenName = formatTokenName(e.token.getType());
    			message = "Syntax error on token \"" + e.token.getText() + " (" + actualTokenName + ")\", \"" + expectedTokenName + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedTreeNodeException) {
    			org.antlr.runtime3_4_0.MismatchedTreeNodeException mtne = (org.antlr.runtime3_4_0.MismatchedTreeNodeException) e;
    			String expectedTokenName = formatTokenName(mtne.expecting);
    			message = "mismatched tree node: " + "xxx" + "; tokenName " + expectedTokenName;
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", check following tokens";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			message = "Syntax error on token \"" + e.token.getText() + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched token: " + e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched token: " +  e.token + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" +  fpe.predicateText + "}?";
    		}
    		// the resource may be null if the parser is used for code completion
    		final String finalMessage = message;
    		if (e.token instanceof org.antlr.runtime3_4_0.CommonToken) {
    			final org.antlr.runtime3_4_0.CommonToken ct = (org.antlr.runtime3_4_0.CommonToken) e.token;
    			addErrorToResource(finalMessage, ct.getCharPositionInLine(), ct.getLine(), ct.getStartIndex(), ct.getStopIndex());
    		} else {
    			addErrorToResource(finalMessage, e.token.getCharPositionInLine(), e.token.getLine(), 1, 5);
    		}
    	}
    	
    	/**
    	 * Translates errors thrown by the lexer into human readable messages.
    	 */
    	public void reportLexicalError(final org.antlr.runtime3_4_0.RecognitionException e)  {
    		String message = "";
    		if (e instanceof org.antlr.runtime3_4_0.MismatchedTokenException) {
    			org.antlr.runtime3_4_0.MismatchedTokenException mte = (org.antlr.runtime3_4_0.MismatchedTokenException) e;
    			message = "Syntax error on token \"" + ((char) e.c) + "\", \"" + (char) mte.expecting + "\" expected";
    		} else if (e instanceof org.antlr.runtime3_4_0.NoViableAltException) {
    			message = "Syntax error on token \"" + ((char) e.c) + "\", delete this token";
    		} else if (e instanceof org.antlr.runtime3_4_0.EarlyExitException) {
    			org.antlr.runtime3_4_0.EarlyExitException eee = (org.antlr.runtime3_4_0.EarlyExitException) e;
    			message = "required (...)+ loop (decision=" + eee.decisionNumber + ") did not match anything; on line " + e.line + ":" + e.charPositionInLine + " char=" + ((char) e.c) + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedSetException) {
    			org.antlr.runtime3_4_0.MismatchedSetException mse = (org.antlr.runtime3_4_0.MismatchedSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedNotSetException) {
    			org.antlr.runtime3_4_0.MismatchedNotSetException mse = (org.antlr.runtime3_4_0.MismatchedNotSetException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set " + mse.expecting;
    		} else if (e instanceof org.antlr.runtime3_4_0.MismatchedRangeException) {
    			org.antlr.runtime3_4_0.MismatchedRangeException mre = (org.antlr.runtime3_4_0.MismatchedRangeException) e;
    			message = "mismatched char: '" + ((char) e.c) + "' on line " + e.line + ":" + e.charPositionInLine + "; expecting set '" + (char) mre.a + "'..'" + (char) mre.b + "'";
    		} else if (e instanceof org.antlr.runtime3_4_0.FailedPredicateException) {
    			org.antlr.runtime3_4_0.FailedPredicateException fpe = (org.antlr.runtime3_4_0.FailedPredicateException) e;
    			message = "rule " + fpe.ruleName + " failed predicate: {" + fpe.predicateText + "}?";
    		}
    		addErrorToResource(message, e.charPositionInLine, e.line, lexerExceptionsPosition.get(lexerExceptions.indexOf(e)), lexerExceptionsPosition.get(lexerExceptions.indexOf(e)));
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.incompleteObjects.add((org.eclipse.emf.ecore.EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof org.eclipse.emf.ecore.EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private org.eclipse.emf.ecore.EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Testmodel.g:553:1: start returns [ org.eclipse.emf.ecore.EObject element = null] : (c0= parse_edu_ustb_sei_mde_testing_testdefinition_TestModel |c1= parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel ) EOF ;
    public final org.eclipse.emf.ecore.EObject start() throws RecognitionException {
        org.eclipse.emf.ecore.EObject element =  null;

        int start_StartIndex = input.index();

        edu.ustb.sei.mde.testing.testdefinition.TestModel c0 =null;

        edu.ustb.sei.mde.testing.testcase.TestCaseModel c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Testmodel.g:554:2: ( (c0= parse_edu_ustb_sei_mde_testing_testdefinition_TestModel |c1= parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel ) EOF )
            // Testmodel.g:555:2: (c0= parse_edu_ustb_sei_mde_testing_testdefinition_TestModel |c1= parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[0]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[1]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Testmodel.g:561:2: (c0= parse_edu_ustb_sei_mde_testing_testdefinition_TestModel |c1= parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==32) ) {
                alt1=1;
            }
            else if ( (LA1_0==33) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }
            switch (alt1) {
                case 1 :
                    // Testmodel.g:562:3: c0= parse_edu_ustb_sei_mde_testing_testdefinition_TestModel
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel_in_start82);
                    c0=parse_edu_ustb_sei_mde_testing_testdefinition_TestModel();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; }

                    }
                    break;
                case 2 :
                    // Testmodel.g:563:8: c1= parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel_in_start96);
                    c1=parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; }

                    }
                    break;

            }


            match(input,EOF,FOLLOW_EOF_in_start103); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_org_eclipse_emf_ecore_EClass"
    // Testmodel.g:571:1: parse_org_eclipse_emf_ecore_EClass returns [org.eclipse.emf.ecore.EClass element = null] : ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* ( (a1= 'interface' |a2= 'type' ) ) (a4= IDENTIFIER ) ( (a5= 'extends' (a6= IDENTIFIER ) ( (a7= ',' (a8= IDENTIFIER ) ) )* ) )? a9= '{' ( (a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature ) )* ( (a11_0= parse_org_eclipse_emf_ecore_EOperation ) )* a12= '}' ;
    public final org.eclipse.emf.ecore.EClass parse_org_eclipse_emf_ecore_EClass() throws RecognitionException {
        org.eclipse.emf.ecore.EClass element =  null;

        int parse_org_eclipse_emf_ecore_EClass_StartIndex = input.index();

        Token a1=null;
        Token a2=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        Token a12=null;
        org.eclipse.emf.ecore.EAnnotation a0_0 =null;

        org.eclipse.emf.ecore.EStructuralFeature a10_0 =null;

        org.eclipse.emf.ecore.EOperation a11_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Testmodel.g:574:2: ( ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* ( (a1= 'interface' |a2= 'type' ) ) (a4= IDENTIFIER ) ( (a5= 'extends' (a6= IDENTIFIER ) ( (a7= ',' (a8= IDENTIFIER ) ) )* ) )? a9= '{' ( (a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature ) )* ( (a11_0= parse_org_eclipse_emf_ecore_EOperation ) )* a12= '}' )
            // Testmodel.g:575:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* ( (a1= 'interface' |a2= 'type' ) ) (a4= IDENTIFIER ) ( (a5= 'extends' (a6= IDENTIFIER ) ( (a7= ',' (a8= IDENTIFIER ) ) )* ) )? a9= '{' ( (a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature ) )* ( (a11_0= parse_org_eclipse_emf_ecore_EOperation ) )* a12= '}'
            {
            // Testmodel.g:575:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==ANNOTATION) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Testmodel.g:576:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    {
            	    // Testmodel.g:576:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    // Testmodel.g:577:4: a0_0= parse_org_eclipse_emf_ecore_EAnnotation
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EClass138);
            	    a0_0=parse_org_eclipse_emf_ecore_EAnnotation();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a0_0 != null) {
            	    					if (a0_0 != null) {
            	    						Object value = a0_0;
            	    						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__EANNOTATIONS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_0, a0_0, true);
            	    					copyLocalizationInfos(a0_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[2]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[3]);
            	}

            // Testmodel.g:604:2: ( (a1= 'interface' |a2= 'type' ) )
            // Testmodel.g:605:3: (a1= 'interface' |a2= 'type' )
            {
            // Testmodel.g:605:3: (a1= 'interface' |a2= 'type' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==31) ) {
                alt3=1;
            }
            else if ( (LA3_0==35) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // Testmodel.g:606:4: a1= 'interface'
                    {
                    a1=(Token)match(input,31,FOLLOW_31_in_parse_org_eclipse_emf_ecore_EClass173); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_1, true, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    				// set value of boolean attribute
                    				Object value = true;
                    				element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INTERFACE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;
                case 2 :
                    // Testmodel.g:619:8: a2= 'type'
                    {
                    a2=(Token)match(input,35,FOLLOW_35_in_parse_org_eclipse_emf_ecore_EClass188); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_1, false, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    				// set value of boolean attribute
                    				Object value = false;
                    				element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__INTERFACE), value);
                    				completedElement(value, false);
                    			}

                    }
                    break;

            }


            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[4]);
            	}

            // Testmodel.g:639:2: (a4= IDENTIFIER )
            // Testmodel.g:640:3: a4= IDENTIFIER
            {
            a4=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EClass213); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[5]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[6]);
            	}

            // Testmodel.g:676:2: ( (a5= 'extends' (a6= IDENTIFIER ) ( (a7= ',' (a8= IDENTIFIER ) ) )* ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==28) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // Testmodel.g:677:3: (a5= 'extends' (a6= IDENTIFIER ) ( (a7= ',' (a8= IDENTIFIER ) ) )* )
                    {
                    // Testmodel.g:677:3: (a5= 'extends' (a6= IDENTIFIER ) ( (a7= ',' (a8= IDENTIFIER ) ) )* )
                    // Testmodel.g:678:4: a5= 'extends' (a6= IDENTIFIER ) ( (a7= ',' (a8= IDENTIFIER ) ) )*
                    {
                    a5=(Token)match(input,28,FOLLOW_28_in_parse_org_eclipse_emf_ecore_EClass243); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[7]);
                    			}

                    // Testmodel.g:692:4: (a6= IDENTIFIER )
                    // Testmodel.g:693:5: a6= IDENTIFIER
                    {
                    a6=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EClass269); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    						startIncompleteElement(element);
                    					}
                    					if (a6 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEClassESuperTypesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES, value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_1, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[8]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[9]);
                    			}

                    // Testmodel.g:733:4: ( (a7= ',' (a8= IDENTIFIER ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==17) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // Testmodel.g:734:5: (a7= ',' (a8= IDENTIFIER ) )
                    	    {
                    	    // Testmodel.g:734:5: (a7= ',' (a8= IDENTIFIER ) )
                    	    // Testmodel.g:735:6: a7= ',' (a8= IDENTIFIER )
                    	    {
                    	    a7=(Token)match(input,17,FOLLOW_17_in_parse_org_eclipse_emf_ecore_EClass315); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (element == null) {
                    	    							element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    	    							startIncompleteElement(element);
                    	    						}
                    	    						collectHiddenTokens(element);
                    	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_2_0_0_0, null, true);
                    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
                    	    					}

                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[10]);
                    	    					}

                    	    // Testmodel.g:749:6: (a8= IDENTIFIER )
                    	    // Testmodel.g:750:7: a8= IDENTIFIER
                    	    {
                    	    a8=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EClass349); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a8 != null) {
                    	    								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                    	    								tokenResolver.setOptions(getOptions());
                    	    								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    	    								tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), result);
                    	    								Object resolvedObject = result.getResolvedToken();
                    	    								if (resolvedObject == null) {
                    	    									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
                    	    								}
                    	    								String resolved = (String) resolvedObject;
                    	    								org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
                    	    								collectHiddenTokens(element);
                    	    								registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.EClass, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getEClassESuperTypesReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES), resolved, proxy);
                    	    								if (proxy != null) {
                    	    									Object value = proxy;
                    	    									addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__ESUPER_TYPES, value);
                    	    									completedElement(value, false);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_3_0_0_2_0_0_1, proxy, true);
                    	    								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
                    	    								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, proxy);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[11]);
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[12]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[13]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[14]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[15]);
            	}

            a9=(Token)match(input,37,FOLLOW_37_in_parse_org_eclipse_emf_ecore_EClass430); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[16]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[17]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[18]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[19]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[20]);
            	}

            // Testmodel.g:823:2: ( (a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature ) )*
            loop6:
            do {
                int alt6=2;
                alt6 = dfa6.predict(input);
                switch (alt6) {
            	case 1 :
            	    // Testmodel.g:824:3: (a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature )
            	    {
            	    // Testmodel.g:824:3: (a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature )
            	    // Testmodel.g:825:4: a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EStructuralFeature_in_parse_org_eclipse_emf_ecore_EClass453);
            	    a10_0=parse_org_eclipse_emf_ecore_EStructuralFeature();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a10_0 != null) {
            	    					if (a10_0 != null) {
            	    						Object value = a10_0;
            	    						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__ESTRUCTURAL_FEATURES, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_5, a10_0, true);
            	    					copyLocalizationInfos(a10_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[21]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[22]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[23]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[24]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[25]);
            	}

            // Testmodel.g:855:2: ( (a11_0= parse_org_eclipse_emf_ecore_EOperation ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0 >= ANNOTATION && LA7_0 <= IDENTIFIER)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // Testmodel.g:856:3: (a11_0= parse_org_eclipse_emf_ecore_EOperation )
            	    {
            	    // Testmodel.g:856:3: (a11_0= parse_org_eclipse_emf_ecore_EOperation )
            	    // Testmodel.g:857:4: a11_0= parse_org_eclipse_emf_ecore_EOperation
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EOperation_in_parse_org_eclipse_emf_ecore_EClass488);
            	    a11_0=parse_org_eclipse_emf_ecore_EOperation();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a11_0 != null) {
            	    					if (a11_0 != null) {
            	    						Object value = a11_0;
            	    						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.ECLASS__EOPERATIONS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_6, a11_0, true);
            	    					copyLocalizationInfos(a11_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[26]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[27]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[28]);
            	}

            a12=(Token)match(input,38,FOLLOW_38_in_parse_org_eclipse_emf_ecore_EClass514); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_0_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[29]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[30]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[31]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_org_eclipse_emf_ecore_EClass_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_emf_ecore_EClass"



    // $ANTLR start "parse_org_eclipse_emf_ecore_EAttribute"
    // Testmodel.g:903:1: parse_org_eclipse_emf_ecore_EAttribute returns [org.eclipse.emf.ecore.EAttribute element = null] : ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) a8= ';' ;
    public final org.eclipse.emf.ecore.EAttribute parse_org_eclipse_emf_ecore_EAttribute() throws RecognitionException {
        org.eclipse.emf.ecore.EAttribute element =  null;

        int parse_org_eclipse_emf_ecore_EAttribute_StartIndex = input.index();

        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        org.eclipse.emf.ecore.EAnnotation a0_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Testmodel.g:906:2: ( ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) a8= ';' )
            // Testmodel.g:907:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) a8= ';'
            {
            // Testmodel.g:907:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==ANNOTATION) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // Testmodel.g:908:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    {
            	    // Testmodel.g:908:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    // Testmodel.g:909:4: a0_0= parse_org_eclipse_emf_ecore_EAnnotation
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EAttribute552);
            	    a0_0=parse_org_eclipse_emf_ecore_EAnnotation();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a0_0 != null) {
            	    					if (a0_0 != null) {
            	    						Object value = a0_0;
            	    						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__EANNOTATIONS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_0, a0_0, true);
            	    					copyLocalizationInfos(a0_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAttribute(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[32]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[33]);
            	}

            // Testmodel.g:936:2: (a1= IDENTIFIER )
            // Testmodel.g:937:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EAttribute582); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__ETYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[34]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[35]);
            	}

            // Testmodel.g:977:2: ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==23) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // Testmodel.g:978:3: (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' )
                    {
                    // Testmodel.g:978:3: (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' )
                    // Testmodel.g:979:4: a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']'
                    {
                    a2=(Token)match(input,23,FOLLOW_23_in_parse_org_eclipse_emf_ecore_EAttribute612); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[36]);
                    			}

                    // Testmodel.g:993:4: (a3= NUMBER )
                    // Testmodel.g:994:5: a3= NUMBER
                    {
                    a3=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EAttribute638); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
                    						startIncompleteElement(element);
                    					}
                    					if (a3 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__LOWER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__LOWER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_1, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[37]);
                    			}

                    a4=(Token)match(input,19,FOLLOW_19_in_parse_org_eclipse_emf_ecore_EAttribute671); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_2, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[38]);
                    			}

                    // Testmodel.g:1043:4: (a5= NUMBER )
                    // Testmodel.g:1044:5: a5= NUMBER
                    {
                    a5=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EAttribute697); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
                    						startIncompleteElement(element);
                    					}
                    					if (a5 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UPPER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__UPPER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_3, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[39]);
                    			}

                    a6=(Token)match(input,24,FOLLOW_24_in_parse_org_eclipse_emf_ecore_EAttribute730); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_2_0_0_4, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[40]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[41]);
            	}

            // Testmodel.g:1100:2: (a7= IDENTIFIER )
            // Testmodel.g:1101:3: a7= IDENTIFIER
            {
            a7=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EAttribute767); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
            				startIncompleteElement(element);
            			}
            			if (a7 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EATTRIBUTE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_3, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[42]);
            	}

            a8=(Token)match(input,21,FOLLOW_21_in_parse_org_eclipse_emf_ecore_EAttribute788); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAttribute();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_1_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[43]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[44]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[45]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[46]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[47]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_org_eclipse_emf_ecore_EAttribute_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_emf_ecore_EAttribute"



    // $ANTLR start "parse_org_eclipse_emf_ecore_EReference"
    // Testmodel.g:1156:1: parse_org_eclipse_emf_ecore_EReference returns [org.eclipse.emf.ecore.EReference element = null] : ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) a2= '*' ( (a3= '[' (a4= NUMBER ) a5= '..' (a6= NUMBER ) a7= ']' ) )? (a8= IDENTIFIER ) a9= ';' ;
    public final org.eclipse.emf.ecore.EReference parse_org_eclipse_emf_ecore_EReference() throws RecognitionException {
        org.eclipse.emf.ecore.EReference element =  null;

        int parse_org_eclipse_emf_ecore_EReference_StartIndex = input.index();

        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a9=null;
        org.eclipse.emf.ecore.EAnnotation a0_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Testmodel.g:1159:2: ( ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) a2= '*' ( (a3= '[' (a4= NUMBER ) a5= '..' (a6= NUMBER ) a7= ']' ) )? (a8= IDENTIFIER ) a9= ';' )
            // Testmodel.g:1160:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) a2= '*' ( (a3= '[' (a4= NUMBER ) a5= '..' (a6= NUMBER ) a7= ']' ) )? (a8= IDENTIFIER ) a9= ';'
            {
            // Testmodel.g:1160:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==ANNOTATION) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // Testmodel.g:1161:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    {
            	    // Testmodel.g:1161:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    // Testmodel.g:1162:4: a0_0= parse_org_eclipse_emf_ecore_EAnnotation
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EReference826);
            	    a0_0=parse_org_eclipse_emf_ecore_EAnnotation();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a0_0 != null) {
            	    					if (a0_0 != null) {
            	    						Object value = a0_0;
            	    						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EREFERENCE__EANNOTATIONS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_0, a0_0, true);
            	    					copyLocalizationInfos(a0_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEReference(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[48]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[49]);
            	}

            // Testmodel.g:1189:2: (a1= IDENTIFIER )
            // Testmodel.g:1190:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EReference856); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__ETYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[50]);
            	}

            a2=(Token)match(input,16,FOLLOW_16_in_parse_org_eclipse_emf_ecore_EReference877); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[51]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[52]);
            	}

            // Testmodel.g:1244:2: ( (a3= '[' (a4= NUMBER ) a5= '..' (a6= NUMBER ) a7= ']' ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // Testmodel.g:1245:3: (a3= '[' (a4= NUMBER ) a5= '..' (a6= NUMBER ) a7= ']' )
                    {
                    // Testmodel.g:1245:3: (a3= '[' (a4= NUMBER ) a5= '..' (a6= NUMBER ) a7= ']' )
                    // Testmodel.g:1246:4: a3= '[' (a4= NUMBER ) a5= '..' (a6= NUMBER ) a7= ']'
                    {
                    a3=(Token)match(input,23,FOLLOW_23_in_parse_org_eclipse_emf_ecore_EReference900); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[53]);
                    			}

                    // Testmodel.g:1260:4: (a4= NUMBER )
                    // Testmodel.g:1261:5: a4= NUMBER
                    {
                    a4=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EReference926); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
                    						startIncompleteElement(element);
                    					}
                    					if (a4 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__LOWER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__LOWER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_1, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[54]);
                    			}

                    a5=(Token)match(input,19,FOLLOW_19_in_parse_org_eclipse_emf_ecore_EReference959); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_2, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[55]);
                    			}

                    // Testmodel.g:1310:4: (a6= NUMBER )
                    // Testmodel.g:1311:5: a6= NUMBER
                    {
                    a6=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EReference985); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
                    						startIncompleteElement(element);
                    					}
                    					if (a6 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a6.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UPPER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a6).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a6).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__UPPER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_3, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a6, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[56]);
                    			}

                    a7=(Token)match(input,24,FOLLOW_24_in_parse_org_eclipse_emf_ecore_EReference1018); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_3_0_0_4, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[57]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[58]);
            	}

            // Testmodel.g:1367:2: (a8= IDENTIFIER )
            // Testmodel.g:1368:3: a8= IDENTIFIER
            {
            a8=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EReference1055); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
            				startIncompleteElement(element);
            			}
            			if (a8 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a8.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a8).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a8).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EREFERENCE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_4, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a8, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[59]);
            	}

            a9=(Token)match(input,21,FOLLOW_21_in_parse_org_eclipse_emf_ecore_EReference1076); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEReference();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_2_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a9, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[60]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[61]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[62]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[63]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[64]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_org_eclipse_emf_ecore_EReference_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_emf_ecore_EReference"



    // $ANTLR start "parse_org_eclipse_emf_ecore_EOperation"
    // Testmodel.g:1423:1: parse_org_eclipse_emf_ecore_EOperation returns [org.eclipse.emf.ecore.EOperation element = null] : ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) a8= '(' ( ( (a9_0= parse_org_eclipse_emf_ecore_EParameter ) ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )* ) )* a12= ')' a13= ';' ;
    public final org.eclipse.emf.ecore.EOperation parse_org_eclipse_emf_ecore_EOperation() throws RecognitionException {
        org.eclipse.emf.ecore.EOperation element =  null;

        int parse_org_eclipse_emf_ecore_EOperation_StartIndex = input.index();

        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;
        Token a10=null;
        Token a12=null;
        Token a13=null;
        org.eclipse.emf.ecore.EAnnotation a0_0 =null;

        org.eclipse.emf.ecore.EParameter a9_0 =null;

        org.eclipse.emf.ecore.EParameter a11_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Testmodel.g:1426:2: ( ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) a8= '(' ( ( (a9_0= parse_org_eclipse_emf_ecore_EParameter ) ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )* ) )* a12= ')' a13= ';' )
            // Testmodel.g:1427:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) a8= '(' ( ( (a9_0= parse_org_eclipse_emf_ecore_EParameter ) ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )* ) )* a12= ')' a13= ';'
            {
            // Testmodel.g:1427:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==ANNOTATION) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // Testmodel.g:1428:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    {
            	    // Testmodel.g:1428:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    // Testmodel.g:1429:4: a0_0= parse_org_eclipse_emf_ecore_EAnnotation
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EOperation1114);
            	    a0_0=parse_org_eclipse_emf_ecore_EAnnotation();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a0_0 != null) {
            	    					if (a0_0 != null) {
            	    						Object value = a0_0;
            	    						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EOPERATION__EANNOTATIONS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_0, a0_0, true);
            	    					copyLocalizationInfos(a0_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[65]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[66]);
            	}

            // Testmodel.g:1456:2: (a1= IDENTIFIER )
            // Testmodel.g:1457:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EOperation1144); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__ETYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[67]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[68]);
            	}

            // Testmodel.g:1497:2: ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==23) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // Testmodel.g:1498:3: (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' )
                    {
                    // Testmodel.g:1498:3: (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' )
                    // Testmodel.g:1499:4: a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']'
                    {
                    a2=(Token)match(input,23,FOLLOW_23_in_parse_org_eclipse_emf_ecore_EOperation1174); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[69]);
                    			}

                    // Testmodel.g:1513:4: (a3= NUMBER )
                    // Testmodel.g:1514:5: a3= NUMBER
                    {
                    a3=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EOperation1200); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
                    						startIncompleteElement(element);
                    					}
                    					if (a3 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__LOWER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__LOWER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_1, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[70]);
                    			}

                    a4=(Token)match(input,19,FOLLOW_19_in_parse_org_eclipse_emf_ecore_EOperation1233); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_2, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[71]);
                    			}

                    // Testmodel.g:1563:4: (a5= NUMBER )
                    // Testmodel.g:1564:5: a5= NUMBER
                    {
                    a5=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EOperation1259); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
                    						startIncompleteElement(element);
                    					}
                    					if (a5 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UPPER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__UPPER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_3, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[72]);
                    			}

                    a6=(Token)match(input,24,FOLLOW_24_in_parse_org_eclipse_emf_ecore_EOperation1292); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_2_0_0_4, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[73]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[74]);
            	}

            // Testmodel.g:1620:2: (a7= IDENTIFIER )
            // Testmodel.g:1621:3: a7= IDENTIFIER
            {
            a7=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EOperation1329); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            				startIncompleteElement(element);
            			}
            			if (a7 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EOPERATION__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_3, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[75]);
            	}

            a8=(Token)match(input,14,FOLLOW_14_in_parse_org_eclipse_emf_ecore_EOperation1350); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[76]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[77]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[78]);
            	}

            // Testmodel.g:1672:2: ( ( (a9_0= parse_org_eclipse_emf_ecore_EParameter ) ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )* ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0 >= ANNOTATION && LA15_0 <= IDENTIFIER)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // Testmodel.g:1673:3: ( (a9_0= parse_org_eclipse_emf_ecore_EParameter ) ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )* )
            	    {
            	    // Testmodel.g:1673:3: ( (a9_0= parse_org_eclipse_emf_ecore_EParameter ) ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )* )
            	    // Testmodel.g:1674:4: (a9_0= parse_org_eclipse_emf_ecore_EParameter ) ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )*
            	    {
            	    // Testmodel.g:1674:4: (a9_0= parse_org_eclipse_emf_ecore_EParameter )
            	    // Testmodel.g:1675:5: a9_0= parse_org_eclipse_emf_ecore_EParameter
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EParameter_in_parse_org_eclipse_emf_ecore_EOperation1379);
            	    a9_0=parse_org_eclipse_emf_ecore_EParameter();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a9_0 != null) {
            	    						if (a9_0 != null) {
            	    							Object value = a9_0;
            	    							addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EOPERATION__EPARAMETERS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_5_0_0_0, a9_0, true);
            	    						copyLocalizationInfos(a9_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[79]);
            	    				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[80]);
            	    				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[81]);
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[82]);
            	    			}

            	    // Testmodel.g:1703:4: ( (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) ) )*
            	    loop14:
            	    do {
            	        int alt14=2;
            	        int LA14_0 = input.LA(1);

            	        if ( (LA14_0==17) ) {
            	            alt14=1;
            	        }


            	        switch (alt14) {
            	    	case 1 :
            	    	    // Testmodel.g:1704:5: (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) )
            	    	    {
            	    	    // Testmodel.g:1704:5: (a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter ) )
            	    	    // Testmodel.g:1705:6: a10= ',' (a11_0= parse_org_eclipse_emf_ecore_EParameter )
            	    	    {
            	    	    a10=(Token)match(input,17,FOLLOW_17_in_parse_org_eclipse_emf_ecore_EOperation1420); if (state.failed) return element;

            	    	    if ( state.backtracking==0 ) {
            	    	    						if (element == null) {
            	    	    							element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            	    	    							startIncompleteElement(element);
            	    	    						}
            	    	    						collectHiddenTokens(element);
            	    	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_5_0_0_1_0_0_0, null, true);
            	    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	    	    					}

            	    	    if ( state.backtracking==0 ) {
            	    	    						// expected elements (follow set)
            	    	    						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[83]);
            	    	    						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[84]);
            	    	    					}

            	    	    // Testmodel.g:1720:6: (a11_0= parse_org_eclipse_emf_ecore_EParameter )
            	    	    // Testmodel.g:1721:7: a11_0= parse_org_eclipse_emf_ecore_EParameter
            	    	    {
            	    	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EParameter_in_parse_org_eclipse_emf_ecore_EOperation1454);
            	    	    a11_0=parse_org_eclipse_emf_ecore_EParameter();

            	    	    state._fsp--;
            	    	    if (state.failed) return element;

            	    	    if ( state.backtracking==0 ) {
            	    	    							if (terminateParsing) {
            	    	    								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    	    							}
            	    	    							if (element == null) {
            	    	    								element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            	    	    								startIncompleteElement(element);
            	    	    							}
            	    	    							if (a11_0 != null) {
            	    	    								if (a11_0 != null) {
            	    	    									Object value = a11_0;
            	    	    									addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EOPERATION__EPARAMETERS, value);
            	    	    									completedElement(value, true);
            	    	    								}
            	    	    								collectHiddenTokens(element);
            	    	    								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_5_0_0_1_0_0_1, a11_0, true);
            	    	    								copyLocalizationInfos(a11_0, element);
            	    	    							}
            	    	    						}

            	    	    }


            	    	    if ( state.backtracking==0 ) {
            	    	    						// expected elements (follow set)
            	    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[85]);
            	    	    						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[86]);
            	    	    						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[87]);
            	    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[88]);
            	    	    					}

            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop14;
            	        }
            	    } while (true);


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[89]);
            	    				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[90]);
            	    				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[91]);
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[92]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[93]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[94]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[95]);
            	}

            a12=(Token)match(input,15,FOLLOW_15_in_parse_org_eclipse_emf_ecore_EOperation1528); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a12, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[96]);
            	}

            a13=(Token)match(input,21,FOLLOW_21_in_parse_org_eclipse_emf_ecore_EOperation1542); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_3_0_0_7, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a13, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[97]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[98]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[99]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_org_eclipse_emf_ecore_EOperation_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_emf_ecore_EOperation"



    // $ANTLR start "parse_org_eclipse_emf_ecore_EParameter"
    // Testmodel.g:1800:1: parse_org_eclipse_emf_ecore_EParameter returns [org.eclipse.emf.ecore.EParameter element = null] : ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) ;
    public final org.eclipse.emf.ecore.EParameter parse_org_eclipse_emf_ecore_EParameter() throws RecognitionException {
        org.eclipse.emf.ecore.EParameter element =  null;

        int parse_org_eclipse_emf_ecore_EParameter_StartIndex = input.index();

        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        org.eclipse.emf.ecore.EAnnotation a0_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Testmodel.g:1803:2: ( ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER ) )
            // Testmodel.g:1804:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )* (a1= IDENTIFIER ) ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )? (a7= IDENTIFIER )
            {
            // Testmodel.g:1804:2: ( (a0_0= parse_org_eclipse_emf_ecore_EAnnotation ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==ANNOTATION) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // Testmodel.g:1805:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    {
            	    // Testmodel.g:1805:3: (a0_0= parse_org_eclipse_emf_ecore_EAnnotation )
            	    // Testmodel.g:1806:4: a0_0= parse_org_eclipse_emf_ecore_EAnnotation
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EParameter1580);
            	    a0_0=parse_org_eclipse_emf_ecore_EAnnotation();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a0_0 != null) {
            	    					if (a0_0 != null) {
            	    						Object value = a0_0;
            	    						addObjectToList(element, org.eclipse.emf.ecore.EcorePackage.EPARAMETER__EANNOTATIONS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_0, a0_0, true);
            	    					copyLocalizationInfos(a0_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEParameter(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[100]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[101]);
            	}

            // Testmodel.g:1833:2: (a1= IDENTIFIER )
            // Testmodel.g:1834:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EParameter1610); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<org.eclipse.emf.ecore.ETypedElement, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getETypedElementETypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__ETYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[102]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[103]);
            	}

            // Testmodel.g:1874:2: ( (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==23) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // Testmodel.g:1875:3: (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' )
                    {
                    // Testmodel.g:1875:3: (a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']' )
                    // Testmodel.g:1876:4: a2= '[' (a3= NUMBER ) a4= '..' (a5= NUMBER ) a6= ']'
                    {
                    a2=(Token)match(input,23,FOLLOW_23_in_parse_org_eclipse_emf_ecore_EParameter1640); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[104]);
                    			}

                    // Testmodel.g:1890:4: (a3= NUMBER )
                    // Testmodel.g:1891:5: a3= NUMBER
                    {
                    a3=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EParameter1666); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
                    						startIncompleteElement(element);
                    					}
                    					if (a3 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__LOWER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__LOWER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_1, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[105]);
                    			}

                    a4=(Token)match(input,19,FOLLOW_19_in_parse_org_eclipse_emf_ecore_EParameter1699); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_2, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[106]);
                    			}

                    // Testmodel.g:1940:4: (a5= NUMBER )
                    // Testmodel.g:1941:5: a5= NUMBER
                    {
                    a5=(Token)match(input,NUMBER,FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EParameter1725); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
                    						startIncompleteElement(element);
                    					}
                    					if (a5 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("NUMBER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UPPER_BOUND), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
                    						}
                    						java.lang.Integer resolved = (java.lang.Integer) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__UPPER_BOUND), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_3, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[107]);
                    			}

                    a6=(Token)match(input,24,FOLLOW_24_in_parse_org_eclipse_emf_ecore_EParameter1758); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_2_0_0_4, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[108]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[109]);
            	}

            // Testmodel.g:1997:2: (a7= IDENTIFIER )
            // Testmodel.g:1998:3: a7= IDENTIFIER
            {
            a7=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EParameter1795); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEParameter();
            				startIncompleteElement(element);
            			}
            			if (a7 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EPARAMETER__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_4_0_0_3, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[110]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[111]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEOperation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[112]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[113]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_org_eclipse_emf_ecore_EParameter_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_emf_ecore_EParameter"



    // $ANTLR start "parse_org_eclipse_emf_ecore_EAnnotation"
    // Testmodel.g:2038:1: parse_org_eclipse_emf_ecore_EAnnotation returns [org.eclipse.emf.ecore.EAnnotation element = null] : (a0= ANNOTATION ) ( (a1= '(' (a2_0= parse_java_util_Map_Entry ) ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )* a5= ')' ) )? ;
    public final org.eclipse.emf.ecore.EAnnotation parse_org_eclipse_emf_ecore_EAnnotation() throws RecognitionException {
        org.eclipse.emf.ecore.EAnnotation element =  null;

        int parse_org_eclipse_emf_ecore_EAnnotation_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a3=null;
        Token a5=null;
        edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject a2_0 =null;

        edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject a4_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Testmodel.g:2041:2: ( (a0= ANNOTATION ) ( (a1= '(' (a2_0= parse_java_util_Map_Entry ) ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )* a5= ')' ) )? )
            // Testmodel.g:2042:2: (a0= ANNOTATION ) ( (a1= '(' (a2_0= parse_java_util_Map_Entry ) ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )* a5= ')' ) )?
            {
            // Testmodel.g:2042:2: (a0= ANNOTATION )
            // Testmodel.g:2043:3: a0= ANNOTATION
            {
            a0=(Token)match(input,ANNOTATION,FOLLOW_ANNOTATION_in_parse_org_eclipse_emf_ecore_EAnnotation1835); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("ANNOTATION");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__SOURCE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__SOURCE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[114]);
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[115]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[116]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[117]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[118]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[119]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[120]);
            	}

            // Testmodel.g:2084:2: ( (a1= '(' (a2_0= parse_java_util_Map_Entry ) ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )* a5= ')' ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==14) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // Testmodel.g:2085:3: (a1= '(' (a2_0= parse_java_util_Map_Entry ) ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )* a5= ')' )
                    {
                    // Testmodel.g:2085:3: (a1= '(' (a2_0= parse_java_util_Map_Entry ) ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )* a5= ')' )
                    // Testmodel.g:2086:4: a1= '(' (a2_0= parse_java_util_Map_Entry ) ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )* a5= ')'
                    {
                    a1=(Token)match(input,14,FOLLOW_14_in_parse_org_eclipse_emf_ecore_EAnnotation1865); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAnnotation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[121]);
                    			}

                    // Testmodel.g:2100:4: (a2_0= parse_java_util_Map_Entry )
                    // Testmodel.g:2101:5: a2_0= parse_java_util_Map_Entry
                    {
                    pushFollow(FOLLOW_parse_java_util_Map_Entry_in_parse_org_eclipse_emf_ecore_EAnnotation1891);
                    a2_0=parse_java_util_Map_Entry();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
                    						startIncompleteElement(element);
                    					}
                    					if (a2_0 != null) {
                    						if (a2_0 != null) {
                    							Object value = a2_0;
                    							addMapEntry(element, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__DETAILS), a2_0);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_1, a2_0, true);
                    						copyLocalizationInfos(a2_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[122]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[123]);
                    			}

                    // Testmodel.g:2127:4: ( (a3= ',' (a4_0= parse_java_util_Map_Entry ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==17) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // Testmodel.g:2128:5: (a3= ',' (a4_0= parse_java_util_Map_Entry ) )
                    	    {
                    	    // Testmodel.g:2128:5: (a3= ',' (a4_0= parse_java_util_Map_Entry ) )
                    	    // Testmodel.g:2129:6: a3= ',' (a4_0= parse_java_util_Map_Entry )
                    	    {
                    	    a3=(Token)match(input,17,FOLLOW_17_in_parse_org_eclipse_emf_ecore_EAnnotation1932); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (element == null) {
                    	    							element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
                    	    							startIncompleteElement(element);
                    	    						}
                    	    						collectHiddenTokens(element);
                    	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_2_0_0_0, null, true);
                    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
                    	    					}

                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEAnnotation(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[124]);
                    	    					}

                    	    // Testmodel.g:2143:6: (a4_0= parse_java_util_Map_Entry )
                    	    // Testmodel.g:2144:7: a4_0= parse_java_util_Map_Entry
                    	    {
                    	    pushFollow(FOLLOW_parse_java_util_Map_Entry_in_parse_org_eclipse_emf_ecore_EAnnotation1966);
                    	    a4_0=parse_java_util_Map_Entry();

                    	    state._fsp--;
                    	    if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a4_0 != null) {
                    	    								if (a4_0 != null) {
                    	    									Object value = a4_0;
                    	    									addMapEntry(element, element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.EANNOTATION__DETAILS), a4_0);
                    	    									completedElement(value, true);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_2_0_0_1, a4_0, true);
                    	    								copyLocalizationInfos(a4_0, element);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[125]);
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[126]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[127]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[128]);
                    			}

                    a5=(Token)match(input,15,FOLLOW_15_in_parse_org_eclipse_emf_ecore_EAnnotation2027); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_5_0_0_1_0_0_3, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[129]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[130]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[131]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[132]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[133]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[134]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(org.eclipse.emf.ecore.EcorePackage.eINSTANCE.getEClass(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[135]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[136]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[137]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[138]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[139]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[140]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_org_eclipse_emf_ecore_EAnnotation_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_emf_ecore_EAnnotation"



    // $ANTLR start "parse_java_util_Map_Entry"
    // Testmodel.g:2211:1: parse_java_util_Map_Entry returns [edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject element = null] : (a0= QUOTED_34_34_92 ) a1= '=' (a2= QUOTED_34_34_92 ) ;
    public final edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject parse_java_util_Map_Entry() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject element =  null;

        int parse_java_util_Map_Entry_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Testmodel.g:2214:2: ( (a0= QUOTED_34_34_92 ) a1= '=' (a2= QUOTED_34_34_92 ) )
            // Testmodel.g:2215:2: (a0= QUOTED_34_34_92 ) a1= '=' (a2= QUOTED_34_34_92 )
            {
            // Testmodel.g:2215:2: (a0= QUOTED_34_34_92 )
            // Testmodel.g:2216:3: a0= QUOTED_34_34_92
            {
            a0=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_java_util_Map_Entry2079); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject(org.eclipse.emf.ecore.impl.EcorePackageImpl.eINSTANCE.getEStringToStringMapEntry(),"EStringToStringMapEntry");
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__KEY), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__KEY), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_6_0_0_0, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[141]);
            	}

            a1=(Token)match(input,22,FOLLOW_22_in_parse_java_util_Map_Entry2100); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject(org.eclipse.emf.ecore.impl.EcorePackageImpl.eINSTANCE.getEStringToStringMapEntry(),"EStringToStringMapEntry");
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_6_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[142]);
            	}

            // Testmodel.g:2265:2: (a2= QUOTED_34_34_92 )
            // Testmodel.g:2266:3: a2= QUOTED_34_34_92
            {
            a2=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_java_util_Map_Entry2118); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelDummyEObject(org.eclipse.emf.ecore.impl.EcorePackageImpl.eINSTANCE.getEStringToStringMapEntry(),"EStringToStringMapEntry");
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__VALUE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(org.eclipse.emf.ecore.EcorePackage.ESTRING_TO_STRING_MAP_ENTRY__VALUE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_6_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[143]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[144]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_java_util_Map_Entry_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_java_util_Map_Entry"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_TestModel"
    // Testmodel.g:2304:1: parse_edu_ustb_sei_mde_testing_testdefinition_TestModel returns [edu.ustb.sei.mde.testing.testdefinition.TestModel element = null] : a0= 'model' (a1= IDENTIFIER ) ( (a2_0= parse_org_eclipse_emf_ecore_EClass ) )* ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario ) )* ;
    public final edu.ustb.sei.mde.testing.testdefinition.TestModel parse_edu_ustb_sei_mde_testing_testdefinition_TestModel() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.TestModel element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_TestModel_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        org.eclipse.emf.ecore.EClass a2_0 =null;

        edu.ustb.sei.mde.testing.testdefinition.TestScenario a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Testmodel.g:2307:2: (a0= 'model' (a1= IDENTIFIER ) ( (a2_0= parse_org_eclipse_emf_ecore_EClass ) )* ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario ) )* )
            // Testmodel.g:2308:2: a0= 'model' (a1= IDENTIFIER ) ( (a2_0= parse_org_eclipse_emf_ecore_EClass ) )* ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario ) )*
            {
            a0=(Token)match(input,32,FOLLOW_32_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2154); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[145]);
            	}

            // Testmodel.g:2322:2: (a1= IDENTIFIER )
            // Testmodel.g:2323:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2172); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[146]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[147]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[148]);
            	}

            // Testmodel.g:2360:2: ( (a2_0= parse_org_eclipse_emf_ecore_EClass ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==ANNOTATION||LA20_0==31||LA20_0==35) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // Testmodel.g:2361:3: (a2_0= parse_org_eclipse_emf_ecore_EClass )
            	    {
            	    // Testmodel.g:2361:3: (a2_0= parse_org_eclipse_emf_ecore_EClass )
            	    // Testmodel.g:2362:4: a2_0= parse_org_eclipse_emf_ecore_EClass
            	    {
            	    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EClass_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2202);
            	    a2_0=parse_org_eclipse_emf_ecore_EClass();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a2_0 != null) {
            	    					if (a2_0 != null) {
            	    						Object value = a2_0;
            	    						addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__CLASSIFIERS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_2, a2_0, true);
            	    					copyLocalizationInfos(a2_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[149]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[150]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[151]);
            	}

            // Testmodel.g:2390:2: ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==34) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // Testmodel.g:2391:3: (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario )
            	    {
            	    // Testmodel.g:2391:3: (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario )
            	    // Testmodel.g:2392:4: a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario
            	    {
            	    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2237);
            	    a3_0=parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a3_0 != null) {
            	    					if (a3_0 != null) {
            	    						Object value = a3_0;
            	    						addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_MODEL__SCENARIOS, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_7_0_0_3, a3_0, true);
            	    					copyLocalizationInfos(a3_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[152]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_edu_ustb_sei_mde_testing_testdefinition_TestModel_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_TestModel"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario"
    // Testmodel.g:2420:1: parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario returns [edu.ustb.sei.mde.testing.testdefinition.TestScenario element = null] : a0= 'test' (a1= IDENTIFIER ) a2= '{' ( ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a4= ';' ) )* ( (a5= 'do' (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )? (a7_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle ) a8= '}' ;
    public final edu.ustb.sei.mde.testing.testdefinition.TestScenario parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.TestScenario element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        Token a5=null;
        Token a8=null;
        edu.ustb.sei.mde.testing.testdefinition.Variable a3_0 =null;

        edu.ustb.sei.mde.testing.testdefinition.Script a6_0 =null;

        edu.ustb.sei.mde.testing.testdefinition.Oracle a7_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return element; }

            // Testmodel.g:2423:2: (a0= 'test' (a1= IDENTIFIER ) a2= '{' ( ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a4= ';' ) )* ( (a5= 'do' (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )? (a7_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle ) a8= '}' )
            // Testmodel.g:2424:2: a0= 'test' (a1= IDENTIFIER ) a2= '{' ( ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a4= ';' ) )* ( (a5= 'do' (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )? (a7_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle ) a8= '}'
            {
            a0=(Token)match(input,34,FOLLOW_34_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2278); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[153]);
            	}

            // Testmodel.g:2438:2: (a1= IDENTIFIER )
            // Testmodel.g:2439:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2296); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[154]);
            	}

            a2=(Token)match(input,37,FOLLOW_37_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2317); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[155]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[156]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[157]);
            	}

            // Testmodel.g:2490:2: ( ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a4= ';' ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==36) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // Testmodel.g:2491:3: ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a4= ';' )
            	    {
            	    // Testmodel.g:2491:3: ( (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a4= ';' )
            	    // Testmodel.g:2492:4: (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a4= ';'
            	    {
            	    // Testmodel.g:2492:4: (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable )
            	    // Testmodel.g:2493:5: a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable
            	    {
            	    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Variable_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2346);
            	    a3_0=parse_edu_ustb_sei_mde_testing_testdefinition_Variable();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__VARIABLES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_4_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[158]);
            	    			}

            	    a4=(Token)match(input,21,FOLLOW_21_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2374); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_4_0_0_1, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[159]);
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[160]);
            	    				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[161]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[162]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[163]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[164]);
            	}

            // Testmodel.g:2543:2: ( (a5= 'do' (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==27) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // Testmodel.g:2544:3: (a5= 'do' (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) )
                    {
                    // Testmodel.g:2544:3: (a5= 'do' (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) )
                    // Testmodel.g:2545:4: a5= 'do' (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script )
                    {
                    a5=(Token)match(input,27,FOLLOW_27_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2416); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_5_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[165]);
                    				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[166]);
                    				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[167]);
                    				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[168]);
                    			}

                    // Testmodel.g:2562:4: (a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script )
                    // Testmodel.g:2563:5: a6_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Script_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2442);
                    a6_0=parse_edu_ustb_sei_mde_testing_testdefinition_Script();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
                    						startIncompleteElement(element);
                    					}
                    					if (a6_0 != null) {
                    						if (a6_0 != null) {
                    							Object value = a6_0;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__DO), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_5_0_0_1, a6_0, true);
                    						copyLocalizationInfos(a6_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[169]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[170]);
            	}

            // Testmodel.g:2595:2: (a7_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle )
            // Testmodel.g:2596:3: a7_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle
            {
            pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2487);
            a7_0=parse_edu_ustb_sei_mde_testing_testdefinition_Oracle();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
            				startIncompleteElement(element);
            			}
            			if (a7_0 != null) {
            				if (a7_0 != null) {
            					Object value = a7_0;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.TEST_SCENARIO__ASSERTION), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_6, a7_0, true);
            				copyLocalizationInfos(a7_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[171]);
            	}

            a8=(Token)match(input,38,FOLLOW_38_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2505); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_8_0_0_8, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[172]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_Script"
    // Testmodel.g:2637:1: parse_edu_ustb_sei_mde_testing_testdefinition_Script returns [edu.ustb.sei.mde.testing.testdefinition.Script element = null] : ( ( ( (a0= IDENTIFIER ) a1= ':' ) )? (a2= QUOTED_6037_3762_92 ) |c0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification |c1= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle );
    public final edu.ustb.sei.mde.testing.testdefinition.Script parse_edu_ustb_sei_mde_testing_testdefinition_Script() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.Script element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_Script_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification c0 =null;

        edu.ustb.sei.mde.testing.testdefinition.Oracle c1 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return element; }

            // Testmodel.g:2640:2: ( ( ( (a0= IDENTIFIER ) a1= ':' ) )? (a2= QUOTED_6037_3762_92 ) |c0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification |c1= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle )
            int alt25=3;
            switch ( input.LA(1) ) {
            case IDENTIFIER:
            case QUOTED_6037_3762_92:
                {
                alt25=1;
                }
                break;
            case 22:
                {
                alt25=2;
                }
                break;
            case 25:
                {
                alt25=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;

            }

            switch (alt25) {
                case 1 :
                    // Testmodel.g:2641:2: ( ( (a0= IDENTIFIER ) a1= ':' ) )? (a2= QUOTED_6037_3762_92 )
                    {
                    // Testmodel.g:2641:2: ( ( (a0= IDENTIFIER ) a1= ':' ) )?
                    int alt24=2;
                    int LA24_0 = input.LA(1);

                    if ( (LA24_0==IDENTIFIER) ) {
                        alt24=1;
                    }
                    switch (alt24) {
                        case 1 :
                            // Testmodel.g:2642:3: ( (a0= IDENTIFIER ) a1= ':' )
                            {
                            // Testmodel.g:2642:3: ( (a0= IDENTIFIER ) a1= ':' )
                            // Testmodel.g:2643:4: (a0= IDENTIFIER ) a1= ':'
                            {
                            // Testmodel.g:2643:4: (a0= IDENTIFIER )
                            // Testmodel.g:2644:5: a0= IDENTIFIER
                            {
                            a0=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2549); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            					if (terminateParsing) {
                            						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                            					}
                            					if (element == null) {
                            						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createScript();
                            						startIncompleteElement(element);
                            					}
                            					if (a0 != null) {
                            						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                            						tokenResolver.setOptions(getOptions());
                            						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                            						tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__LANGUAGE), result);
                            						Object resolvedObject = result.getResolvedToken();
                            						if (resolvedObject == null) {
                            							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a0).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a0).getStopIndex());
                            						}
                            						java.lang.String resolved = (java.lang.String) resolvedObject;
                            						if (resolved != null) {
                            							Object value = resolved;
                            							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__LANGUAGE), value);
                            							completedElement(value, false);
                            						}
                            						collectHiddenTokens(element);
                            						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_9_0_0_0_0_0_0, resolved, true);
                            						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a0, element);
                            					}
                            				}

                            }


                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[173]);
                            			}

                            a1=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2582); if (state.failed) return element;

                            if ( state.backtracking==0 ) {
                            				if (element == null) {
                            					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createScript();
                            					startIncompleteElement(element);
                            				}
                            				collectHiddenTokens(element);
                            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_9_0_0_0_0_0_1, null, true);
                            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
                            			}

                            if ( state.backtracking==0 ) {
                            				// expected elements (follow set)
                            				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[174]);
                            			}

                            }


                            }
                            break;

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[175]);
                    	}

                    // Testmodel.g:2700:2: (a2= QUOTED_6037_3762_92 )
                    // Testmodel.g:2701:3: a2= QUOTED_6037_3762_92
                    {
                    a2=(Token)match(input,QUOTED_6037_3762_92,FOLLOW_QUOTED_6037_3762_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2619); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    			if (terminateParsing) {
                    				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    			}
                    			if (element == null) {
                    				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createScript();
                    				startIncompleteElement(element);
                    			}
                    			if (a2 != null) {
                    				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
                    				tokenResolver.setOptions(getOptions());
                    				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__SCRIPT), result);
                    				Object resolvedObject = result.getResolvedToken();
                    				if (resolvedObject == null) {
                    					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
                    				}
                    				java.lang.String resolved = (java.lang.String) resolvedObject;
                    				if (resolved != null) {
                    					Object value = resolved;
                    					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.SCRIPT__SCRIPT), value);
                    					completedElement(value, false);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_9_0_0_1, resolved, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
                    			}
                    		}

                    }


                    if ( state.backtracking==0 ) {
                    		// expected elements (follow set)
                    		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[176]);
                    	}

                    }
                    break;
                case 2 :
                    // Testmodel.g:2738:2: c0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2645);
                    c0=parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Testmodel.g:2739:4: c1= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2655);
                    c1=parse_edu_ustb_sei_mde_testing_testdefinition_Oracle();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, parse_edu_ustb_sei_mde_testing_testdefinition_Script_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_Script"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_Variable"
    // Testmodel.g:2743:1: parse_edu_ustb_sei_mde_testing_testdefinition_Variable returns [edu.ustb.sei.mde.testing.testdefinition.Variable element = null] : a0= 'var' (a1= IDENTIFIER ) (a2= IDENTIFIER ) (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification ) ;
    public final edu.ustb.sei.mde.testing.testdefinition.Variable parse_edu_ustb_sei_mde_testing_testdefinition_Variable() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.Variable element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_Variable_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        edu.ustb.sei.mde.testing.testdefinition.ValueSpecification a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return element; }

            // Testmodel.g:2746:2: (a0= 'var' (a1= IDENTIFIER ) (a2= IDENTIFIER ) (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification ) )
            // Testmodel.g:2747:2: a0= 'var' (a1= IDENTIFIER ) (a2= IDENTIFIER ) (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification )
            {
            a0=(Token)match(input,36,FOLLOW_36_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2680); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[177]);
            	}

            // Testmodel.g:2761:2: (a1= IDENTIFIER )
            // Testmodel.g:2762:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2698); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EClassifier proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.Variable, org.eclipse.emf.ecore.EClassifier>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getVariableTypeReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__TYPE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[178]);
            	}

            // Testmodel.g:2801:2: (a2= IDENTIFIER )
            // Testmodel.g:2802:3: a2= IDENTIFIER
            {
            a2=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2723); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[179]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[180]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[181]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[182]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getVariable(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[183]);
            	}

            // Testmodel.g:2841:2: (a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification )
            // Testmodel.g:2842:3: a3_0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification
            {
            pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2748);
            a3_0=parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
            				startIncompleteElement(element);
            			}
            			if (a3_0 != null) {
            				if (a3_0 != null) {
            					Object value = a3_0;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VARIABLE__VALUE), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_10_0_0_3, a3_0, true);
            				copyLocalizationInfos(a3_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[184]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[185]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, parse_edu_ustb_sei_mde_testing_testdefinition_Variable_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_Variable"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification"
    // Testmodel.g:2870:1: parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification element = null] : a0= 'in' a1= '[' (a2= QUOTED_34_34_92 ) a3= '..' (a4= QUOTED_34_34_92 ) a5= ']' ;
    public final edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return element; }

            // Testmodel.g:2873:2: (a0= 'in' a1= '[' (a2= QUOTED_34_34_92 ) a3= '..' (a4= QUOTED_34_34_92 ) a5= ']' )
            // Testmodel.g:2874:2: a0= 'in' a1= '[' (a2= QUOTED_34_34_92 ) a3= '..' (a4= QUOTED_34_34_92 ) a5= ']'
            {
            a0=(Token)match(input,30,FOLLOW_30_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2781); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[186]);
            	}

            a1=(Token)match(input,23,FOLLOW_23_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2795); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[187]);
            	}

            // Testmodel.g:2902:2: (a2= QUOTED_34_34_92 )
            // Testmodel.g:2903:3: a2= QUOTED_34_34_92
            {
            a2=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2813); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MIN_LITERAL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[188]);
            	}

            a3=(Token)match(input,19,FOLLOW_19_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2834); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_3, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[189]);
            	}

            // Testmodel.g:2952:2: (a4= QUOTED_34_34_92 )
            // Testmodel.g:2953:3: a4= QUOTED_34_34_92
            {
            a4=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2852); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
            				startIncompleteElement(element);
            			}
            			if (a4 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_RANGE_SPECIFICATION__MAX_LITERAL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_4, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[190]);
            	}

            a5=(Token)match(input,24,FOLLOW_24_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2873); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueRangeSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_11_0_0_5, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[191]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[192]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification"
    // Testmodel.g:3005:1: parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification element = null] : a0= 'in' a1= '{' (a2= QUOTED_34_34_92 ) ( (a3= ',' (a4= QUOTED_34_34_92 ) ) )* a5= '}' ;
    public final edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return element; }

            // Testmodel.g:3008:2: (a0= 'in' a1= '{' (a2= QUOTED_34_34_92 ) ( (a3= ',' (a4= QUOTED_34_34_92 ) ) )* a5= '}' )
            // Testmodel.g:3009:2: a0= 'in' a1= '{' (a2= QUOTED_34_34_92 ) ( (a3= ',' (a4= QUOTED_34_34_92 ) ) )* a5= '}'
            {
            a0=(Token)match(input,30,FOLLOW_30_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2902); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[193]);
            	}

            a1=(Token)match(input,37,FOLLOW_37_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2916); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_1, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a1, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[194]);
            	}

            // Testmodel.g:3037:2: (a2= QUOTED_34_34_92 )
            // Testmodel.g:3038:3: a2= QUOTED_34_34_92
            {
            a2=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2934); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
            				startIncompleteElement(element);
            			}
            			if (a2 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a2.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a2).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a2).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS, value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a2, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[195]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[196]);
            	}

            // Testmodel.g:3074:2: ( (a3= ',' (a4= QUOTED_34_34_92 ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==17) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // Testmodel.g:3075:3: (a3= ',' (a4= QUOTED_34_34_92 ) )
            	    {
            	    // Testmodel.g:3075:3: (a3= ',' (a4= QUOTED_34_34_92 ) )
            	    // Testmodel.g:3076:4: a3= ',' (a4= QUOTED_34_34_92 )
            	    {
            	    a3=(Token)match(input,17,FOLLOW_17_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2964); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_3_0_0_0, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a3, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[197]);
            	    			}

            	    // Testmodel.g:3090:4: (a4= QUOTED_34_34_92 )
            	    // Testmodel.g:3091:5: a4= QUOTED_34_34_92
            	    {
            	    a4=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2990); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a4 != null) {
            	    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
            	    						tokenResolver.setOptions(getOptions());
            	    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            	    						tokenResolver.resolve(a4.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS), result);
            	    						Object resolvedObject = result.getResolvedToken();
            	    						if (resolvedObject == null) {
            	    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a4).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a4).getStopIndex());
            	    						}
            	    						java.lang.String resolved = (java.lang.String) resolvedObject;
            	    						if (resolved != null) {
            	    							Object value = resolved;
            	    							addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SET_SPECIFICATION__LITERALS, value);
            	    							completedElement(value, false);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_3_0_0_1, resolved, true);
            	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a4, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[198]);
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[199]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[200]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[201]);
            	}

            a5=(Token)match(input,38,FOLLOW_38_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification3036); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueSetSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_12_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a5, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[202]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[203]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification"
    // Testmodel.g:3152:1: parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification element = null] : a0= '=' ( ( (a1= IDENTIFIER ) a2= ':' ) )? (a3= QUOTED_6037_3762_92 ) ;
    public final edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return element; }

            // Testmodel.g:3155:2: (a0= '=' ( ( (a1= IDENTIFIER ) a2= ':' ) )? (a3= QUOTED_6037_3762_92 ) )
            // Testmodel.g:3156:2: a0= '=' ( ( (a1= IDENTIFIER ) a2= ':' ) )? (a3= QUOTED_6037_3762_92 )
            {
            a0=(Token)match(input,22,FOLLOW_22_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3065); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[204]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[205]);
            	}

            // Testmodel.g:3171:2: ( ( (a1= IDENTIFIER ) a2= ':' ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==IDENTIFIER) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // Testmodel.g:3172:3: ( (a1= IDENTIFIER ) a2= ':' )
                    {
                    // Testmodel.g:3172:3: ( (a1= IDENTIFIER ) a2= ':' )
                    // Testmodel.g:3173:4: (a1= IDENTIFIER ) a2= ':'
                    {
                    // Testmodel.g:3173:4: (a1= IDENTIFIER )
                    // Testmodel.g:3174:5: a1= IDENTIFIER
                    {
                    a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3094); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
                    						startIncompleteElement(element);
                    					}
                    					if (a1 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__LANGUAGE), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
                    						}
                    						java.lang.String resolved = (java.lang.String) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__LANGUAGE), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_1_0_0_0, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[206]);
                    			}

                    a2=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3127); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_1_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[207]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[208]);
            	}

            // Testmodel.g:3230:2: (a3= QUOTED_6037_3762_92 )
            // Testmodel.g:3231:3: a3= QUOTED_6037_3762_92
            {
            a3=(Token)match(input,QUOTED_6037_3762_92,FOLLOW_QUOTED_6037_3762_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3164); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createValueScriptSpecification();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__SCRIPT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.VALUE_SCRIPT_SPECIFICATION__SCRIPT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_13_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[209]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[210]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 15, parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification"
    // Testmodel.g:3269:1: parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification element = null] : a0= '=' (a1= IDENTIFIER ) a2= '.' (a3= IDENTIFIER ) a4= '(' ( ( (a5= IDENTIFIER ) ( (a6= ',' (a7= IDENTIFIER ) ) )* ) )? a8= ')' ;
    public final edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        Token a6=null;
        Token a7=null;
        Token a8=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return element; }

            // Testmodel.g:3272:2: (a0= '=' (a1= IDENTIFIER ) a2= '.' (a3= IDENTIFIER ) a4= '(' ( ( (a5= IDENTIFIER ) ( (a6= ',' (a7= IDENTIFIER ) ) )* ) )? a8= ')' )
            // Testmodel.g:3273:2: a0= '=' (a1= IDENTIFIER ) a2= '.' (a3= IDENTIFIER ) a4= '(' ( ( (a5= IDENTIFIER ) ( (a6= ',' (a7= IDENTIFIER ) ) )* ) )? a8= ')'
            {
            a0=(Token)match(input,22,FOLLOW_22_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3200); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[211]);
            	}

            // Testmodel.g:3287:2: (a1= IDENTIFIER )
            // Testmodel.g:3288:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3218); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EClass proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEClass();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EClass>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationInterfaceReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__INTERFACE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_1, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[212]);
            	}

            a2=(Token)match(input,18,FOLLOW_18_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3239); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_2, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[213]);
            	}

            // Testmodel.g:3341:2: (a3= IDENTIFIER )
            // Testmodel.g:3342:3: a3= IDENTIFIER
            {
            a3=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3257); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				String resolved = (String) resolvedObject;
            				org.eclipse.emf.ecore.EOperation proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEOperation();
            				collectHiddenTokens(element);
            				registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, org.eclipse.emf.ecore.EOperation>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationOperationReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION), resolved, proxy);
            				if (proxy != null) {
            					Object value = proxy;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__OPERATION), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_3, proxy, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[214]);
            	}

            a4=(Token)match(input,14,FOLLOW_14_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3278); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[215]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[216]);
            	}

            // Testmodel.g:3396:2: ( ( (a5= IDENTIFIER ) ( (a6= ',' (a7= IDENTIFIER ) ) )* ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==IDENTIFIER) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // Testmodel.g:3397:3: ( (a5= IDENTIFIER ) ( (a6= ',' (a7= IDENTIFIER ) ) )* )
                    {
                    // Testmodel.g:3397:3: ( (a5= IDENTIFIER ) ( (a6= ',' (a7= IDENTIFIER ) ) )* )
                    // Testmodel.g:3398:4: (a5= IDENTIFIER ) ( (a6= ',' (a7= IDENTIFIER ) ) )*
                    {
                    // Testmodel.g:3398:4: (a5= IDENTIFIER )
                    // Testmodel.g:3399:5: a5= IDENTIFIER
                    {
                    a5=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3307); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
                    						startIncompleteElement(element);
                    					}
                    					if (a5 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						edu.ustb.sei.mde.testing.testdefinition.Variable proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationActualParametersReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS, value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_5_0_0_0, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[217]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[218]);
                    			}

                    // Testmodel.g:3439:4: ( (a6= ',' (a7= IDENTIFIER ) ) )*
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==17) ) {
                            alt28=1;
                        }


                        switch (alt28) {
                    	case 1 :
                    	    // Testmodel.g:3440:5: (a6= ',' (a7= IDENTIFIER ) )
                    	    {
                    	    // Testmodel.g:3440:5: (a6= ',' (a7= IDENTIFIER ) )
                    	    // Testmodel.g:3441:6: a6= ',' (a7= IDENTIFIER )
                    	    {
                    	    a6=(Token)match(input,17,FOLLOW_17_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3353); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    						if (element == null) {
                    	    							element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
                    	    							startIncompleteElement(element);
                    	    						}
                    	    						collectHiddenTokens(element);
                    	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_5_0_0_1_0_0_0, null, true);
                    	    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
                    	    					}

                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[219]);
                    	    					}

                    	    // Testmodel.g:3455:6: (a7= IDENTIFIER )
                    	    // Testmodel.g:3456:7: a7= IDENTIFIER
                    	    {
                    	    a7=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3387); if (state.failed) return element;

                    	    if ( state.backtracking==0 ) {
                    	    							if (terminateParsing) {
                    	    								throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    	    							}
                    	    							if (element == null) {
                    	    								element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
                    	    								startIncompleteElement(element);
                    	    							}
                    	    							if (a7 != null) {
                    	    								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                    	    								tokenResolver.setOptions(getOptions());
                    	    								edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    	    								tokenResolver.resolve(a7.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), result);
                    	    								Object resolvedObject = result.getResolvedToken();
                    	    								if (resolvedObject == null) {
                    	    									addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a7).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a7).getStopIndex());
                    	    								}
                    	    								String resolved = (String) resolvedObject;
                    	    								edu.ustb.sei.mde.testing.testdefinition.Variable proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createVariable();
                    	    								collectHiddenTokens(element);
                    	    								registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification, edu.ustb.sei.mde.testing.testdefinition.Variable>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getReturnValueSpecificationActualParametersReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS), resolved, proxy);
                    	    								if (proxy != null) {
                    	    									Object value = proxy;
                    	    									addObjectToList(element, edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.RETURN_VALUE_SPECIFICATION__ACTUAL_PARAMETERS, value);
                    	    									completedElement(value, false);
                    	    								}
                    	    								collectHiddenTokens(element);
                    	    								retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_5_0_0_1_0_0_1, proxy, true);
                    	    								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, element);
                    	    								copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a7, proxy);
                    	    							}
                    	    						}

                    	    }


                    	    if ( state.backtracking==0 ) {
                    	    						// expected elements (follow set)
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[220]);
                    	    						addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[221]);
                    	    					}

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[222]);
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[223]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[224]);
            	}

            a8=(Token)match(input,15,FOLLOW_15_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3468); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createReturnValueSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_14_0_0_6, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a8, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[225]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[226]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 16, parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_Oracle"
    // Testmodel.g:3528:1: parse_edu_ustb_sei_mde_testing_testdefinition_Oracle returns [edu.ustb.sei.mde.testing.testdefinition.Oracle element = null] : a0= 'assert' ( ( (a1= IDENTIFIER ) a2= ':' ) )? (a3= QUOTED_6037_3762_92 ) ;
    public final edu.ustb.sei.mde.testing.testdefinition.Oracle parse_edu_ustb_sei_mde_testing_testdefinition_Oracle() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.Oracle element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return element; }

            // Testmodel.g:3531:2: (a0= 'assert' ( ( (a1= IDENTIFIER ) a2= ':' ) )? (a3= QUOTED_6037_3762_92 ) )
            // Testmodel.g:3532:2: a0= 'assert' ( ( (a1= IDENTIFIER ) a2= ':' ) )? (a3= QUOTED_6037_3762_92 )
            {
            a0=(Token)match(input,25,FOLLOW_25_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3497); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[227]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[228]);
            	}

            // Testmodel.g:3547:2: ( ( (a1= IDENTIFIER ) a2= ':' ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==IDENTIFIER) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // Testmodel.g:3548:3: ( (a1= IDENTIFIER ) a2= ':' )
                    {
                    // Testmodel.g:3548:3: ( (a1= IDENTIFIER ) a2= ':' )
                    // Testmodel.g:3549:4: (a1= IDENTIFIER ) a2= ':'
                    {
                    // Testmodel.g:3549:4: (a1= IDENTIFIER )
                    // Testmodel.g:3550:5: a1= IDENTIFIER
                    {
                    a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3526); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
                    						startIncompleteElement(element);
                    					}
                    					if (a1 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__LANGUAGE), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
                    						}
                    						java.lang.String resolved = (java.lang.String) resolvedObject;
                    						if (resolved != null) {
                    							Object value = resolved;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__LANGUAGE), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_1_0_0_0, resolved, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[229]);
                    			}

                    a2=(Token)match(input,20,FOLLOW_20_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3559); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_1_0_0_1, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[230]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[231]);
            	}

            // Testmodel.g:3606:2: (a3= QUOTED_6037_3762_92 )
            // Testmodel.g:3607:3: a3= QUOTED_6037_3762_92
            {
            a3=(Token)match(input,QUOTED_6037_3762_92,FOLLOW_QUOTED_6037_3762_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3596); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createOracle();
            				startIncompleteElement(element);
            			}
            			if (a3 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_6037_3762_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__SCRIPT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.ORACLE__SCRIPT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_15_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testdefinition.TestdefinitionPackage.eINSTANCE.getTestScenario(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[232]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 17, parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_Oracle"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel"
    // Testmodel.g:3644:1: parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel returns [edu.ustb.sei.mde.testing.testcase.TestCaseModel element = null] : a0= 'module' (a1= IDENTIFIER ) ( (a2= 'based on' (a3= QUOTED_34_34_92 ) ) )? ( (a4= 'import' (a5= QUOTED_34_34_92 ) ) )? ( (a6_0= parse_edu_ustb_sei_mde_testing_testcase_TestCase ) )* ;
    public final edu.ustb.sei.mde.testing.testcase.TestCaseModel parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel() throws RecognitionException {
        edu.ustb.sei.mde.testing.testcase.TestCaseModel element =  null;

        int parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a5=null;
        edu.ustb.sei.mde.testing.testcase.TestCase a6_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return element; }

            // Testmodel.g:3647:2: (a0= 'module' (a1= IDENTIFIER ) ( (a2= 'based on' (a3= QUOTED_34_34_92 ) ) )? ( (a4= 'import' (a5= QUOTED_34_34_92 ) ) )? ( (a6_0= parse_edu_ustb_sei_mde_testing_testcase_TestCase ) )* )
            // Testmodel.g:3648:2: a0= 'module' (a1= IDENTIFIER ) ( (a2= 'based on' (a3= QUOTED_34_34_92 ) ) )? ( (a4= 'import' (a5= QUOTED_34_34_92 ) ) )? ( (a6_0= parse_edu_ustb_sei_mde_testing_testcase_TestCase ) )*
            {
            a0=(Token)match(input,33,FOLLOW_33_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3632); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[233]);
            	}

            // Testmodel.g:3662:2: (a1= IDENTIFIER )
            // Testmodel.g:3663:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3650); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[234]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[235]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[236]);
            	}

            // Testmodel.g:3700:2: ( (a2= 'based on' (a3= QUOTED_34_34_92 ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==26) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // Testmodel.g:3701:3: (a2= 'based on' (a3= QUOTED_34_34_92 ) )
                    {
                    // Testmodel.g:3701:3: (a2= 'based on' (a3= QUOTED_34_34_92 ) )
                    // Testmodel.g:3702:4: a2= 'based on' (a3= QUOTED_34_34_92 )
                    {
                    a2=(Token)match(input,26,FOLLOW_26_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3680); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_2_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[237]);
                    			}

                    // Testmodel.g:3716:4: (a3= QUOTED_34_34_92 )
                    // Testmodel.g:3717:5: a3= QUOTED_34_34_92
                    {
                    a3=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3706); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
                    						startIncompleteElement(element);
                    					}
                    					if (a3 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						edu.ustb.sei.mde.testing.testdefinition.TestModel proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestModel();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testcase.TestCaseModel, edu.ustb.sei.mde.testing.testdefinition.TestModel>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseModelTestmodelReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTMODEL), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_2_0_0_2, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[238]);
                    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[239]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[240]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[241]);
            	}

            // Testmodel.g:3765:2: ( (a4= 'import' (a5= QUOTED_34_34_92 ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==29) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // Testmodel.g:3766:3: (a4= 'import' (a5= QUOTED_34_34_92 ) )
                    {
                    // Testmodel.g:3766:3: (a4= 'import' (a5= QUOTED_34_34_92 ) )
                    // Testmodel.g:3767:4: a4= 'import' (a5= QUOTED_34_34_92 )
                    {
                    a4=(Token)match(input,29,FOLLOW_29_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3761); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[242]);
                    			}

                    // Testmodel.g:3781:4: (a5= QUOTED_34_34_92 )
                    // Testmodel.g:3782:5: a5= QUOTED_34_34_92
                    {
                    a5=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3787); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
                    						startIncompleteElement(element);
                    					}
                    					if (a5 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a5.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a5).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a5).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						org.eclipse.emf.ecore.EPackage proxy = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEPackage();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testcase.TestCaseModel, org.eclipse.emf.ecore.EPackage>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseModelEPackageReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__EPACKAGE), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_3_0_0_2, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a5, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[243]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[244]);
            	}

            // Testmodel.g:3828:2: ( (a6_0= parse_edu_ustb_sei_mde_testing_testcase_TestCase ) )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==34) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // Testmodel.g:3829:3: (a6_0= parse_edu_ustb_sei_mde_testing_testcase_TestCase )
            	    {
            	    // Testmodel.g:3829:3: (a6_0= parse_edu_ustb_sei_mde_testing_testcase_TestCase )
            	    // Testmodel.g:3830:4: a6_0= parse_edu_ustb_sei_mde_testing_testcase_TestCase
            	    {
            	    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testcase_TestCase_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3842);
            	    a6_0=parse_edu_ustb_sei_mde_testing_testcase_TestCase();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (terminateParsing) {
            	    					throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    				}
            	    				if (element == null) {
            	    					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCaseModel();
            	    					startIncompleteElement(element);
            	    				}
            	    				if (a6_0 != null) {
            	    					if (a6_0 != null) {
            	    						Object value = a6_0;
            	    						addObjectToList(element, edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE_MODEL__TESTCASES, value);
            	    						completedElement(value, true);
            	    					}
            	    					collectHiddenTokens(element);
            	    					retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_16_0_0_5, a6_0, true);
            	    					copyLocalizationInfos(a6_0, element);
            	    				}
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[245]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 18, parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testcase_TestCase"
    // Testmodel.g:3858:1: parse_edu_ustb_sei_mde_testing_testcase_TestCase returns [edu.ustb.sei.mde.testing.testcase.TestCase element = null] : a0= 'test' (a1= IDENTIFIER ) ( (a2= 'based on' (a3= IDENTIFIER ) ) )? a4= '{' ( ( (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a6= ';' ) )* ( (a7= 'do' (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )? (a9_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle ) a10= '}' ;
    public final edu.ustb.sei.mde.testing.testcase.TestCase parse_edu_ustb_sei_mde_testing_testcase_TestCase() throws RecognitionException {
        edu.ustb.sei.mde.testing.testcase.TestCase element =  null;

        int parse_edu_ustb_sei_mde_testing_testcase_TestCase_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a3=null;
        Token a4=null;
        Token a6=null;
        Token a7=null;
        Token a10=null;
        edu.ustb.sei.mde.testing.testdefinition.Variable a5_0 =null;

        edu.ustb.sei.mde.testing.testdefinition.Script a8_0 =null;

        edu.ustb.sei.mde.testing.testdefinition.Oracle a9_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return element; }

            // Testmodel.g:3861:2: (a0= 'test' (a1= IDENTIFIER ) ( (a2= 'based on' (a3= IDENTIFIER ) ) )? a4= '{' ( ( (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a6= ';' ) )* ( (a7= 'do' (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )? (a9_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle ) a10= '}' )
            // Testmodel.g:3862:2: a0= 'test' (a1= IDENTIFIER ) ( (a2= 'based on' (a3= IDENTIFIER ) ) )? a4= '{' ( ( (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a6= ';' ) )* ( (a7= 'do' (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )? (a9_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle ) a10= '}'
            {
            a0=(Token)match(input,34,FOLLOW_34_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3883); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[246]);
            	}

            // Testmodel.g:3876:2: (a1= IDENTIFIER )
            // Testmodel.g:3877:3: a1= IDENTIFIER
            {
            a1=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3901); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__NAME), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__NAME), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_2, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[247]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[248]);
            	}

            // Testmodel.g:3913:2: ( (a2= 'based on' (a3= IDENTIFIER ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==26) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // Testmodel.g:3914:3: (a2= 'based on' (a3= IDENTIFIER ) )
                    {
                    // Testmodel.g:3914:3: (a2= 'based on' (a3= IDENTIFIER ) )
                    // Testmodel.g:3915:4: a2= 'based on' (a3= IDENTIFIER )
                    {
                    a2=(Token)match(input,26,FOLLOW_26_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3931); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_3_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a2, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[249]);
                    			}

                    // Testmodel.g:3929:4: (a3= IDENTIFIER )
                    // Testmodel.g:3930:5: a3= IDENTIFIER
                    {
                    a3=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3957); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
                    						startIncompleteElement(element);
                    					}
                    					if (a3 != null) {
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("IDENTIFIER");
                    						tokenResolver.setOptions(getOptions());
                    						edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
                    						tokenResolver.resolve(a3.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO), result);
                    						Object resolvedObject = result.getResolvedToken();
                    						if (resolvedObject == null) {
                    							addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a3).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a3).getStopIndex());
                    						}
                    						String resolved = (String) resolvedObject;
                    						edu.ustb.sei.mde.testing.testdefinition.TestScenario proxy = edu.ustb.sei.mde.testing.testdefinition.TestdefinitionFactory.eINSTANCE.createTestScenario();
                    						collectHiddenTokens(element);
                    						registerContextDependentProxy(new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelContextDependentURIFragmentFactory<edu.ustb.sei.mde.testing.testcase.TestCase, edu.ustb.sei.mde.testing.testdefinition.TestScenario>(getReferenceResolverSwitch() == null ? null : getReferenceResolverSwitch().getTestCaseScenarioReferenceResolver()), element, (org.eclipse.emf.ecore.EReference) element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO), resolved, proxy);
                    						if (proxy != null) {
                    							Object value = proxy;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__SCENARIO), value);
                    							completedElement(value, false);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_3_0_0_2, proxy, true);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, element);
                    						copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a3, proxy);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[250]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[251]);
            	}

            a4=(Token)match(input,37,FOLLOW_37_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4003); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_4, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[252]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[253]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[254]);
            	}

            // Testmodel.g:3992:2: ( ( (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a6= ';' ) )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==36) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // Testmodel.g:3993:3: ( (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a6= ';' )
            	    {
            	    // Testmodel.g:3993:3: ( (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a6= ';' )
            	    // Testmodel.g:3994:4: (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable ) a6= ';'
            	    {
            	    // Testmodel.g:3994:4: (a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable )
            	    // Testmodel.g:3995:5: a5_0= parse_edu_ustb_sei_mde_testing_testdefinition_Variable
            	    {
            	    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Variable_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4032);
            	    a5_0=parse_edu_ustb_sei_mde_testing_testdefinition_Variable();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a5_0 != null) {
            	    						if (a5_0 != null) {
            	    							Object value = a5_0;
            	    							addObjectToList(element, edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__VARIABLES, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_6_0_0_0, a5_0, true);
            	    						copyLocalizationInfos(a5_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[255]);
            	    			}

            	    a6=(Token)match(input,21,FOLLOW_21_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4060); if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    				if (element == null) {
            	    					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
            	    					startIncompleteElement(element);
            	    				}
            	    				collectHiddenTokens(element);
            	    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_6_0_0_1, null, true);
            	    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a6, element);
            	    			}

            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[256]);
            	    				addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[257]);
            	    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[258]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[259]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[260]);
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[261]);
            	}

            // Testmodel.g:4045:2: ( (a7= 'do' (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==27) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // Testmodel.g:4046:3: (a7= 'do' (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) )
                    {
                    // Testmodel.g:4046:3: (a7= 'do' (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script ) )
                    // Testmodel.g:4047:4: a7= 'do' (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script )
                    {
                    a7=(Token)match(input,27,FOLLOW_27_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4102); if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    				if (element == null) {
                    					element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
                    					startIncompleteElement(element);
                    				}
                    				collectHiddenTokens(element);
                    				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_7_0_0_0, null, true);
                    				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a7, element);
                    			}

                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[262]);
                    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[263]);
                    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[264]);
                    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[265]);
                    			}

                    // Testmodel.g:4064:4: (a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script )
                    // Testmodel.g:4065:5: a8_0= parse_edu_ustb_sei_mde_testing_testdefinition_Script
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Script_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4128);
                    a8_0=parse_edu_ustb_sei_mde_testing_testdefinition_Script();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) {
                    					if (terminateParsing) {
                    						throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
                    					}
                    					if (element == null) {
                    						element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
                    						startIncompleteElement(element);
                    					}
                    					if (a8_0 != null) {
                    						if (a8_0 != null) {
                    							Object value = a8_0;
                    							element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__DO), value);
                    							completedElement(value, true);
                    						}
                    						collectHiddenTokens(element);
                    						retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_7_0_0_1, a8_0, true);
                    						copyLocalizationInfos(a8_0, element);
                    					}
                    				}

                    }


                    if ( state.backtracking==0 ) {
                    				// expected elements (follow set)
                    				addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[266]);
                    			}

                    }


                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCase(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[267]);
            	}

            // Testmodel.g:4097:2: (a9_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle )
            // Testmodel.g:4098:3: a9_0= parse_edu_ustb_sei_mde_testing_testdefinition_Oracle
            {
            pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4173);
            a9_0=parse_edu_ustb_sei_mde_testing_testdefinition_Oracle();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
            				startIncompleteElement(element);
            			}
            			if (a9_0 != null) {
            				if (a9_0 != null) {
            					Object value = a9_0;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.TEST_CASE__ASSERTION), value);
            					completedElement(value, true);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_8, a9_0, true);
            				copyLocalizationInfos(a9_0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[268]);
            	}

            a10=(Token)match(input,38,FOLLOW_38_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4191); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createTestCase();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_17_0_0_10, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a10, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(edu.ustb.sei.mde.testing.testcase.TestcasePackage.eINSTANCE.getTestCaseModel(), edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[269]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 19, parse_edu_ustb_sei_mde_testing_testcase_TestCase_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testcase_TestCase"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification"
    // Testmodel.g:4139:1: parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification returns [edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification element = null] : a0= '=' (a1= QUOTED_34_34_92 ) ;
    public final edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification() throws RecognitionException {
        edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification element =  null;

        int parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification_StartIndex = input.index();

        Token a0=null;
        Token a1=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return element; }

            // Testmodel.g:4142:2: (a0= '=' (a1= QUOTED_34_34_92 ) )
            // Testmodel.g:4143:2: a0= '=' (a1= QUOTED_34_34_92 )
            {
            a0=(Token)match(input,22,FOLLOW_22_in_parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification4220); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_18_0_0_0, null, true);
            		copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[270]);
            	}

            // Testmodel.g:4157:2: (a1= QUOTED_34_34_92 )
            // Testmodel.g:4158:3: a1= QUOTED_34_34_92
            {
            a1=(Token)match(input,QUOTED_34_34_92,FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification4238); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelTerminateParsingException();
            			}
            			if (element == null) {
            				element = edu.ustb.sei.mde.testing.testcase.TestcaseFactory.eINSTANCE.createValueLiteralSpecification();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("QUOTED_34_34_92");
            				tokenResolver.setOptions(getOptions());
            				edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.VALUE_LITERAL_SPECIFICATION__LITERAL), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((org.antlr.runtime3_4_0.CommonToken) a1).getLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getCharPositionInLine(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStartIndex(), ((org.antlr.runtime3_4_0.CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(edu.ustb.sei.mde.testing.testcase.TestcasePackage.VALUE_LITERAL_SPECIFICATION__LITERAL), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelGrammarInformationProvider.TESTMODEL_18_0_0_1, resolved, true);
            				copyLocalizationInfos((org.antlr.runtime3_4_0.CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[271]);
            		addExpectedElement(null, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelExpectationConstants.EXPECTATIONS[272]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 20, parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification"



    // $ANTLR start "parse_org_eclipse_emf_ecore_EStructuralFeature"
    // Testmodel.g:4196:1: parse_org_eclipse_emf_ecore_EStructuralFeature returns [org.eclipse.emf.ecore.EStructuralFeature element = null] : (c0= parse_org_eclipse_emf_ecore_EAttribute |c1= parse_org_eclipse_emf_ecore_EReference );
    public final org.eclipse.emf.ecore.EStructuralFeature parse_org_eclipse_emf_ecore_EStructuralFeature() throws RecognitionException {
        org.eclipse.emf.ecore.EStructuralFeature element =  null;

        int parse_org_eclipse_emf_ecore_EStructuralFeature_StartIndex = input.index();

        org.eclipse.emf.ecore.EAttribute c0 =null;

        org.eclipse.emf.ecore.EReference c1 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return element; }

            // Testmodel.g:4197:2: (c0= parse_org_eclipse_emf_ecore_EAttribute |c1= parse_org_eclipse_emf_ecore_EReference )
            int alt37=2;
            alt37 = dfa37.predict(input);
            switch (alt37) {
                case 1 :
                    // Testmodel.g:4198:2: c0= parse_org_eclipse_emf_ecore_EAttribute
                    {
                    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EAttribute_in_parse_org_eclipse_emf_ecore_EStructuralFeature4270);
                    c0=parse_org_eclipse_emf_ecore_EAttribute();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Testmodel.g:4199:4: c1= parse_org_eclipse_emf_ecore_EReference
                    {
                    pushFollow(FOLLOW_parse_org_eclipse_emf_ecore_EReference_in_parse_org_eclipse_emf_ecore_EStructuralFeature4280);
                    c1=parse_org_eclipse_emf_ecore_EReference();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 21, parse_org_eclipse_emf_ecore_EStructuralFeature_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_org_eclipse_emf_ecore_EStructuralFeature"



    // $ANTLR start "parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification"
    // Testmodel.g:4203:1: parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification returns [edu.ustb.sei.mde.testing.testdefinition.ValueSpecification element = null] : (c0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification |c1= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification |c2= parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification |c3= parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification |c4= parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification );
    public final edu.ustb.sei.mde.testing.testdefinition.ValueSpecification parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification() throws RecognitionException {
        edu.ustb.sei.mde.testing.testdefinition.ValueSpecification element =  null;

        int parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification_StartIndex = input.index();

        edu.ustb.sei.mde.testing.testdefinition.ValueRangeSpecification c0 =null;

        edu.ustb.sei.mde.testing.testdefinition.ValueSetSpecification c1 =null;

        edu.ustb.sei.mde.testing.testdefinition.ValueScriptSpecification c2 =null;

        edu.ustb.sei.mde.testing.testdefinition.ReturnValueSpecification c3 =null;

        edu.ustb.sei.mde.testing.testcase.ValueLiteralSpecification c4 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return element; }

            // Testmodel.g:4204:2: (c0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification |c1= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification |c2= parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification |c3= parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification |c4= parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification )
            int alt38=5;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==30) ) {
                int LA38_1 = input.LA(2);

                if ( (LA38_1==23) ) {
                    alt38=1;
                }
                else if ( (LA38_1==37) ) {
                    alt38=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 38, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA38_0==22) ) {
                switch ( input.LA(2) ) {
                case IDENTIFIER:
                    {
                    int LA38_5 = input.LA(3);

                    if ( (LA38_5==20) ) {
                        alt38=3;
                    }
                    else if ( (LA38_5==18) ) {
                        alt38=4;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return element;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 38, 5, input);

                        throw nvae;

                    }
                    }
                    break;
                case QUOTED_6037_3762_92:
                    {
                    alt38=3;
                    }
                    break;
                case QUOTED_34_34_92:
                    {
                    alt38=5;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return element;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 38, 2, input);

                    throw nvae;

                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;

            }
            switch (alt38) {
                case 1 :
                    // Testmodel.g:4205:2: c0= parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4301);
                    c0=parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Testmodel.g:4206:4: c1= parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4311);
                    c1=parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Testmodel.g:4207:4: c2= parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4321);
                    c2=parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Testmodel.g:4208:4: c3= parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4331);
                    c3=parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Testmodel.g:4209:4: c4= parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification
                    {
                    pushFollow(FOLLOW_parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4341);
                    c4=parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 22, parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification"

    // Delegated rules


    protected DFA6 dfa6 = new DFA6(this);
    protected DFA37 dfa37 = new DFA37(this);
    static final String DFA6_eotS =
        "\24\uffff";
    static final String DFA6_eofS =
        "\24\uffff";
    static final String DFA6_minS =
        "\2\4\1\5\1\uffff\1\11\1\10\1\16\1\uffff\1\26\1\23\1\11\1\10\1\17"+
        "\1\30\1\11\1\4\1\5\1\26\1\11\1\17";
    static final String DFA6_maxS =
        "\1\46\1\16\1\27\1\uffff\1\11\1\10\1\25\1\uffff\1\26\1\23\1\11\1"+
        "\10\1\21\1\30\1\11\2\5\1\26\1\11\1\21";
    static final String DFA6_acceptS =
        "\3\uffff\1\2\3\uffff\1\1\14\uffff";
    static final String DFA6_specialS =
        "\24\uffff}>";
    static final String[] DFA6_transitionS = {
            "\1\1\1\2\40\uffff\1\3",
            "\1\1\1\2\10\uffff\1\4",
            "\1\6\12\uffff\1\7\6\uffff\1\5",
            "",
            "\1\10",
            "\1\11",
            "\1\3\6\uffff\1\7",
            "",
            "\1\12",
            "\1\13",
            "\1\14",
            "\1\15",
            "\1\17\1\uffff\1\16",
            "\1\20",
            "\1\21",
            "\1\1\1\2",
            "\1\6",
            "\1\22",
            "\1\23",
            "\1\17\1\uffff\1\16"
    };

    static final short[] DFA6_eot = DFA.unpackEncodedString(DFA6_eotS);
    static final short[] DFA6_eof = DFA.unpackEncodedString(DFA6_eofS);
    static final char[] DFA6_min = DFA.unpackEncodedStringToUnsignedChars(DFA6_minS);
    static final char[] DFA6_max = DFA.unpackEncodedStringToUnsignedChars(DFA6_maxS);
    static final short[] DFA6_accept = DFA.unpackEncodedString(DFA6_acceptS);
    static final short[] DFA6_special = DFA.unpackEncodedString(DFA6_specialS);
    static final short[][] DFA6_transition;

    static {
        int numStates = DFA6_transitionS.length;
        DFA6_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA6_transition[i] = DFA.unpackEncodedString(DFA6_transitionS[i]);
        }
    }

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = DFA6_eot;
            this.eof = DFA6_eof;
            this.min = DFA6_min;
            this.max = DFA6_max;
            this.accept = DFA6_accept;
            this.special = DFA6_special;
            this.transition = DFA6_transition;
        }
        public String getDescription() {
            return "()* loopback of 823:2: ( (a10_0= parse_org_eclipse_emf_ecore_EStructuralFeature ) )*";
        }
    }
    static final String DFA37_eotS =
        "\16\uffff";
    static final String DFA37_eofS =
        "\16\uffff";
    static final String DFA37_minS =
        "\2\4\1\5\1\11\2\uffff\1\26\1\11\1\17\1\11\1\4\1\26\1\11\1\17";
    static final String DFA37_maxS =
        "\1\5\1\16\1\27\1\11\2\uffff\1\26\1\11\1\21\1\11\1\5\1\26\1\11\1"+
        "\21";
    static final String DFA37_acceptS =
        "\4\uffff\1\1\1\2\10\uffff";
    static final String DFA37_specialS =
        "\16\uffff}>";
    static final String[] DFA37_transitionS = {
            "\1\1\1\2",
            "\1\1\1\2\10\uffff\1\3",
            "\1\4\12\uffff\1\5\6\uffff\1\4",
            "\1\6",
            "",
            "",
            "\1\7",
            "\1\10",
            "\1\12\1\uffff\1\11",
            "\1\13",
            "\1\1\1\2",
            "\1\14",
            "\1\15",
            "\1\12\1\uffff\1\11"
    };

    static final short[] DFA37_eot = DFA.unpackEncodedString(DFA37_eotS);
    static final short[] DFA37_eof = DFA.unpackEncodedString(DFA37_eofS);
    static final char[] DFA37_min = DFA.unpackEncodedStringToUnsignedChars(DFA37_minS);
    static final char[] DFA37_max = DFA.unpackEncodedStringToUnsignedChars(DFA37_maxS);
    static final short[] DFA37_accept = DFA.unpackEncodedString(DFA37_acceptS);
    static final short[] DFA37_special = DFA.unpackEncodedString(DFA37_specialS);
    static final short[][] DFA37_transition;

    static {
        int numStates = DFA37_transitionS.length;
        DFA37_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA37_transition[i] = DFA.unpackEncodedString(DFA37_transitionS[i]);
        }
    }

    class DFA37 extends DFA {

        public DFA37(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 37;
            this.eot = DFA37_eot;
            this.eof = DFA37_eof;
            this.min = DFA37_min;
            this.max = DFA37_max;
            this.accept = DFA37_accept;
            this.special = DFA37_special;
            this.transition = DFA37_transition;
        }
        public String getDescription() {
            return "4196:1: parse_org_eclipse_emf_ecore_EStructuralFeature returns [org.eclipse.emf.ecore.EStructuralFeature element = null] : (c0= parse_org_eclipse_emf_ecore_EAttribute |c1= parse_org_eclipse_emf_ecore_EReference );";
        }
    }
 

    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel_in_start96 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EClass138 = new BitSet(new long[]{0x0000000880000010L});
    public static final BitSet FOLLOW_31_in_parse_org_eclipse_emf_ecore_EClass173 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_35_in_parse_org_eclipse_emf_ecore_EClass188 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EClass213 = new BitSet(new long[]{0x0000002010000000L});
    public static final BitSet FOLLOW_28_in_parse_org_eclipse_emf_ecore_EClass243 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EClass269 = new BitSet(new long[]{0x0000002000020000L});
    public static final BitSet FOLLOW_17_in_parse_org_eclipse_emf_ecore_EClass315 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EClass349 = new BitSet(new long[]{0x0000002000020000L});
    public static final BitSet FOLLOW_37_in_parse_org_eclipse_emf_ecore_EClass430 = new BitSet(new long[]{0x0000004000000030L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EStructuralFeature_in_parse_org_eclipse_emf_ecore_EClass453 = new BitSet(new long[]{0x0000004000000030L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EOperation_in_parse_org_eclipse_emf_ecore_EClass488 = new BitSet(new long[]{0x0000004000000030L});
    public static final BitSet FOLLOW_38_in_parse_org_eclipse_emf_ecore_EClass514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EAttribute552 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EAttribute582 = new BitSet(new long[]{0x0000000000800020L});
    public static final BitSet FOLLOW_23_in_parse_org_eclipse_emf_ecore_EAttribute612 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EAttribute638 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_parse_org_eclipse_emf_ecore_EAttribute671 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EAttribute697 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_parse_org_eclipse_emf_ecore_EAttribute730 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EAttribute767 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_parse_org_eclipse_emf_ecore_EAttribute788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EReference826 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EReference856 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_parse_org_eclipse_emf_ecore_EReference877 = new BitSet(new long[]{0x0000000000800020L});
    public static final BitSet FOLLOW_23_in_parse_org_eclipse_emf_ecore_EReference900 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EReference926 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_parse_org_eclipse_emf_ecore_EReference959 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EReference985 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_parse_org_eclipse_emf_ecore_EReference1018 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EReference1055 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_parse_org_eclipse_emf_ecore_EReference1076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EOperation1114 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EOperation1144 = new BitSet(new long[]{0x0000000000800020L});
    public static final BitSet FOLLOW_23_in_parse_org_eclipse_emf_ecore_EOperation1174 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EOperation1200 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_parse_org_eclipse_emf_ecore_EOperation1233 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EOperation1259 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_parse_org_eclipse_emf_ecore_EOperation1292 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EOperation1329 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_org_eclipse_emf_ecore_EOperation1350 = new BitSet(new long[]{0x0000000000008030L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EParameter_in_parse_org_eclipse_emf_ecore_EOperation1379 = new BitSet(new long[]{0x0000000000028030L});
    public static final BitSet FOLLOW_17_in_parse_org_eclipse_emf_ecore_EOperation1420 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EParameter_in_parse_org_eclipse_emf_ecore_EOperation1454 = new BitSet(new long[]{0x0000000000028030L});
    public static final BitSet FOLLOW_15_in_parse_org_eclipse_emf_ecore_EOperation1528 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_parse_org_eclipse_emf_ecore_EOperation1542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EAnnotation_in_parse_org_eclipse_emf_ecore_EParameter1580 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EParameter1610 = new BitSet(new long[]{0x0000000000800020L});
    public static final BitSet FOLLOW_23_in_parse_org_eclipse_emf_ecore_EParameter1640 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EParameter1666 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_parse_org_eclipse_emf_ecore_EParameter1699 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_NUMBER_in_parse_org_eclipse_emf_ecore_EParameter1725 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_parse_org_eclipse_emf_ecore_EParameter1758 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_org_eclipse_emf_ecore_EParameter1795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ANNOTATION_in_parse_org_eclipse_emf_ecore_EAnnotation1835 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_14_in_parse_org_eclipse_emf_ecore_EAnnotation1865 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_parse_java_util_Map_Entry_in_parse_org_eclipse_emf_ecore_EAnnotation1891 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_17_in_parse_org_eclipse_emf_ecore_EAnnotation1932 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_parse_java_util_Map_Entry_in_parse_org_eclipse_emf_ecore_EAnnotation1966 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_15_in_parse_org_eclipse_emf_ecore_EAnnotation2027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_java_util_Map_Entry2079 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_parse_java_util_Map_Entry2100 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_java_util_Map_Entry2118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2154 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2172 = new BitSet(new long[]{0x0000000C80000012L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EClass_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2202 = new BitSet(new long[]{0x0000000C80000012L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestModel2237 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2278 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2296 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2317 = new BitSet(new long[]{0x000000100A000000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Variable_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2346 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2374 = new BitSet(new long[]{0x000000100A000000L});
    public static final BitSet FOLLOW_27_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2416 = new BitSet(new long[]{0x0000000002400420L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Script_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2442 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2487 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_parse_edu_ustb_sei_mde_testing_testdefinition_TestScenario2505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2549 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2582 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_QUOTED_6037_3762_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2645 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_in_parse_edu_ustb_sei_mde_testing_testdefinition_Script2655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2680 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2698 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2723 = new BitSet(new long[]{0x0000000040400000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_Variable2748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2781 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2795 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2813 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2834 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2852 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification2873 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2902 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2916 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2934 = new BitSet(new long[]{0x0000004000020000L});
    public static final BitSet FOLLOW_17_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2964 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification2990 = new BitSet(new long[]{0x0000004000020000L});
    public static final BitSet FOLLOW_38_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification3036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3065 = new BitSet(new long[]{0x0000000000000420L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3094 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3127 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_QUOTED_6037_3762_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification3164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3200 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3218 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3239 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3257 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3278 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3307 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_17_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3353 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3387 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_15_in_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification3468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3497 = new BitSet(new long[]{0x0000000000000420L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3526 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3559 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_QUOTED_6037_3762_92_in_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle3596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3632 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3650 = new BitSet(new long[]{0x0000000424000002L});
    public static final BitSet FOLLOW_26_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3680 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3706 = new BitSet(new long[]{0x0000000420000002L});
    public static final BitSet FOLLOW_29_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3761 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3787 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testcase_TestCase_in_parse_edu_ustb_sei_mde_testing_testcase_TestCaseModel3842 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_34_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3883 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3901 = new BitSet(new long[]{0x0000002004000000L});
    public static final BitSet FOLLOW_26_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3931 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IDENTIFIER_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase3957 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4003 = new BitSet(new long[]{0x000000100A000000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Variable_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4032 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4060 = new BitSet(new long[]{0x000000100A000000L});
    public static final BitSet FOLLOW_27_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4102 = new BitSet(new long[]{0x0000000002400420L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Script_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4128 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_Oracle_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4173 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_parse_edu_ustb_sei_mde_testing_testcase_TestCase4191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification4220 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_QUOTED_34_34_92_in_parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification4238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EAttribute_in_parse_org_eclipse_emf_ecore_EStructuralFeature4270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_org_eclipse_emf_ecore_EReference_in_parse_org_eclipse_emf_ecore_EStructuralFeature4280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueRangeSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSetSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ValueScriptSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4321 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testdefinition_ReturnValueSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_edu_ustb_sei_mde_testing_testcase_ValueLiteralSpecification_in_parse_edu_ustb_sei_mde_testing_testdefinition_ValueSpecification4341 = new BitSet(new long[]{0x0000000000000002L});

}