/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class TestmodelExpectedCsString extends edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelAbstractExpectedElement {
	
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelKeyword keyword;
	
	public TestmodelExpectedCsString(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.grammar.TestmodelSyntaxElement getSymtaxElement() {
		return keyword;
	}
	
	public java.util.Set<String> getTokenNames() {
		return java.util.Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof TestmodelExpectedCsString) {
			return getValue().equals(((TestmodelExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override	
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
