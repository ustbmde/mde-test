/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp;

public class TestmodelProblem implements edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelProblem {
	
	private String message;
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType type;
	private edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity severity;
	private java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix> quickFixes;
	
	public TestmodelProblem(String message, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType type, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity severity) {
		this(message, type, severity, java.util.Collections.<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix>emptySet());
	}
	
	public TestmodelProblem(String message, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType type, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity severity, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix quickFix) {
		this(message, type, severity, java.util.Collections.singleton(quickFix));
	}
	
	public TestmodelProblem(String message, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType type, edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity severity, java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new java.util.LinkedHashSet<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemType getType() {
		return type;
	}
	
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.TestmodelEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public java.util.Collection<edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
