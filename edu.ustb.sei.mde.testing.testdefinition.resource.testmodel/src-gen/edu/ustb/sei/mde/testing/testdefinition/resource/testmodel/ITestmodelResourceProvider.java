/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel;

/**
 * Implementors of this interface provide an EMF resource.
 */
public interface ITestmodelResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.ITestmodelTextResource getResource();
	
}
