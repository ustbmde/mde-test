/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util;

/**
 * Class TestmodelTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourc
 * eUtil.
 */
public class TestmodelTextResourceUtil {
	
	/**
	 * Use
	 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourc
	 * eUtil.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResource getResource(org.eclipse.core.resources.IFile file) {
		return new edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourc
	 * eUtil.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResource getResource(java.io.File file, java.util.Map<?,?> options) {
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourc
	 * eUtil.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResource getResource(org.eclipse.emf.common.util.URI uri) {
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourc
	 * eUtil.getResource() instead.
	 */
	@Deprecated	
	public static edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.mopp.TestmodelResource getResource(org.eclipse.emf.common.util.URI uri, java.util.Map<?,?> options) {
		return edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.util.TestmodelResourceUtil.getResource(uri, options);
	}
	
}
