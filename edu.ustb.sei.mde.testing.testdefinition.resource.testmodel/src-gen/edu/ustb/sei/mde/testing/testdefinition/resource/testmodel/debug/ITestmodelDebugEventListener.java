/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug;

public interface ITestmodelDebugEventListener {
	
	/**
	 * Notification that the given event occurred in the while debugging.
	 */
	public void handleMessage(edu.ustb.sei.mde.testing.testdefinition.resource.testmodel.debug.TestmodelDebugMessage message);
}
